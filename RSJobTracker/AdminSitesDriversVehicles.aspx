﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="AdminSitesDriversVehicles.aspx.cs" Inherits="AdminSitesDriversVehicles" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script language="javascript" type="text/javascript">
    window.setTimeout("DoPostBack();", 50000);
    function DoPostBack() {
        document.forms[0].submit();
    }
    </script>
    <style type="text/css">
        .style4
        {
            width: 560px;
        }
    </style>
    </asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h1><asp:Label ID="lblHeading" runat="server" Text=""></asp:Label></h1>
    
    
    <table>
    <tr>
    <td bgcolor="#DDDDDD">
        Site</td>
    <td bgcolor="#EEEEEE">
        Drivers
    </td>
    <td bgcolor="#DDDDDD">
        Vehicles</td>
    <td class="style4" bgcolor="#EEEEEE">
        <br />
        
    </td>
    </tr>
    <tr>
    <td valign="top" bgcolor="#DDDDDD">
        <asp:ListBox ID="ListBoxClients" runat="server" AutoPostBack="True" 
            DataSourceID="ObjectDataSourceSites" DataTextField="RSGSiteName" 
            DataValueField="RSGSiteID" 
            onselectedindexchanged="ListBoxClients_SelectedIndexChanged"></asp:ListBox>
        <br />
        Add Site:<br />
        <asp:TextBox ID="TextBoxAddSite" runat="server"></asp:TextBox>
        <br />
        <asp:Button ID="ButtonAddSite" runat="server" Text="Add" 
            onclick="ButtonAddSite_Click" />
        <br />
    </td>
    <td valign="top" bgcolor="#EEEEEE" width="400">
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
            DataSourceID="ObjectDataSourceDrivers" 
            onrowdatabound="GridView1_RowDataBound" Width="357px">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:DropDownList ID="DropDownList1" runat="server" 
                            DataSourceID="ObjectDataSourceSites" DataTextField="RSGSiteName" 
                            DataValueField="RSGSiteID" SelectedValue='<%# Bind("RSGSiteID") %>' 
                            onselectedindexchanged="DropDownList1_SelectedIndexChanged" 
                            AutoPostBack="True">
                        </asp:DropDownList>
                        <asp:Label ID="lblDiverID" runat="server" Text='<%# Eval("RSGDriverID") %>' 
                            Visible="False"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="RSGDriverName" HeaderText="RSGDriverName" 
                    SortExpression="RSGDriverName" />
                <asp:TemplateField HeaderText="CellNumber" SortExpression="CellNumber">
                    
                    <ItemTemplate>
                         <asp:TextBox ID="TextBoxCellNumber" runat="server" 
                             Text='<%# Bind("CellNumber") %>' AutoPostBack="True" 
                             ontextchanged="TextBoxCellNumber_TextChanged"></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Active" SortExpression="Active">
                    <EditItemTemplate>
                        <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Active") %>' />
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Active") %>' 
                            oncheckedchanged="CheckBox1_CheckedChanged" AutoPostBack="True" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        Add Driver<br />
        <asp:TextBox ID="TextBoxAddDriver" runat="server"></asp:TextBox>
        <br />
        <asp:Button ID="ButtonAddDriver" runat="server" Text="Add" 
            onclick="ButtonAddDriver_Click" />
    </td>
    <td valign="top" bgcolor="#DDDDDD" width="400">
        <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" 
            DataSourceID="ObjectDataSourceVehicles" 
            onrowdatabound="GridView1_RowDataBound" Width="558px">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:DropDownList ID="DropDownList1" runat="server" 
                            DataSourceID="ObjectDataSourceSites" DataTextField="RSGSiteName" 
                            DataValueField="RSGSiteID" SelectedValue='<%# Bind("RSGSiteID") %>' 
                            onselectedindexchanged="DropDownList2_SelectedIndexChanged1" 
                            AutoPostBack="True">
                        </asp:DropDownList>
                        <asp:Label ID="lblVehicleID" runat="server" Text='<%# Eval("RSGVehicleID") %>' 
                            Visible="False"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField><asp:BoundField DataField="RSGVehicleRegNumber" 
                    HeaderText="RSGVehicleRegNumber" SortExpression="RSGVehicleRegNumber" />
                <asp:TemplateField HeaderText="RSGVehicleDescription" 
                    SortExpression="RSGVehicleDescription">
                    
                    <ItemTemplate>
                        <asp:TextBox ID="TextBoxVehDesc" runat="server" 
                            Text='<%# Bind("RSGVehicleDescription") %>' AutoPostBack="True" 
                            ontextchanged="TextBoxVehDesc_TextChanged"></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Active" SortExpression="Active">
                    <EditItemTemplate>
                        <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Active") %>' />
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Active") %>' 
                            oncheckedchanged="CheckBox1_CheckedChanged1" AutoPostBack="True" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        Add Vehicle:<br />
        Reg Number: 
        <asp:TextBox ID="TextBoxAddVehicle" runat="server"></asp:TextBox>
        <br />
        Description:<asp:TextBox ID="TextBoxAddVehicledesc" runat="server"></asp:TextBox>
        <br />
        <asp:Button ID="ButtonAddVehicle" runat="server" Text="Add" 
            onclick="ButtonAddVehicle_Click" />
    </td>
    <td class="style4" bgcolor="#EEEEEE">
        &nbsp;</td>
    </tr>
    </table>
    
    
    <asp:Panel ID="PanelListView" runat="server">
    </asp:Panel>
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </asp:ToolkitScriptManager>
    <asp:ObjectDataSource ID="ObjectDataSourceDrivers" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByRSGSiteID" 
        TypeName="LookupsXSDTableAdapters.RSGDriversTableAdapter" 
        InsertMethod="Insert">
        <InsertParameters>
            <asp:Parameter Name="RSGSiteID" Type="Int32" />
            <asp:Parameter Name="RSGDriverName" Type="String" />
            <asp:Parameter Name="Active" Type="Boolean" />
        </InsertParameters>
        <SelectParameters>
            <asp:ControlParameter ControlID="ListBoxClients" Name="RSGSiteID" 
                PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceVehicles" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByRSGSiteID" 
        TypeName="LookupsXSDTableAdapters.RSGVehiclesTableAdapter">
        <SelectParameters>
            <asp:ControlParameter ControlID="ListBoxClients" 
                Name="RSGSiteID" PropertyName="SelectedValue" Type="Int32" 
                DefaultValue="0" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceSites" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        TypeName="LookupsXSDTableAdapters.RSGSitesTableAdapter" 
    InsertMethod="Insert">
        <InsertParameters>
            <asp:Parameter Name="RSGSiteName" Type="String" />
        </InsertParameters>
    </asp:ObjectDataSource>
    </asp:Content>
