﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="TicketDetail.aspx.cs" Inherits="MyTickets" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
        .style3
        {
            color: #FF6600;
        }
        .style4
        {
            width: 266px;
        }
    </style>
    </asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    
        <asp:Button ID="ButtonBack" runat="server" Text="Back" 
            onclick="ButtonBack_Click" />
    &nbsp;<asp:Panel ID="PanelEditHeader" runat="server">
   <ul>
       <asp:Label ID="LabelError" runat="server" 
           style="font-size: medium; font-weight: 700; color: #FF0000" Text="Label" 
           Visible="False"></asp:Label>
&nbsp;<table width="1200" bgcolor=#333333 cellspacing =1 cellpadding=2>
           <tr>
               <td bgcolor="#AAAAAA" colspan="6">
                   JOB NUMBER:
                   <asp:Label ID="LabelJobNumber" runat="server" style="font-weight: 700" 
                       Text="2213"></asp:Label>
               </td>
           </tr>
           <tr>
               <td bgcolor="#CCCCCC" class="style4">
                   Client, Region, SubRegion, Site</td>
               <td bgcolor="#CCCCCC" colspan="5">
                   &nbsp;<asp:DropDownList ID="DropDownListClient" runat="server" 
                       AutoPostBack="True" 
                       onselectedindexchanged="DropDownListClient_SelectedIndexChanged">
                   </asp:DropDownList>
                   &nbsp;<asp:DropDownList ID="DropDownListRegion" runat="server" 
                       AutoPostBack="True" 
                       onselectedindexchanged="DropDownListRegion_SelectedIndexChanged">
                   </asp:DropDownList>
                   &nbsp;<asp:DropDownList ID="DropDownListSubRegion" runat="server" 
                       AutoPostBack="True" 
                       onselectedindexchanged="DropDownListSubRegion_SelectedIndexChanged">
                   </asp:DropDownList>
                   &nbsp;Filter for site: 
                   <asp:TextBox ID="TextBoxSiteFilter" runat="server" 
                       AutoPostBack="True" ontextchanged="TextBoxSiteFilter_TextChanged" 
                       Width="62px"></asp:TextBox>
                   <asp:DropDownList ID="DropDownListSite" runat="server" AutoPostBack="True" 
                       onselectedindexchanged="DropDownListSite_SelectedIndexChanged">
                   </asp:DropDownList>
                   <br />
                   &nbsp;<asp:Label ID="LabelSiteLoc" runat="server" Text="-"></asp:Label>
                   &nbsp;<asp:HyperLink ID="HyperLink1" runat="server" Target="_blank">Map</asp:HyperLink>
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; sms to: +27<asp:TextBox ID="TextBoxSMSNo" runat="server"></asp:TextBox>
                   <asp:Button ID="ButtonSendSMS" runat="server" onclick="ButtonSendSMS_Click" 
                       Text="Send" />
                   &nbsp;&nbsp; Site Owner:
                   <asp:TextBox ID="TextBoxClientSiteOwner" runat="server" AutoPostBack="True" 
                       ontextchanged="TextBoxClientSiteOwner_TextChanged" Width="108px" 
                       style="height: 22px"></asp:TextBox>
                   <br />
                   &nbsp;ETA as per site priority:
                   <asp:Label ID="LabelCalculatedETA" runat="server" Text="-"></asp:Label>
               </td>
           </tr>
           <tr>
               <td bgcolor="#CCCCCC" class="style4">
                   Ticket Number</td>
               <td bgcolor="#CCCCCC" colspan="2">
                   <asp:TextBox ID="TextBoxTicketNumber" runat="server" Width="137px" 
                       AutoPostBack="True" ontextchanged="TextBoxTicketNumber_TextChanged"></asp:TextBox>
               </td>
               <td bgcolor="#CCCCCC" colspan="2">
                   Call Type:</td>
               <td bgcolor="#CCCCCC">
                   <asp:DropDownList ID="DropDownListCallType" runat="server" AutoPostBack="True" 
                       onselectedindexchanged="DropDownListCallType_SelectedIndexChanged">
                   </asp:DropDownList>
               </td>
           </tr>
           <tr>
               <td bgcolor="#EEEEEE" colspan="6">
                   </td>
           </tr>
           <tr>
               <td bgcolor="#CCCCCC" class="style4">
                   Call received</td>
               <td bgcolor="#CCCCCC" colspan="2">
                   &nbsp;<asp:TextBox ID="TextBoxCallRec" runat="server" Width="178px" 
                       ReadOnly="True"></asp:TextBox>
                   &nbsp;<asp:Button ID="ButtonSetCallRec" runat="server" 
                       onclick="ButtonSetCallRec_Click" Text="&lt;&lt; Set" />
                   &nbsp;<asp:Label ID="LabelCallReceivedBy" runat="server"></asp:Label>
                   <asp:Label ID="LabelCallReceivedByID" runat="server" Visible="False"></asp:Label>
               </td>
               <td bgcolor="#CCCCCC" colspan="2">
                   R&amp;S Site</td>
               <td bgcolor="#CCCCCC">
                   <asp:DropDownList ID="DropDownListRSGSite" runat="server" AutoPostBack="True" 
                       onselectedindexchanged="DropDownListRSGSite_SelectedIndexChanged">
                   </asp:DropDownList>
               </td>
           </tr>
           <tr>
               <td bgcolor="#DDDDDD" class="style4">
                   Committed ETA</td>
               <td bgcolor="#DDDDDD" colspan="2">
                   &nbsp;<asp:TextBox ID="TextBoxCommitedETA" runat="server" AutoPostBack="True" 
                       ontextchanged="TextBoxCommitedETA_TextChanged" Width="178px">yyyy/mm/dd hh:mm</asp:TextBox>
                   &nbsp;&nbsp;@
                   <asp:Label ID="LabelETAAt" runat="server"></asp:Label>
                   &nbsp;</td>
               <td bgcolor="#CCCCCC" colspan="2">
                   Driver </td>
               <td bgcolor="#CCCCCC">
                   <asp:DropDownList ID="DropDownListDriver" runat="server" AutoPostBack="True" 
                       onselectedindexchanged="DropDownListDriver_SelectedIndexChanged">
                   </asp:DropDownList>
               </td>
           </tr>
           <tr>
               <td bgcolor="#DDDDDD" class="style4">
                   Driver Dispatched</td>
               <td bgcolor="#DDDDDD" colspan="2">
                   &nbsp;<asp:TextBox ID="TextBoxDisp" runat="server" ReadOnly="True" Width="178px"></asp:TextBox>
                   &nbsp;<asp:Button ID="ButtonSetDisp" runat="server" onclick="ButtonSetDisp_Click" 
                       Text="&lt;&lt; Set" />
                   &nbsp;<asp:Label ID="LabelDriverDispatchedBy" runat="server"></asp:Label>
                   <asp:Label ID="LabelDriverDispatchedByID" runat="server" Visible="False"></asp:Label>
               </td>
               <td bgcolor="#CCCCCC" colspan="2">
                   Vehicle</td>
               <td bgcolor="#CCCCCC">
                   <asp:DropDownList ID="DropDownListVehicle" runat="server" AutoPostBack="True" 
                       onselectedindexchanged="DropDownListVehicle_SelectedIndexChanged">
                   </asp:DropDownList>
               </td>
           </tr>
           <tr>
               <td bgcolor="#CCCCCC" class="style4">
                   Site Up/Complete</td>
               <td bgcolor="#CCCCCC" colspan="2">
                   &nbsp;<asp:TextBox ID="TextBoxSiteUp" runat="server" ReadOnly="True" Width="178px"></asp:TextBox>
                   &nbsp;<asp:Button ID="ButtonSetSiteUp" runat="server" 
                       onclick="ButtonSetSiteUp_Click" Text="&lt;&lt; Set" />
                   <asp:Label ID="LabelDriverDispatchedBy1" runat="server" Text=" "></asp:Label>
                   &nbsp;<asp:Label ID="LabelSiteUpBy" runat="server"></asp:Label>
                   <asp:Label ID="LabelSiteUpByID" runat="server" Visible="False"></asp:Label>
               </td>
               <td bgcolor="#CCCCCC" colspan="2">
                   &nbsp;</td>
               <td bgcolor="#CCCCCC">
                   &nbsp;</td>
           </tr>
           <tr>
               
               <td bgcolor="#EEEEEE" colspan="6">
                   </td>
           </tr>
           <tr>
               <td bgcolor="#CCCCCC">
                   Site Deployment Record:</td>
               <td bgcolor="#CCCCCC" colspan="3">Mains supply alarm active:
                   <asp:DropDownList ID="DropDownListAlarmActive" runat="server" 
                       AutoPostBack="True" 
                       onselectedindexchanged="DropDownListAlarmActive_SelectedIndexChanged">
                       <asp:ListItem></asp:ListItem>
                       <asp:ListItem>Yes</asp:ListItem>
                       <asp:ListItem>No</asp:ListItem>
                   </asp:DropDownList>
                   &nbsp; Generator Mobility:
                   <asp:DropDownList ID="DropDownListGenMob" runat="server" AutoPostBack="True" onselectedindexchanged="DropDownListAlarmActive_SelectedIndexChanged">
                       <asp:ListItem></asp:ListItem>
                       <asp:ListItem>On wheels</asp:ListItem>
                       <asp:ListItem>Locking Bar</asp:ListItem>
                       <asp:ListItem>On stands</asp:ListItem>
                   </asp:DropDownList>
               </td>
               <td bgcolor="#CCCCCC">
                   &nbsp;</td>
               <td bgcolor="#CCCCCC">
&nbsp;</td>
           </tr>
           <tr>
               <td bgcolor="#CCCCCC">
                   Tracker Contact Reference<br /> Control Reference</td>
               <td bgcolor="#CCCCCC" colspan="3">
                   &nbsp;
                   <asp:TextBox ID="TextBoxTrackerRef" runat="server" AutoPostBack="True" 
                       ontextchanged="TextBoxTrackerRef_TextChanged" Width="168px"></asp:TextBox>
                   <br />
                   &nbsp;
                   <asp:TextBox ID="TextBoxVodacomRef" runat="server" AutoPostBack="True" 
                       ontextchanged="TextBoxTrackerRef_TextChanged" Width="168px"></asp:TextBox>
                   &nbsp;<asp:Label ID="LabelTrackerDetail" runat="server"></asp:Label>
               </td>
               <td bgcolor="#CCCCCC">
                   Job Status</td>
               <td bgcolor="#CCCCCC">
                   <asp:CheckBox ID="CheckBoxCancelled" runat="server" AutoPostBack="True" 
                       oncheckedchanged="CheckBoxCancelled_CheckedChanged" Text="Cancelled" />
                   &nbsp;&nbsp;<asp:Label ID="LabelCancelDetail" runat="server"></asp:Label>
                   <br />
                   <asp:CheckBox ID="CheckBoxOnHold" runat="server" AutoPostBack="True" 
                       oncheckedchanged="CheckBoxOnHold_CheckedChanged" Text="On Hold" />
                   &nbsp;
                   <asp:Label ID="LabelOnHoldDetail" runat="server"></asp:Label>
                   <br />
                   <asp:CheckBox ID="CheckBoxCompleted" runat="server" AutoPostBack="True" 
                       oncheckedchanged="CheckBoxCompleted_CheckedChanged" Text="Completed" />
                   &nbsp;
                   <asp:Label ID="LabelCompletedDetail" runat="server"></asp:Label>
               </td>
           </tr>
           <tr>
               
               <td bgcolor="#EEEEEE" colspan="6">
                   <asp:ObjectDataSource ID="ObjectDataSourceGenFills" runat="server" 
                       OldValuesParameterFormatString="original_{0}" 
                       SelectMethod="GetDataByGeneratorFillsForJobID" 
                       TypeName="FuelLogXSDTableAdapters.FuelLogTableAdapter">
                       <SelectParameters>
                           <asp:SessionParameter DefaultValue="" Name="JobID" SessionField="JobID" 
                               Type="Int32" />
                       </SelectParameters>
                   </asp:ObjectDataSource>
                   
                   <asp:ObjectDataSource ID="ObjectDataSourceBowserFills" runat="server" 
                       OldValuesParameterFormatString="original_{0}" 
                       SelectMethod="GetDataByBowserFillsForJobID" 
                       TypeName="FuelLogXSDTableAdapters.FuelLogTableAdapter">
                       <SelectParameters>
                           <asp:SessionParameter DefaultValue="" Name="JobID" SessionField="JobID" 
                               Type="Int32" />
                       </SelectParameters>
                   </asp:ObjectDataSource>
                   </td>
           </tr>
           <tr>
               <td bgcolor="#CCCCCC" colspan="2" valign="top">
                   Generator:
                   <asp:DropDownList ID="DropDownListGenerator" runat="server" AutoPostBack="True" 
                       onselectedindexchanged="DropDownListGenerator_SelectedIndexChanged">
                   </asp:DropDownList>
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
               <td bgcolor="#AAAAAA" colspan="4" valign="top">
                   Bowser:
                   <asp:DropDownList ID="DropDownListBowser" runat="server" AutoPostBack="True" 
                       onselectedindexchanged="DropDownListBowser_SelectedIndexChanged">
                   </asp:DropDownList>
                   &nbsp;</td>
           </tr>
        
           
           <asp:Panel ID="pnlFuel" runat="server">
           <tr>
               <td bgcolor="#CCCCCC" colspan="2" valign="top">
                   Liters Filled:&nbsp;<asp:GridView ID="GridView1" runat="server" 
                       AutoGenerateColumns="False" BackColor="White" 
                       DataSourceID="ObjectDataSourceGenFills" Width="490px">
                       <Columns>
                           <asp:BoundField DataField="GeneratorRegNumber" 
                               HeaderText="Generator Reg Number" SortExpression="GeneratorRegNumber" />
                           <asp:BoundField DataField="FilledAt" HeaderText="FilledAt" 
                               SortExpression="FilledAt" />
                           <asp:BoundField DataField="LitersFilled" HeaderText="LitersFilled" 
                               SortExpression="LitersFilled" />
                           <asp:BoundField DataField="BowserReg" HeaderText="From Bowser / Other" 
                               SortExpression="BowserReg" />
                           <asp:TemplateField>
                               <ItemTemplate>
                                   <asp:Button ID="ButtonDelGen" runat="server" 
                                       CommandArgument='<%# Eval("FuelLogID") %>' Height="17px" 
                                       onclick="ButtonDelGen_Click" Text="Delete" />
                               </ItemTemplate>
                           </asp:TemplateField>
                       </Columns>
                   </asp:GridView>
                   <br />
               </td>
               <td bgcolor="#AAAAAA" colspan="4" valign="top">
                   Liters filled:<asp:GridView ID="GridView2" runat="server" 
                       AutoGenerateColumns="False" BackColor="White" 
                       DataSourceID="ObjectDataSourceBowserFills" Width="400px">
                       <Columns>
                           <asp:BoundField DataField="BowserReg" HeaderText="BowserReg" 
                               SortExpression="BowserReg" />
                           <asp:BoundField DataField="FilledAt" HeaderText="FilledAt" 
                               SortExpression="FilledAt" />
                           <asp:BoundField DataField="LitersFilled" HeaderText="LitersFilled" 
                               SortExpression="LitersFilled" />
                           <asp:TemplateField>
                               <ItemTemplate>
                                   <asp:Button ID="ButtonDelBowser" runat="server" 
                                       CommandArgument='<%# Eval("FuelLogID") %>' Height="17px" 
                                       onclick="ButtonDelBowser_Click" Text="Delete" />
                               </ItemTemplate>
                           </asp:TemplateField>
                       </Columns>
                   </asp:GridView>
                   <br />
               </td>
           </tr>
           <tr>
               <td bgcolor="#CCCCCC" colspan="2" valign="top">
                   Add entry: Date&amp;time:
                   <asp:TextBox ID="TextBoxGenFillAt" runat="server" Width="149px"></asp:TextBox>
                   &nbsp; Liters:
                   <asp:TextBox ID="TextBoxGenFillLiters" runat="server" Width="53px"></asp:TextBox>
                   &nbsp;from
                   <asp:DropDownList ID="DropDownListGenFillFrom" runat="server">
                       <asp:ListItem Value="Bowser"></asp:ListItem>
                       <asp:ListItem>Other</asp:ListItem>
                   </asp:DropDownList>
                   &nbsp;<asp:Button ID="ButtonAddGenLiters" runat="server" 
                       onclick="ButtonAddGenLiters_Click" Text="Add" />
               </td>
               <td bgcolor="#AAAAAA" colspan="4" valign="top">
                   Add entry: Date&amp;time:
                   <asp:TextBox ID="TextBoxBowserFillAt" runat="server" Width="150px"></asp:TextBox>
                   &nbsp; Liters:
                   <asp:TextBox ID="TextBoxBowserFillLiters" runat="server" Width="53px"></asp:TextBox>
                   &nbsp;
                   <asp:Button ID="ButtonAddBowserLiters" runat="server" 
                       onclick="ButtonAddBowserLiters_Click" Text="Add" />
               </td>
           </tr>
           <tr>
               <td bgcolor="#CCCCCC" colspan="2" valign="top">
                   &nbsp;Hours:<asp:TextBox ID="TextBoxLastHours" runat="server" Width="53px" OnTextChanged="DropDownListGenerator_SelectedIndexChanged"  AutoPostBack="True" ></asp:TextBox>
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Service: &nbsp;<asp:DropDownList ID="DropDownListService"  AutoPostBack="True"  runat="server" onselectedindexchanged="DropDownListGenerator_SelectedIndexChanged" Enabled="false" >
                       <asp:ListItem Value="0">None</asp:ListItem>
                       <asp:ListItem>250</asp:ListItem>
                       <asp:ListItem>750</asp:ListItem>
                   </asp:DropDownList>
               
               
                   
                   <asp:Label ID="LabelLastHoursAt" runat="server" Text=""></asp:Label>
                   <td bgcolor="#AAAAAA" colspan="4" valign="top">
                       &nbsp;</td>
               
           </tr>
          
           </asp:Panel>
       
           <tr>
               <td bgcolor="#CCCCCC" colspan="6">
                   <asp:Button ID="ButtonAddUpdate" runat="server" onclick="ButtonAddUpdate_Click" 
                       Text="Update" />
                   <asp:Button ID="ButtonCancel0" runat="server" Text="Cancel" Visible="False" 
                       Width="72px" />
               </td>
           </tr>
       </table>
       <table bgcolor="#333333" cellpadding="2" cellspacing="1" width="1000">
       </table>
       

     
    </ul>
      
     </asp:Panel>  
    
           <asp:Panel ID="PanelComments" runat="server" Visible=false>
         <ul>
           
               <span class="style3"><b>Comments and notes</b></span>
             <table bgcolor="Black" width="1000" cellspacing="1">
             <tr>
             <td bgcolor="#AAAAAA">Add comment/note:
                 <table>
                 <tr>
                 <td>
                 Comment/Note:
                 </td>
                 <td>
                 <asp:TextBox ID="TextBoxComment" runat="server" Width="663px"></asp:TextBox>
                 </td>
                     <td rowspan=2>
                          <asp:Button ID="ButtonAddLogItem" runat="server" 
                             onclick="ButtonAddLogItem_Click" Text="Add" Width="75px" />&nbsp;</td>
                 </tr>
                 <tr>
                 <td>
                 Photo(optional):
                 </td>
                 <td>
                 <asp:FileUpload ID="FileUpload1" runat="server" Width="200px" />
                 </td>
                     
                 </tr>
                 </table>
                 
             </td>
             </tr></table>
                 <asp:GridView 
           ID="GridViewComments" runat="server" AutoGenerateColumns="False" PageSize="20" 
           style="font-size: x-small" Width="1000px" 
                 DataSourceID="ObjectDataSourceJobComments">
           <AlternatingRowStyle BackColor="#DEDEDE" />
                     <Columns>
                         <asp:BoundField DataField="CommentText" HeaderText="Comment" 
                             SortExpression="CommentText" />
                         <asp:BoundField DataField="CommentAt" HeaderText="Logged At" 
                             SortExpression="CommentAt" />
                         <asp:BoundField DataField="UserName" HeaderText="Logged by User" 
                             SortExpression="UserName" />
                         <asp:TemplateField HeaderText="Photo" SortExpression="ImageURL">
                             <ItemTemplate>
                                 <asp:HyperLink ID="HyperLink2" runat="server" 
                                     NavigateUrl='<%# "uploadedimages/" + Eval("ImageURL") %>' 
                                     Text='<%# Eval("ImageURL") %>'></asp:HyperLink>
                             </ItemTemplate>
                             <EditItemTemplate>
                                 <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("ImageURL") %>'></asp:TextBox>
                             </EditItemTemplate>
                         </asp:TemplateField>
                     </Columns>
           <HeaderStyle BackColor="#CCCCCC" />
           <RowStyle BackColor="White" />
       </asp:GridView>
             
                 
                 
             
             <asp:ObjectDataSource ID="ObjectDataSourceJobComments" runat="server" 
                 OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByJobID" 
                 TypeName="JobXSDTableAdapters.JobCommentsTableAdapter">
                 <SelectParameters>
                     <asp:SessionParameter Name="JobID" SessionField="JobID" Type="Int32" />
                 </SelectParameters>
             </asp:ObjectDataSource>
             
                 
                 
             
               <asp:ObjectDataSource ID="ObjectDataSourceJobActionTypes" runat="server" 
                   InsertMethod="Insert" OldValuesParameterFormatString="original_{0}" 
                   SelectMethod="GetData" 
                   TypeName="LookupsXSDTableAdapters.JobActionTypesTableAdapter">
                   <InsertParameters>
                       <asp:Parameter Name="JobActionType" Type="String" />
                       <asp:Parameter Name="JobActionCost" Type="Decimal" />
                   </InsertParameters>
               </asp:ObjectDataSource>
             
                 
                 
             
       </ul>
       </asp:Panel>
   
    
    <br />
&nbsp;<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </asp:ToolkitScriptManager>
           &nbsp;&nbsp;
           </asp:Content>
