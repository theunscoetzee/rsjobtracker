﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Microsoft.Reporting;
using Microsoft.Reporting.WebForms;

public partial class AutoReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string exportdirectory = ConfigurationManager.AppSettings["MailExportDirectory"];

        System.IO.FileStream fs;

        byte[] bytes;

        LocalReport _LocalReport;
        _LocalReport = new LocalReport();

        Warning[] warnings;
        string[] streamids;
        string mimeType;
        string encoding;
        string extension;

        _LocalReport.ReportPath = "Reports/AutoReportJobs.rdlc";

       // AutoReortXSDTableAdapters.JobsDataTableTableAdapter JTA = new AutoReortXSDTableAdapters.JobsDataTableTableAdapter();
      //  AutoReortXSD.JobsDataTableDataTable JDT = JTA.GetData(DateTime.Now.AddDays(-14));

        JobXSDTableAdapters.JobFilterTableAdapter JFTA = new JobXSDTableAdapters.JobFilterTableAdapter();
        JobXSD.JobFilterDataTable JFDT = JFTA.GetData(0, 0, false, "","0", Convert.ToDateTime("1900/1/1"), Convert.ToDateTime("2200/1/1"),"0");

        _LocalReport.DataSources.Add(new ReportDataSource("DataSet1", JFDT.DefaultView.Table));

        bytes = _LocalReport.Render("Excel", null, out mimeType, out encoding, out extension, out streamids, out warnings);

        string FileNameOpen = DateTime.Now.Ticks + "OPEN.xls";

        fs = new System.IO.FileStream(exportdirectory + FileNameOpen, System.IO.FileMode.Create);
        fs.Write(bytes, 0, bytes.Length);
        fs.Close();

        //---send completed jobs as well
        JFDT = JFTA.GetData(0, 0,true, "", "0", DateTime.Now.AddDays (-14), Convert.ToDateTime("2200/1/1"),"0");
        _LocalReport.DataSources.Clear();
        _LocalReport.DataSources.Add(new ReportDataSource("DataSet1", JFDT.DefaultView.Table));

        bytes = _LocalReport.Render("Excel", null, out mimeType, out encoding, out extension, out streamids, out warnings);

        string FileNameCompleted = DateTime.Now.Ticks + "COMPLETED.xls";

        fs = new System.IO.FileStream(exportdirectory + FileNameCompleted, System.IO.FileMode.Create);
        fs.Write(bytes, 0, bytes.Length);
        fs.Close();



        // send mail
        System.Net.Mail.SmtpClient oSmtpClient = new System.Net.Mail.SmtpClient();
        System.Net.Mail.MailMessage oMessage = new System.Net.Mail.MailMessage();

        oMessage.IsBodyHtml = true;
        oMessage.Subject = "Automated report form JobTracker system";
        oMessage.Body = "Please find report attached.";
        oMessage.To.Add("theuns@corporatewebservices.co.za");
        oMessage.To.Add("admin@rsautogen.co.za");
        //oMessage.Bcc.Add("theuns@corporatewebservices.co.za");
        // get & attach documents

        System.Net.Mail.Attachment mA = new System.Net.Mail.Attachment(exportdirectory + FileNameOpen);
        mA.Name = FileNameOpen;
        oMessage.Attachments.Add(mA);

        mA = new System.Net.Mail.Attachment(exportdirectory + FileNameCompleted);
        mA.Name = FileNameCompleted;
        oMessage.Attachments.Add(mA);


        oMessage.Priority = System.Net.Mail.MailPriority.High;
        try
        {
            oSmtpClient.Send(oMessage);
        }
        catch (System.Net.Mail.SmtpException ee)
        {

        }
        Response.Write("Report Sent.");

    }
}