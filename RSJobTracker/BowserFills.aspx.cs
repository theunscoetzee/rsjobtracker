﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class BowserFills : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            TextBoxDate.Text = DateTime.Now.ToString();
        }
       
    }



    protected void ListBoxClients_SelectedIndexChanged(object sender, EventArgs e)
    {
        ListBoxRegions.DataBind();
        //ListBoxSubRegions.DataBind();
        GridView1.DataBind();
        
    }
    protected void ListBoxRegions_SelectedIndexChanged(object sender, EventArgs e)
    {
        //ListBoxSubRegions.DataBind();
        DropDownListBowser.DataBind();
        GridView1.DataBind();
    }
    protected void ListBoxSubRegions_SelectedIndexChanged(object sender, EventArgs e)
    {
        
        GridView1.DataBind();
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
      
    }
    protected void DropDownListRegion_SelectedIndexChanged(object sender, EventArgs e)
    {
        //change region

        DropDownList myDD = (DropDownList)sender;
        Label mylbl = (Label)myDD.Parent.FindControl("lblBowserID");
        BowsersXSDTableAdapters.BowsersTableAdapter BTA = new BowsersXSDTableAdapters.BowsersTableAdapter();
        int x = BTA.UpdateRegionID(Convert.ToInt32(myDD.SelectedValue ),Convert.ToInt32(mylbl.Text));
        GridView1.DataBind();



    }

    protected void DropDownListSubRegion_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList myDD = (DropDownList)sender;
        Label mylbl = (Label)myDD.Parent.FindControl("lblBowserID");

        int? SubRegID = null;
        if (myDD.SelectedValue != "")
        {
            SubRegID = Convert.ToInt32(myDD.SelectedValue);
        }

        BowsersXSDTableAdapters.BowsersTableAdapter BTA = new BowsersXSDTableAdapters.BowsersTableAdapter();
        int x = BTA.UpdateSubRegionID(SubRegID, Convert.ToInt32(mylbl.Text));
        GridView1.DataBind();
        
    }

   
    
   
    protected void ButtonAdd_Click1(object sender, EventArgs e)
    {
        try
        {
            if (TextBoxLiters.Text !="" & TextBoxDate.Text !="")
            {
                FuelLogXSDTableAdapters.FuelLogTableAdapter FLTA = new FuelLogXSDTableAdapters.FuelLogTableAdapter();
                int x=FLTA.InsertQuery (0,Convert.ToDateTime(TextBoxDate.Text),null,Convert.ToInt32(DropDownListBowser.SelectedValue),Convert.ToInt32(TextBoxLiters.Text));
                GridView1.DataBind();
            }

        }
        catch
        {

        }
    }
    protected void TextBoxLiters_TextChanged(object sender, EventArgs e)
    {
        try
        {
           
            TextBox myTB = (TextBox)sender;
            Label logID = (Label)myTB.Parent.FindControl("LabelLogID");

            if (myTB.Text != "")
            {
                FuelLogXSDTableAdapters.FuelLogTableAdapter FLTA = new FuelLogXSDTableAdapters.FuelLogTableAdapter();
                int x = FLTA.UpdateLitresFilled(Convert.ToInt32(myTB.Text), Convert.ToInt32(logID.Text));
                GridView1.DataBind();
            }
        }
        catch
        {

        }
    }
}
