﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AdminGens : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
           
        }
       
    }



    protected void ListBoxClients_SelectedIndexChanged(object sender, EventArgs e)
    {
        ListBoxRegions.DataBind();
        ListBoxSubRegions.DataBind();
        GridView1.DataBind();
        
    }
    protected void ListBoxRegions_SelectedIndexChanged(object sender, EventArgs e)
    {
        ListBoxSubRegions.DataBind();
        GridView1.DataBind();
    }
    protected void ListBoxSubRegions_SelectedIndexChanged(object sender, EventArgs e)
    {
        
        GridView1.DataBind();
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DropDownList ddReg = (DropDownList)e.Row.FindControl("DropDownListRegion");
            DropDownList ddSubReg = (DropDownList)e.Row.FindControl("DropDownListSubRegion");

            LookupsXSDTableAdapters.ClientSubRegionsTableAdapter CSTA = new LookupsXSDTableAdapters.ClientSubRegionsTableAdapter();
            LookupsXSD.ClientSubRegionsDataTable CSDT = CSTA.GetDataByClientRegionID(Convert.ToInt32(ddReg.SelectedValue));
            
            ListItem myLI = new ListItem("","");
            ddSubReg.Items.Add(myLI);

            for (int f = 0; f < CSDT.Rows.Count; f++)
            {
                LookupsXSD.ClientSubRegionsRow  CSRow = (LookupsXSD.ClientSubRegionsRow )CSDT[f];
                
                    myLI = new ListItem(CSRow.SubRegionName, CSRow.ClientSubRegionID.ToString());
                    ddSubReg.Items.Add(myLI);
                
            }
            Label LabelSubRegID = (Label)e.Row.FindControl("lblSubRegID");
            ddSubReg.SelectedValue=LabelSubRegID.Text;

        }
    }
    protected void DropDownListRegion_SelectedIndexChanged(object sender, EventArgs e)
    {
        //change region

        DropDownList myDD = (DropDownList)sender;
        Label mylbl = (Label)myDD.Parent.FindControl("lblGenID");
        GeneratorsXSTableAdapters.QueriesTableAdapter QTA = new GeneratorsXSTableAdapters.QueriesTableAdapter ();
        int x = QTA.UpdateRegionID(Convert.ToInt32(myDD.SelectedValue ),Convert.ToInt32(mylbl.Text));
        GridView1.DataBind();



    }

    protected void DropDownListSubRegion_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList myDD = (DropDownList)sender;
        Label mylbl = (Label)myDD.Parent.FindControl("lblGenID");

        int? SubRegID = null;
        if (myDD.SelectedValue != "")
        {
            SubRegID = Convert.ToInt32(myDD.SelectedValue);
        }

        GeneratorsXSTableAdapters.QueriesTableAdapter QTA = new GeneratorsXSTableAdapters.QueriesTableAdapter();
        int x = QTA.UpdateSubRegionID(SubRegID, Convert.ToInt32(mylbl.Text));
        GridView1.DataBind();
        
    }

    protected void ButtonAdd_Click(object sender, EventArgs e)
    {
        if (TextBoxGenReg.Text != "")
        {
            try
            {
                int? SubRegID = null;
                if (ListBoxSubRegions.SelectedValue != "")
                {
                    SubRegID = Convert.ToInt32(ListBoxSubRegions.SelectedValue);
                }
                GeneratorsXSTableAdapters.QueriesTableAdapter QTA = new GeneratorsXSTableAdapters.QueriesTableAdapter();
                int x = QTA.InsertNewGen(Convert.ToInt32(ListBoxClients.SelectedValue), Convert.ToInt32(ListBoxRegions.SelectedValue), SubRegID, TextBoxGenReg.Text, true, true);
                TextBoxGenReg.Text = "";
            }
            catch
            {
                Response.Write("An error occured - generator could not be added. Perhpas already in database?");
            }
            GridView1.DataBind();
        }
    }
    protected void CheckBoxInService_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox myCB = (CheckBox)sender;
        Label mylbl = (Label)myCB.Parent.FindControl("lblGenID");
        
        GeneratorsXSTableAdapters.QueriesTableAdapter QTA = new GeneratorsXSTableAdapters.QueriesTableAdapter();
        int x = QTA.UpdateSetInService (myCB.Checked ,Convert.ToInt32(mylbl.Text));
        GridView1.DataBind();
    }
    protected void CheckBoxActive_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox myCB = (CheckBox)sender;
        Label mylbl = (Label)myCB.Parent.FindControl("lblGenID");

        GeneratorsXSTableAdapters.QueriesTableAdapter QTA = new GeneratorsXSTableAdapters.QueriesTableAdapter();
        int x = QTA.UpdateSetActive(myCB.Checked, Convert.ToInt32(mylbl.Text));
        GridView1.DataBind();
    }

    protected void TextBoxRegNum_TextChanged(object sender, EventArgs e)
    {
        TextBox myTB = (TextBox)sender;
        Label mylbl = (Label)myTB.Parent.FindControl("lblGenID");

        GeneratorsXSTableAdapters.QueriesTableAdapter QTA = new GeneratorsXSTableAdapters.QueriesTableAdapter();
        int x = QTA.UpdateRegNum(myTB.Text, Convert.ToInt32(mylbl.Text));
        GridView1.DataBind();

    }

    protected void TextBoxLastLocation_TextChanged(object sender, EventArgs e)
    {
        TextBox myTB=(TextBox)sender;

        Label genID = (Label)myTB.Parent.FindControl("lblGenID");
        TextBox LastLoc =(TextBox)myTB.Parent.FindControl("TextBoxLastLocation");
        TextBox LastLocDate = (TextBox)myTB.Parent.FindControl("TextBoxLastLocationDate");
        TextBox LastHours = (TextBox)myTB.Parent.FindControl("TextBoxLastHours");
        TextBox LastHoursAt = (TextBox)myTB.Parent.FindControl("TextBoxLastHoursAt");
        TextBox LastHoursJobID = (TextBox)myTB.Parent.FindControl("TextBoxLastHoursJobID");

        TextBox LastServiceType = (TextBox)myTB.Parent.FindControl("TextBoxLastServiceType");
        TextBox LastServiceAt = (TextBox)myTB.Parent.FindControl("TextBoxLastServiceAt");
        TextBox LastServiceHours = (TextBox)myTB.Parent.FindControl("TextBoxLastServiceHours");
        TextBox LastServiceJobID = (TextBox)myTB.Parent.FindControl("TextBoxLastServcieJobID");

        DateTime? dtLastLocDate = null;
        if(LastLocDate.Text != "") {dtLastLocDate = Convert.ToDateTime (LastLocDate.Text);}
        int? intLastHours = null;
        if (LastHours.Text !="") {intLastHours=Convert.ToInt32(LastHours.Text);}
        DateTime? dtLastHoursAt = null;
        if (LastHoursAt.Text !="") { dtLastHoursAt=Convert.ToDateTime(LastHoursAt.Text);}
        int? intJobID = null;
        if (LastHoursJobID.Text !="") { intLastHours = Convert.ToInt32(LastHoursJobID.Text);}
  
        DateTime? dtLastServiceAt = null;
        if (LastServiceAt.Text !="") { dtLastServiceAt=Convert.ToDateTime(LastServiceAt.Text);}
        int? intLastServiceType = null;
        if (LastServiceType.Text !="") { intLastServiceType = Convert.ToInt32(LastServiceType.Text);}
        int? intLastServiceHours = null;
        if (LastServiceHours.Text !="") { intLastServiceHours = Convert.ToInt32(LastServiceHours.Text);}
        int? intLastServiceJobID = null;
        if (LastServiceJobID.Text !="") { intLastServiceJobID = Convert.ToInt32(LastServiceJobID.Text);}

        GeneratorsXSTableAdapters.QueriesTableAdapter QTA = new GeneratorsXSTableAdapters.QueriesTableAdapter();
        int x= QTA.UpdateFields(LastLoc.Text,dtLastLocDate,intLastHours,dtLastHoursAt,intJobID,intLastServiceType,dtLastServiceAt,intLastServiceHours,intLastServiceJobID,Convert.ToInt32(genID.Text));
        GridView1.DataBind();        
    }
}
