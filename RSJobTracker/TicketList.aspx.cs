﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class TicketList : System.Web.UI.Page
{
    int ctopen = 0;
    int ctcan = 0;
    int cthold = 0;
    int ctover90 = 0;
    int ctover180 = 0;
    int ctrefill = 0;


    protected void Page_Load(object sender, EventArgs e)
    {
        Label20MinsAgo.Text = DateTime.Now.AddMinutes(-30).ToString();
        ListView1.DataBind();
        if (!IsPostBack)
        {
            loadDropdowns();
          //  graph();
            if (Session["UserIsController"].ToString() == "True" | Session["UserIsManager"].ToString() == "True" | Session["UserIsSystemAdmin"].ToString() == "True")
            {

            }
            else
            {
                Response.Write("user has no rights");
                Response.End();
            }
            setCountersToZero();
            
            
        }
        
        GridView1.DataBind();
        
        updateCounters();
    }
    

    protected void loadDropdowns()
    {
       



        ListItem myLI = new ListItem("All","0");
        DropDownListClient.Items.Add(myLI);

        LookupsXSDTableAdapters.ClientsTableAdapter CTA = new LookupsXSDTableAdapters.ClientsTableAdapter();
        LookupsXSD.ClientsDataTable CDT = CTA.GetData();
        
       
        for (int f = 0; f < CDT.Rows.Count; f++)
        {
            LookupsXSD.ClientsRow CRow = (LookupsXSD.ClientsRow)CDT[f];
            myLI = new ListItem(CRow.ClientName ,CRow.ClientID.ToString());
            DropDownListClient.Items.Add(myLI);
        }

        myLI = new ListItem("All", "0");
        DropDownListRegions.Items.Add(myLI);

        LookupsXSDTableAdapters.ClientRegionsTableAdapter CRTA = new LookupsXSDTableAdapters.ClientRegionsTableAdapter();
        LookupsXSD.ClientRegionsDataTable CRDT = CRTA.GetDataByClientID(Convert.ToInt32(DropDownListClient.SelectedValue));

        for (int f = 0; f < CRDT.Rows.Count; f++)
        {
            LookupsXSD.ClientRegionsRow CRRow = (LookupsXSD.ClientRegionsRow)CRDT[f];
            myLI = new ListItem(CRRow.ClientRegionName, CRRow.ClientRegionName);
            DropDownListRegions.Items.Add(myLI);
        }




        //myLI = new ListItem("All", "0");
        //DropDownListRegion.Items.Add(myLI);
        //LookupsXSDTableAdapters.ClientRegionsTableAdapter  CRTA = new LookupsXSDTableAdapters.ClientRegionsTableAdapter ();
        //LookupsXSD.ClientRegionsDataTable CRDT = CRTA.GetData();


        //for (int f = 0; f < CRDT.Rows.Count; f++)
        //{
        //    LookupsXSD.ClientRegionsRow CRow = (LookupsXSD.ClientRegionsRow)CRDT[f];
        //    myLI = new ListItem(CRow.ClientRegionName, CRow.ClientRegionID.ToString());
        //    DropDownListRegion.Items.Add(myLI);
        //}

        //myLI = new ListItem("All", "0");
        //DropDownListSite.Items.Add(myLI);
        //LookupsXSDTableAdapters.ClientSitesTableAdapter CSTA = new LookupsXSDTableAdapters.ClientSitesTableAdapter();
        //LookupsXSD.ClientSitesDataTable CSDT = CSTA.GetData();

        //for (int f = 0; f < CSDT.Rows.Count; f++)
        //{
        //    LookupsXSD.ClientSitesRow CSRow = (LookupsXSD.ClientSitesRow)CSDT[f];
        //    myLI = new ListItem(CSRow.ClientSiteCode, CSRow.ClientSiteCode );
        //    DropDownListSite.Items.Add(myLI);
        //}

        //myLI = new ListItem("All", "0");
        //DropDownListCallType.Items.Add(myLI);
        //LookupsXSDTableAdapters.CallTypesTableAdapter CTTA = new LookupsXSDTableAdapters.CallTypesTableAdapter();
        //LookupsXSD.CallTypesDataTable CTDT = CTTA.GetData();

        //for (int f = 0; f < CTDT.Rows.Count; f++)
        //{
        //    LookupsXSD.CallTypesRow  CTRow = (LookupsXSD.CallTypesRow)CTDT[f];
        //    myLI = new ListItem(CTRow.CallType, CTRow.CallType );
        //    DropDownListCallType.Items.Add(myLI);
        //}
        myLI = new ListItem("", "0");
        DropDownListCallType.Items.Add(myLI);


        LookupsXSDTableAdapters.CallTypesTableAdapter CTTA = new LookupsXSDTableAdapters.CallTypesTableAdapter();
        LookupsXSD.CallTypesDataTable CTDT = CTTA.GetData();

        for (int f = 0; f < CTDT.Rows.Count; f++)
        {
            LookupsXSD.CallTypesRow CTRow = (LookupsXSD.CallTypesRow)CTDT[f];
            myLI = new ListItem(CTRow.CallType, CTRow.CallType);
            DropDownListCallType.Items.Add(myLI);
        }
        //myLI = new ListItem("All", "0");
        //DropDownListDriver.Items.Add(myLI);
        //LookupsXSDTableAdapters.RSGDriversTableAdapter DTA = new LookupsXSDTableAdapters.RSGDriversTableAdapter();
        //LookupsXSD.RSGDriversDataTable DDT = DTA.GetData();

        //for (int f = 0; f < DDT.Rows.Count; f++)
        //{
        //    LookupsXSD.RSGDriversRow DRow = (LookupsXSD.RSGDriversRow )DDT[f];
        //    myLI = new ListItem(DRow.RSGDriverName, DRow.RSGDriverID.ToString() );
        //    DropDownListDriver.Items.Add(myLI);
        //}
        //for (int jare = 2015; jare < 2050; jare++)
        //{
        //    for (int maande = 1; maande < 13; maande++)
        //    {
        //        DropDownListDateFrom.Items.Add(jare.ToString() + "/" + maande.ToString());
        //        DropDownListDateTo.Items.Add(jare.ToString() + "/" + maande.ToString());
        //    }
        //}

        TextBoxFrom.Text = DateTime.Now.AddDays (-14).ToShortDateString();
        TextBoxTo.Text = DateTime.Now.AddDays(1).ToShortDateString();

    }


    //protected void graph()
    //{
    //    Chart1.Series[0].Points.AddY(200);
    //    Chart1.Series[0].Points.Add (20);
    //    Chart1.Series[0].Points.Add(40);
    //    Chart1.Series[0].Points.Add(250);
    //}

     protected void ButtonAddTicket_Click(object sender, EventArgs e)
    {
        Session["Mode"] = "Add";
        Response.Redirect("TicketDetail.aspx");
    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        LinkButton myLB = (LinkButton)sender;
        
        Session["Mode"] = "Edit";
        Session["JobID"] = myLB.Text;
        Response.Redirect("TicketDetail.aspx");

    }
    protected void ButtonFind_Click(object sender, EventArgs e)
    {
        setCountersToZero();
        GridView1.DataBind();
        updateCounters();
    }

    protected void setCountersToZero()
    {
        ctopen = 0;
        ctcan = 0;
        cthold = 0;
        ctover90 = 0;
        ctover180 = 0;
        ctrefill = 0;
    }

    protected void updateCounters()
    {
        LabelCounters.Text = " &nbsp; &nbsp; Status: Jobs: <b>" + ctopen.ToString() + "</b>  | <font color=yellow>Over 90: <b>" + ctover90.ToString() + "</b></font>  | <font color=red>Over 180: <b>" + ctover180.ToString() + "</b></font>  | <font color=pink>On Hold: <b>" + cthold.ToString() + "</b></font> | Cancelled: <b>" + ctcan.ToString() + "</b>" + "</b></font> |  <font color=cyan>Refills: <b>" + ctrefill.ToString() + "</b></font> | Drivers on shift: " + driversonshift().ToString() + " | Gens in Yard: <img src='images/yellow.jpg'> " + gensinyard(2).ToString() + " <img src='images/red.jpg'> " + gensinyard(1).ToString(); 
    }

    protected void TextBoxTicketNumber_TextChanged(object sender, EventArgs e)
    {
        TextBox myTB = (TextBox)sender;
        if (myTB.Text != "")
        {
            
            GridView1.DataBind();

        }
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
       



        if (DropDownListCompleted.SelectedValue == "False")
        {



            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ctopen++;


                Image myImage = (Image)e.Row.FindControl("ImageLogo");
                Label LabelClient = (Label)e.Row.FindControl("LabelClient");
                if (LabelClient.Text == "MTN")
                {
                    myImage.ImageUrl = "~/images/yellow.jpg";
                }
                else
                {
                    myImage.ImageUrl = "~/images/red.jpg";
                }


                Label LabelCallReceivedAt = (Label)e.Row.FindControl("LabelCallReceivedAt");
                Label LabelDispatchedAt = (Label)e.Row.FindControl("LabelDispatchedAt");
                Label LabelSiteUpAt = (Label)e.Row.FindControl("labelSiteUpAt");
                Label LabelCallType = (Label)e.Row.FindControl("LabelCallType");

                CheckBox CBHold = (CheckBox)e.Row.FindControl("CheckBoxOnHold");
                CheckBox CBCan = (CheckBox)e.Row.FindControl("CheckBoxCan");

                if (CBHold.Checked) cthold++;
                if (CBCan.Checked) ctcan++;


                if (!CBHold.Checked & !CBCan.Checked)
                {
                    DateTime DTreceived = Convert.ToDateTime(LabelCallReceivedAt.Text);
                    
                    if (LabelCallType.Text != "Refill Generator")
                    {

                        if (DTreceived.AddMinutes(90) < DateTime.Now) { ctover90++;  e.Row.BackColor = System.Drawing.Color.Yellow; }
                        if (DTreceived.AddMinutes(180) < DateTime.Now) { ctover180++; ctover90--; e.Row.BackColor = System.Drawing.Color.Red; }
                    }
                    else
                    {
                        if (DTreceived.AddMinutes(1440) > DateTime.Now)
                        { ctrefill++;  e.Row.BackColor = System.Drawing.Color.Cyan; }
                        else
                        { ctover180++; e.Row.BackColor = System.Drawing.Color.Red; }
                    }
                }
                else
                {
                    e.Row.BackColor = System.Drawing.Color.LightPink;
                }


               
            }

        }

        if (DropDownListCompleted.SelectedValue == "True")
        {
            


            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ctopen++;
                try
                {
                    Label LabelCallReceivedAt = (Label)e.Row.FindControl("LabelCallReceivedAt");
                    Label LabelDispatchedAt = (Label)e.Row.FindControl("LabelDispatchedAt");
                    Label LabelSiteUpAt = (Label)e.Row.FindControl("labelSiteUpAt");

                     CheckBox CBHold = (CheckBox)e.Row.FindControl("CheckBoxOnHold");
                    CheckBox CBCan = (CheckBox)e.Row.FindControl("CheckBoxCan");
                    if (CBHold.Checked) cthold++;
                    if (CBCan.Checked) ctcan++;

                    if (!CBHold.Checked & !CBCan.Checked)
                    {
                        DateTime DTreceived = Convert.ToDateTime(LabelCallReceivedAt.Text);
                        DateTime DTcompleted = Convert.ToDateTime(LabelSiteUpAt.Text);
                        if (DTreceived.AddMinutes(90) < DTcompleted) { ctover90++; e.Row.BackColor = System.Drawing.Color.LightYellow; }
                        if (DTreceived.AddMinutes(180) < DTcompleted) { ctover90--; ctover180++; e.Row.BackColor = System.Drawing.Color.Salmon; }
                    }
                    else
                    {
                        e.Row.BackColor = System.Drawing.Color.LightPink;
                    }
                }
                 
                catch
                {
                    
                }
            }
        }


    }
    protected void LinkButtonJobID_Click(object sender, EventArgs e)
    {
        LinkButton myLB = (LinkButton)sender;
        Session["JobID"]= myLB.Text;
        Session["Mode"] = "Edit";
        Response.Redirect("TicketDetail.aspx");
    }

    protected void LinkButtonUpdateComment_Click(object sender, EventArgs e)
    {
        LinkButton myLB = (LinkButton)sender;
        Session["JobID"] = myLB.Text;
        Session["Mode"] = "Edit";
        Response.Redirect("TicketDetail.aspx");
    }





    protected void ButtonDrivers_Click(object sender, EventArgs e)
    {
        Response.Redirect("DriversList.aspx");
    }
    protected void ButtonAdd_Click(object sender, EventArgs e)
    {
        Session["Mode"] = "Add";
        Session["JobID"] = "";
        Response.Redirect("TicketDetail.aspx");
    }
    protected void ButtonBowserFills_Click(object sender, EventArgs e)
    {
        Response.Redirect("BowserFills.aspx");
    }
    protected void DropDownListClient_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownListRegions.Items.Clear();

        ListItem myLI = new ListItem("All", "0");
        DropDownListRegions.Items.Add(myLI);
        LookupsXSDTableAdapters.ClientRegionsTableAdapter CRTA = new LookupsXSDTableAdapters.ClientRegionsTableAdapter();
        LookupsXSD.ClientRegionsDataTable CRDT = CRTA.GetDataByClientID(Convert.ToInt32(DropDownListClient.SelectedValue));

        for (int f = 0; f < CRDT.Rows.Count; f++)
        {
            LookupsXSD.ClientRegionsRow CRRow = (LookupsXSD.ClientRegionsRow)CRDT[f];
            myLI = new ListItem(CRRow.ClientRegionName, CRRow.ClientRegionName);
            DropDownListRegions.Items.Add(myLI);
        }
    }

    protected int gensinyard(int ClientID)
    {
        int? num=0;
        GeneratorsXSTableAdapters.QueriesTableAdapter QTA = new GeneratorsXSTableAdapters.QueriesTableAdapter();
        num = Convert.ToInt32( QTA.GetGensInYard(ClientID));
        return Convert.ToInt32( num);
    }

    protected int driversonshift()
    {
        int? num = 0;
        DriversXSDTableAdapters.QueriesTableAdapter QTA = new DriversXSDTableAdapters.QueriesTableAdapter();
        num = Convert.ToInt32(QTA.OnShiftDrivers());
        return Convert.ToInt32(num);
    }

    protected void ButtonGens_Click(object sender, EventArgs e)
    {
        Response.Redirect("Reporting/dailyGeneratorReport.aspx");
    }
}
