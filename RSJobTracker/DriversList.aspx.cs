﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DriversList : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            loadDropdowns();
        }
       
    }


    protected void loadDropdowns()
    {
       



        


    }

     protected void ButtonAddTicket_Click(object sender, EventArgs e)
    {
        Session["Mode"] = "Add";
        Response.Redirect("TicketDetail.aspx");
    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        LinkButton myLB = (LinkButton)sender;
        
        Session["Mode"] = "Edit";
        Session["TicketID"] = myLB.Text;
        Response.Redirect("TicketDetail.aspx");

    }
    protected void ButtonFind_Click(object sender, EventArgs e)
    {
        GridView1.DataBind();
    }

    protected void setcolumns()
    {

        
    }

    protected void TextBoxTicketNumber_TextChanged(object sender, EventArgs e)
    {
        TextBox myTB = (TextBox)sender;
        if (myTB.Text != "")
        {
            
            GridView1.DataBind();
        }
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {



       
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lblShiftEnded = (Label)e.Row.FindControl("lblShiftEnded");
            Label lblShiftStarted = (Label)e.Row.FindControl("lblShiftStarted");
            Button btnSetShiftStatus = (Button)e.Row.FindControl("btnSetStatus");

            if (lblShiftEnded.Text == "" & lblShiftStarted.Text != "" )
            {
                e.Row.BackColor = System.Drawing.Color.LightGreen;
                btnSetShiftStatus.Text = "Set to OFF Shift";
                btnSetShiftStatus.BackColor = System.Drawing.Color.LightPink;


                DateTime myDT = Convert.ToDateTime(lblShiftStarted.Text);
                if (myDT.AddHours(12) < DateTime.Now) e.Row.BackColor = System.Drawing.Color.Red;
            }
            if ((lblShiftEnded.Text == "" & lblShiftStarted.Text == "") | (lblShiftEnded.Text != "" & lblShiftStarted.Text != ""))
            {
                e.Row.BackColor = System.Drawing.Color.LightPink;
                btnSetShiftStatus.Text = "Set to ON Shift";
                btnSetShiftStatus.BackColor = System.Drawing.Color.LightGreen;
            }
        

            
        }
    }
    protected void LinkButtonJobID_Click(object sender, EventArgs e)
    {
        LinkButton myLB = (LinkButton)sender;
        Session["JobID"]= myLB.Text;
        Session["Mode"] = "Edit";
        Response.Redirect("TicketDetail.aspx");
    }

    


    protected void LinkButtonDriverName_Click(object sender, EventArgs e)
    {
        LinkButton myLB = (LinkButton)sender;
        LabelDriverID.Text = myLB.CommandArgument;
        PanelListView.Visible = false;
        PanelDriverHistory.Visible = true;
        GridView2.DataBind();

    }
    protected void btnSetStatus_Click(object sender, EventArgs e)
    {
        DriversXSDTableAdapters.DriversShiftStatusTableAdapter DSTA = new DriversXSDTableAdapters.DriversShiftStatusTableAdapter();

        Button myBut = (Button)sender;
        Label lblShiftStarted = (Label)myBut.Parent.FindControl("lblShiftStarted");
        
        if (myBut.Text == "Set to ON Shift")
        {
            int x = DSTA.InsertNewShift(Convert.ToInt32(myBut.CommandArgument ), DateTime.Now, Convert.ToInt32(Session["UserID"]));
        }
        else
        {
            int x = DSTA.UpdateSetShiftEnded(DateTime.Now, Convert.ToInt32(Session["UserID"]),Convert.ToInt32(myBut.CommandArgument));
        }
        GridView1.DataBind();
    }
    protected void ButtonDrivers_Click(object sender, EventArgs e)
    {
        Response.Redirect("TicketList.aspx");
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        PanelListView.Visible = true;
        PanelDriverHistory.Visible = false;
    }
    protected void TextBoxStart_TextChanged(object sender, EventArgs e)
    {
        // if manager update start of shift
    }
    protected void TextBoxEnd_TextChanged(object sender, EventArgs e)
    {
        // if manager update end of shift
    }
    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}
