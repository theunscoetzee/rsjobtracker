﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/MobileSite.master" AutoEventWireup="true" EnableEventValidation="false"
    CodeFile="MobileDriverTicketList.aspx.cs" Inherits="MobileDriverTicketList" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<%@ Register assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script language="javascript" type="text/javascript">
    window.setTimeout("DoPostBack();", 50000);
    function DoPostBack() {
        document.forms[0].submit();
    }
    </script>
    </asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    
    Jobs for <asp:Label ID="LabelDriver" runat="server" Text="Label"></asp:Label>:<br />
    <asp:MultiView ID="MultiView1" runat="server">
        <asp:View ID="View1" runat="server">
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                DataSourceID="ObjectDataSourceJobList" EmptyDataText="No Current Jobs" 
                Font-Size="XX-Large" onrowdatabound="GridView1_RowDataBound" 
                onselectedindexchanged="GridView1_SelectedIndexChanged" PageSize="20" 
                ShowHeader="False" style="font-size:XX-Large" Width="860px">
                <AlternatingRowStyle BackColor="#DEDEDE" />
                <Columns>
                    <asp:TemplateField InsertVisible="False" SortExpression="JobID">
                        <ItemTemplate>
                            <table border="1" width="100%">
                                <tr>
                                    <td>
                                        Job#:<asp:Label ID="Label25" runat="server" Text='<%# Eval("JobID") %>'></asp:Label>
&nbsp;<asp:ImageButton ID="ImageButton2" runat="server" CommandArgument='<%# Eval("JobID") %>' Height="100px" 
                                            ImageAlign="AbsMiddle" ImageUrl="~/images/start.jpg" 
                                            onclick="ImageButton2_Click" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>
                                        <asp:Label ID="LabelCallType" runat="server" Text='<%# Bind("CallType") %>'></asp:Label>
                                        </b>
                                        <br />
                                        Dipatched:
                                        <asp:Label ID="LabelCallDispatchedAt" runat="server" 
                                            Text='<%# String.Format("{0:yyyy/MM/dd HH:mm}",DataBinder.Eval(Container.DataItem, "DriverDispatchedAt")) %>'></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label12" runat="server" Text='<%# Bind("ClientName") %>'></asp:Label>
                                        /
                                        <asp:Label ID="Label14" runat="server" Text='<%# Bind("ClientRegionName") %>'></asp:Label>
                                        /
                                        <asp:Label ID="Label15" runat="server" Text='<%# Bind("SubRegionName") %>'></asp:Label>
                                        <br />
                                        <asp:Label ID="Label16" runat="server" Text='<%# Bind("ClientSiteCode") %>'></asp:Label>
                                        :
                                        <asp:Label ID="Label17" runat="server" Text='<%# Bind("ClientSiteName") %>'></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Generator:
                                        <asp:Label ID="Label22" runat="server" Text='<%# Bind("GeneratorRegNumber") %>'></asp:Label>
                                        <br />
                                        Bowser:
                                        <asp:Label ID="Label24" runat="server" Text='<%# Bind("BowserReg") %>'></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle BackColor="#CCCCCC" />
                <RowStyle BackColor="White" />
            </asp:GridView>
        </asp:View>
        <asp:View ID="View2" runat="server">
        <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" 
                DataSourceID="ObjectDataSourceJobData" EmptyDataText="No Current Jobs" 
                Font-Size="XX-Large"  
               PageSize="1" 
                ShowHeader="False" style="font-size:XX-Large" Width="860px">
                <AlternatingRowStyle BackColor="#DEDEDE" />
                <Columns>
                    <asp:TemplateField InsertVisible="False" SortExpression="JobID">
                        <ItemTemplate>
                            <table border="1" width="100%">
                                
        
                                <tr>
                                    <td>
                                        Job#:
                                        <asp:Label ID="LabelEditJobID" runat="server"  Text='<%# Eval("JobID") %>'></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>
                                        <asp:Label ID="LabelCallType" runat="server" Text='<%# Bind("CallType") %>'></asp:Label>
                                        </b>
                                        <br />
                                        Dipatched:
                                        <asp:Label ID="LabelCallDispatchedAt" runat="server" 
                                            Text=''></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label12" runat="server" Text='<%# Bind("ClientName") %>'></asp:Label>
                                        /
                                        <asp:Label ID="Label14" runat="server" Text='<%# Bind("ClientRegionName") %>'></asp:Label>
                                        /
                                        <asp:Label ID="Label15" runat="server" Text='<%# Bind("SubRegionName") %>'></asp:Label>
                                        <br />
                                        <asp:Label ID="Label16" runat="server" Text='<%# Bind("ClientSiteCode") %>'></asp:Label>
                                        :
                                        <asp:Label ID="Label17" runat="server" Text='<%# Bind("ClientSiteName") %>'></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Generator:
                                        <asp:Label ID="Label22" runat="server" Text='<%# Bind("GeneratorRegNumber") %>'></asp:Label>
                                        <br />
                                        Bowser:
                                        <asp:Label ID="Label24" runat="server" Text='<%# Bind("BowserReg") %>'></asp:Label>
                                    </td>
                                </tr>
                                
                                </table>
                                </ItemTemplate> 
                                </asp:TemplateField>
                                </Columns>
                                 <HeaderStyle BackColor="#CCCCCC" />
                <RowStyle BackColor="White" />
            </asp:GridView>
           
                                        <strong>Checklist:</strong>
                                        <ul>
                                            <table width="90%">
                                                <tr>
                                                    <td bgcolor="#CCCCCC">
                                                        <asp:Label ID="LabelJobAcknowledged" runat="server" Text="Job Received"></asp:Label>
                                                        &nbsp;
                                                    </td>
                                                    <td>
                                                        <asp:ImageButton ID="ImageButton1" runat="server" ImageAlign="AbsMiddle" 
                                                            ImageUrl="~/images/yesButton.jpg" onclick="ImageButton1_Click" 
                                                            Width="100px" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        &nbsp;</td>
                                                    <td>
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td bgcolor="#CCCCCC">
                                                        <asp:Label ID="LabelArriveDepot" runat="server" Text="At Depot"></asp:Label>
                                                        &nbsp;
                                                    </td>
                                                    <td>
                                                        <asp:ImageButton ID="ImageButtonAtDepot" runat="server" ImageAlign="AbsMiddle" 
                                                            ImageUrl="~/images/yesButton.jpg" Visible="False" Width="100px" 
                                                            onclick="ImageButtonAtDepot_Click1" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        &nbsp;</td>
                                                    <td>
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td bgcolor="#CCCCCC">
                                                        <asp:Label ID="LabelLeaveDepot" runat="server" Text="Leave Depot"></asp:Label>
                                                        &nbsp;
                                                    </td>
                                                    <td>
                                                        <asp:ImageButton ID="ImageButtonLeaveDepot" runat="server" ImageAlign="AbsMiddle" 
                                                            ImageUrl="~/images/yesButton.jpg" Visible="False" Width="100px" 
                                                            onclick="ImageButtonLeaveDepot_Click" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        &nbsp;</td>
                                                    <td>
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td bgcolor="#CCCCCC">
                                                        <asp:Label ID="LabelArriveSite" runat="server" Text="Arrive at site"></asp:Label>
                                                        &nbsp;
                                                    </td>
                                                    <td>
                                                        <asp:ImageButton ID="ImageButtonArriveSite" runat="server" ImageAlign="AbsMiddle" 
                                                            ImageUrl="~/images/yesButton.jpg" Visible="False" Width="100px" 
                                                            onclick="ImageButtonArriveSite_Click" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        &nbsp;&nbsp;</td>
                                                    <td>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td bgcolor="#CCCCCC">
                                                        <asp:Label ID="LabelJobCompleted" runat="server" Text="Job Complete"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:ImageButton ID="ImageButtonJobCompleted" runat="server" ImageAlign="AbsMiddle" 
                                                            ImageUrl="~/images/yesButton.jpg" Visible="False" Width="100px" 
                                                            onclick="ImageButtonJobCompleted_Click" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        &nbsp;</td>
                                                    <td>
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        &nbsp;</td>
                                                    <td>
                                                        &nbsp;</td>
                                                </tr>
                                            </table>
                                      
                                            
                                                <asp:ImageButton ID="ImageButtonBack" runat="server" 
                                                ImageUrl="~/images/back.jpg" onclick="ImageButtonBack_Click" /> <br />
                                           
                                      
        </asp:View>
    </asp:MultiView>
&nbsp;<asp:Panel ID="PanelListView" runat="server">
    </asp:Panel>
    <br />
&nbsp;<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </asp:ToolkitScriptManager>
    <asp:ObjectDataSource ID="ObjectDataSourceJobList" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        TypeName="JobXSDTableAdapters.JobFilterDriverTableAdapter">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="0" Name="DriverID" 
                SessionField="RSGDriverID" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceJobData" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByJobID" 
        TypeName="JobXSDTableAdapters.JobTableAdapter">
        <SelectParameters>
            <asp:SessionParameter Name="JobID" 
                SessionField="JobID" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    </asp:Content>
