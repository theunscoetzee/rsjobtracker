﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ListView.aspx.cs" Inherits="ListView" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    </asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    Part of name:<asp:TextBox ID="TextBoxName" runat="server" AutoPostBack="True" 
        ontextchanged="TextBoxName_TextChanged"></asp:TextBox>
    <asp:Panel ID="PanelFilter" runat="server">
        
        </asp:Panel>
    
    <asp:Panel ID="PanelListView" runat="server">
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
            Width="1200px" PageSize="20" 
            style="font-size: x-small" AllowSorting="True" 
            >
            <AlternatingRowStyle BackColor="#DEDEDE" />
            <Columns>
                <asp:BoundField DataField="CustomerID" HeaderText="CustomerID" 
                    SortExpression="CustomerID" ReadOnly="True" InsertVisible="False" />
                <asp:TemplateField HeaderText="CompanyName" SortExpression="CompanyName">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton1" runat="server" 
                            CommandArgument='<%# Eval("CustomerID") %>' onclick="LinkButton1_Click" 
                            Text='<%# Eval("CompanyName") %>'></asp:LinkButton>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("CompanyName") %>'></asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>
                    <asp:BoundField DataField="PostalAddress" HeaderText="PostalAddress" 
                    SortExpression="PostalAddress" />
                <asp:BoundField DataField="PhysicalAddress" HeaderText="PhysicalAddress" 
                    SortExpression="PhysicalAddress" />
                <asp:BoundField DataField="webaddress" 
                    HeaderText="webaddress" SortExpression="webaddress" />
                <asp:BoundField DataField="SwitchboardNumber" HeaderText="SwitchboardNumber" 
                    SortExpression="SwitchboardNumber" />
                
                <asp:BoundField DataField="FaxNumber" HeaderText="FaxNumber" 
                    SortExpression="FaxNumber" />
                <asp:BoundField DataField="emailaddress" HeaderText="emailaddress" 
                    SortExpression="emailaddress" />
                <asp:BoundField DataField="Region" HeaderText="Region" 
                    SortExpression="Region" >
                </asp:BoundField>
                <asp:BoundField DataField="WithholdingTax" HeaderText="WithholdingTax" 
                    SortExpression="WithholdingTax" >
                </asp:BoundField>
                <asp:BoundField DataField="Active" HeaderText="Active" 
                    SortExpression="Active" >
                </asp:BoundField>
                <asp:BoundField DataField="VATNumber" HeaderText="VATNumber" 
                    SortExpression="VATNumber" >
                </asp:BoundField>
                
                <asp:BoundField DataField="BillingAddress" HeaderText="BillingAddress" 
                    SortExpression="BillingAddress" >
                </asp:BoundField>
            </Columns>
            <HeaderStyle BackColor="#CCCCCC" />
            <RowStyle BackColor="White" />
        </asp:GridView>
    </asp:Panel>
    <br />
&nbsp;<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </asp:ToolkitScriptManager>
    </asp:Content>
