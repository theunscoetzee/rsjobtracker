﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ListView : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            
            
        }
       
    }

    protected void fillgrid()
    {
        
        
       GridView1.DataBind();
        
    }


    protected void ButtonLogContact_Click(object sender, EventArgs e)
    {
        Button myBut = (Button)sender;

        Session["AcctNumber"] = myBut.CommandArgument;
        Session["Mode"] = "Add";

        Response.Redirect("Contacts.aspx");
    }
    protected void ButtonLogCSI_Click(object sender, EventArgs e)
    {
        Button myBut = (Button)sender;

        Session["AcctNumber"] = myBut.CommandArgument;
        Session["Mode"] = "View";

        Response.Redirect("CSI.aspx");
    }


    protected void LinkButtonLastCSalesman_Click(object sender, EventArgs e)
    {
        LinkButton myLB = (LinkButton)sender;
        Session["AcctNumber"] = myLB.CommandArgument;
        Session["Mode"] = "ViewSalesman";
        
        Response.Redirect("Contacts.aspx");

    }
    protected void LinkButtonLastCRegRep_Click(object sender, EventArgs e)
    {
        LinkButton myLB = (LinkButton)sender;
        Session["AcctNumber"] = myLB.CommandArgument;
        Session["Mode"] = "ViewRegRep";

        Response.Redirect("Contacts.aspx");
    }
    protected void LinkButtonLastCManager_Click(object sender, EventArgs e)
    {
        LinkButton myLB = (LinkButton)sender;
        Session["AcctNumber"] = myLB.CommandArgument;
        Session["Mode"] = "ViewManager";

        Response.Redirect("Contacts.aspx");
    }
    
    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList myDD = (DropDownList)sender;
        Session["CoNumber"] = myDD.SelectedValue;
    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        LinkButton myLB = (LinkButton)sender;
        Session["CustomerID"] = myLB.CommandArgument;
        Response.Redirect("CustomerDetail.aspx"); 
    }
    protected void TextBoxName_TextChanged(object sender, EventArgs e)
    {
        fillgrid();
    }
}
