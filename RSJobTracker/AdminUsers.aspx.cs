﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AdminUsers : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
           
        }
       
    }



    protected void ButtonAdd_Click(object sender, EventArgs e)
    {
        UsersXSDTableAdapters.UsersTableAdapter UTA = new UsersXSDTableAdapters.UsersTableAdapter();
        int x = UTA.Insert(TextBoxUserName.Text, "12345", 1, true, "", "", true, false, false);
        GridView1.DataBind();
    }
    protected void ButtonUpdate_Click(object sender, EventArgs e)
    {
      
        Button myBut = (Button)sender;
        TextBox UN= (TextBox)myBut.Parent.FindControl("TextBoxUserName");
        TextBox PW = (TextBox)myBut.Parent.FindControl("TextBoxPassword");
        DropDownList Site = (DropDownList)myBut.Parent.FindControl("DropDownListSiteID");
        CheckBox Active=(CheckBox)myBut.Parent.FindControl("CheckBoxActive");
        TextBox Email = (TextBox)myBut.Parent.FindControl("TextBoxEmail");
        TextBox Cell = (TextBox)myBut.Parent.FindControl("TextBoxCell");
        CheckBox Controller=(CheckBox)myBut.Parent.FindControl("CheckBoxController");
        CheckBox Manager=(CheckBox)myBut.Parent.FindControl("CheckBoxManager");
        CheckBox SystemAdmin=(CheckBox)myBut.Parent.FindControl("CheckBoxSystemAdmin");

        if (UN.Text != "" & PW.Text != "")
        {
            UsersXSDTableAdapters.UsersTableAdapter UTA = new UsersXSDTableAdapters.UsersTableAdapter();
            int x = UTA.UpdateQuery(UN.Text, PW.Text, Convert.ToInt32(Site.SelectedValue), Active.Checked, Email.Text, Cell.Text, Controller.Checked, Manager.Checked, SystemAdmin.Checked, Convert.ToInt32(myBut.CommandArgument));
        }
        GridView1.DataBind();


    }
}
