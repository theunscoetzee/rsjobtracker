﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Components_Menu : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

         if (Session["UserID"] != null)
        {
            PanelMenu.Visible = true;

            if (Session["UserIsSystemAdmin"].ToString() == "True")
            {
                ButtonAdmin.Enabled = true;
            }
            else
            {
                ButtonAdmin.Enabled = false;
            }

            if (Session["UserIsManager"].ToString() == "True")
            {
                ButtonReports.Enabled = true;
            }
            else
            {
                ButtonReports.Enabled = false;
            }

            if (Session["UserIsController"].ToString() == "True")
            {
                ButtonJobs.Enabled = true;
            }
            else
            {
                ButtonJobs.Enabled = false;
            }

        }
        else
        {
            Response.Write("You are not logged in. <a href='login.aspx'>Please logon</a>");
            Response.End();
        }
    


       
        if (Session["MenuOption"] != null)
        {
            if (Session["MenuOption"].ToString() == "Jobs") { MultiView1.Visible = false; }
	    if (Session["MenuOption"].ToString() == "Assets") { MultiView1.Visible = false; }
            if (Session["MenuOption"].ToString() == "Reports") { MultiView1.ActiveViewIndex = 0; }
            if (Session["MenuOption"].ToString()=="Admin") { MultiView1.ActiveViewIndex=1;}
        }

        
    }
    protected void ButtonJobs_Click(object sender, EventArgs e)
    {
        Session["MenuOption"] = "Jobs";
        MultiView1.Visible = false;
        Response.Redirect("~/TicketList.aspx");
    }
    protected void ButtonReports_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 0;
        Session["MenuOption"] = "Reports";
        MultiView1.Visible = true;
        Response.Redirect("~/ReportsMain.aspx");
    }
    protected void ButtonAdmin_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 1;
        Session["MenuOption"] = "Admin";
        MultiView1.Visible = true;
    }
    protected void ButtonLogOff_Click(object sender, EventArgs e)
    {
        Session.Clear ();
        Response.Redirect("~/Login.aspx");
    }
    protected void btnAssets_Click(object sender, EventArgs e)
    {
        Session["MenuOption"] = "Assets";
        MultiView1.Visible = false;
        Response.Redirect("~/AssetsList.aspx");
    }


    protected void ButtonWorkshop_Click(object sender, EventArgs e)
    {
        Response.Redirect("WorkshopTicketList.aspx");
    }
}