﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Menu.ascx.cs" Inherits="Components_Menu" %>
 <asp:Panel ID="PanelMenu" runat="server">
    
<table>
   
<tr>
<td>
<asp:Button 
        ID="ButtonJobs" runat="server" Text="Jobs" Width="70px" 
        onclick="ButtonJobs_Click" />
<asp:Button 
        ID="btnAssets" runat="server" Text="Assets" Width="70px" onclick="btnAssets_Click" 
        />
<asp:Button 
        ID="ButtonReports" runat="server" Text="Reports" Width="70px" 
        onclick="ButtonReports_Click" />
<asp:Button 
        ID="ButtonAdmin" runat="server" Text="Admin" Width="70px" 
        onclick="ButtonAdmin_Click" />
        <asp:Button 
        ID="ButtonLogOff" runat="server" Text="Logout" Width="70px" 
        onclick="ButtonLogOff_Click" />
    &nbsp;
    <asp:Button ID="ButtonWorkshop" runat="server" onclick="ButtonWorkshop_Click" 
        Text="Workshop" Width="70px" />
</td></tr>

</table>
<asp:MultiView ID="MultiView1" runat="server">
    <asp:View ID="View1" runat="server">
        &nbsp; <a href="Reporting/ClientActivityReport.aspx">Client Activity</a> | <a href="Reporting/FuelReport.aspx">Fuel</a> | <a href="Reporting/GeneratorReport.aspx">Generators</a>
    </asp:View>
    <asp:View ID="View2" runat="server">
        &nbsp; <a href="AdminClientsSites.aspx">Clients -&gt; Sites</a> | <a href="AdminGens.aspx">Generators</a> | <a href="AdminBowsers.aspx">Bowsers</a> | <a href="AdminSitesDriversVehicles.aspx">RS Sites Drivers Vehicles</a> | <a href="AdminAssets.aspx">Assets</a> | <a href="AdminUsers.aspx">Users</a>
</asp:View>
</asp:MultiView>
</asp:Panel>
