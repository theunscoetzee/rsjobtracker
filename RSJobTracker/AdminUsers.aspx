﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="AdminUsers.aspx.cs" Inherits="AdminUsers" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script language="javascript" type="text/javascript">
    window.setTimeout("DoPostBack();", 50000);
    function DoPostBack() {
        document.forms[0].submit();
    }
    </script>
    </asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h1><asp:Label ID="lblHeading" runat="server" Text=""></asp:Label></h1>
    
    
    <asp:Panel ID="PanelListView" runat="server">
    </asp:Panel>
        Generators:<br />
    Add User Name:<br />
&nbsp;<asp:TextBox ID="TextBoxUserName" runat="server" 
    style="font-size: xx-small"></asp:TextBox>
        <asp:Button ID="ButtonAdd" runat="server" Height="19px" Text="Add" 
            onclick="ButtonAdd_Click" />
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </asp:ToolkitScriptManager>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
            DataSourceID="ObjectDataSourceSites" Width="964px" 
            >
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:BoundField DataField="UserID" HeaderText="UserID" 
                    SortExpression="UserID" InsertVisible="False" ReadOnly="True" />
                <asp:TemplateField HeaderText="UserName" SortExpression="UserName">
                    <ItemTemplate>
                       <asp:TextBox ID="TextBoxUserName" runat="server" Text='<%# Bind("UserName") %>'></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Password" SortExpression="Password">
                    <ItemTemplate>
                        <asp:TextBox ID="TextBoxPassword" runat="server" Text='<%# Bind("Password") %>'></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="RSGSite" SortExpression="RSGSiteID">
                    
                    <ItemTemplate>
                        
                        <asp:DropDownList ID="DropDownListSiteID" runat="server" 
                            DataSourceID="ObjectDataSourceRSSites" DataTextField="RSGSiteName" 
                            DataValueField="RSGSiteID" SelectedValue='<%# Bind("RSGSiteID") %>'>
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Active" SortExpression="Active">
                    
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBoxActive" runat="server" Checked='<%# Bind("Active") %>' 
                             />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Email" SortExpression="Email">
                    
                    <ItemTemplate>
                        <asp:TextBox ID="TextBoxEmail" runat="server" Text='<%# Bind("Email") %>'></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Cell" SortExpression="Cell">
                    <ItemTemplate>
                        <asp:TextBox ID="TextBoxCell" runat="server" Text='<%# Bind("Cell") %>'></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Controller" SortExpression="Controller">
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBoxController" runat="server" Checked='<%# Bind("Controller") %>' 
                            />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Manager" SortExpression="Manager">
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBoxManager" runat="server" Checked='<%# Bind("Manager") %>' 
                           />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="SystemAdmin" SortExpression="SystemAdmin">
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBoxSystemAdmin" runat="server" 
                            Checked='<%# Bind("SystemAdmin") %>'  />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Button ID="ButtonUpdate" runat="server" 
                            CommandArgument='<%# Eval("UserID") %>' onclick="ButtonUpdate_Click" 
                            Text="Update" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    <asp:ObjectDataSource ID="ObjectDataSourceSites" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        TypeName="UsersXSDTableAdapters.UsersTableAdapter" InsertMethod="Insert">
        <InsertParameters>
            <asp:Parameter Name="UserName" Type="String" />
            <asp:Parameter Name="Password" Type="String" />
            <asp:Parameter Name="RSGSiteID" Type="Int32" />
            <asp:Parameter Name="Active" Type="Boolean" />
            <asp:Parameter Name="Email" Type="String" />
            <asp:Parameter Name="Cell" Type="String" />
            <asp:Parameter Name="Controller" Type="Boolean" />
            <asp:Parameter Name="Manager" Type="Boolean" />
            <asp:Parameter Name="SystemAdmin" Type="Boolean" />
        </InsertParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceRSSites" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        TypeName="LookupsXSDTableAdapters.RSGSitesTableAdapter">
    </asp:ObjectDataSource>
    </asp:Content>
