﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["UserID"] = null;
        }
    }
    protected void ButtonCancel_Click(object sender, EventArgs e)
    {
        TextBoxUsername.Text = "";
        TextBoxPassword.Text = "";
    }
    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        LabelMessage.Text = "";
        //Session["UserID"] = "1";
        //Response.Redirect("TicketList.aspx");

        if (TextBoxPassword.Text != "" & TextBoxUsername.Text != "")
        {
            UsersXSDTableAdapters.UsersTableAdapter UTA = new UsersXSDTableAdapters.UsersTableAdapter();
            UsersXSD.UsersDataTable UDT = UTA.GetDataByUNPW(TextBoxUsername.Text, TextBoxPassword.Text);
            if (UDT.Rows.Count > 0)
            {
                UsersXSD.UsersRow URow = (UsersXSD.UsersRow)UDT[0];
                Session["UserName"] = URow.UserName;
                Session["UserID"] = URow.UserID;
                Session["UserIsController"] = URow.Controller;
                Session["UserIsManager"] = URow.Manager;
                Session["UserIsSystemAdmin"] = URow.SystemAdmin;
                Session["MenuOpion"] = "Jobs";
                Response.Redirect("TicketList.aspx");

            }
        }
        else
        {
            TextBoxPassword.Text = "";
        }

        
       
                   
        
    }
    
   

    
    
}
