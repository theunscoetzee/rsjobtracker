﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="AdminAssets.aspx.cs" Inherits="AdminUsers" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script language="javascript" type="text/javascript">
    window.setTimeout("DoPostBack();", 50000);
    function DoPostBack() {
        document.forms[0].submit();
    }
    </script>
    </asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h1><asp:Label ID="lblHeading" runat="server" Text=""></asp:Label></h1>
    
    
    <asp:Panel ID="PanelListView" runat="server">
    </asp:Panel>
        <asp:Label ID="lblSite" runat="server" Text="Site: "></asp:Label>
                        <asp:DropDownList ID="ddlSite" runat="server" AutoPostBack="True" 
                            DataSourceID="odsSites" DataTextField="RSGSiteName" DataValueField="RSGSiteID" 
                            >
                        </asp:DropDownList>
    
    <asp:ObjectDataSource ID="odsSites" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        TypeName="AssetsTableAdapters.RSGSitesTableAdapter" InsertMethod="Insert">
        <InsertParameters>
            <asp:Parameter Name="RSGSiteName" Type="String" />
        </InsertParameters>
    </asp:ObjectDataSource>
    
    <br />
    <br />

    <div id="Container"
        Generators:<br />
    Add Asset:<br />
&nbsp;<asp:Label ID="lblAssetName" runat="server" Text=" Asset Name:"></asp:Label>
    <asp:TextBox ID="txtAssetName" runat="server" 
    style="font-size: xx-small"></asp:TextBox>
        <asp:Label ID="lblActive" runat="server" Text="Active/In-Active"></asp:Label>
    <asp:CheckBox ID="cbActive" runat="server" />
        <asp:Button ID="ButtonAdd" runat="server" Height="19px" Text="Add" 
            onclick="ButtonAdd_Click" />
            </div>

        <br />
        <br />
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </asp:ToolkitScriptManager>
        <asp:GridView ID="gvAssets" runat="server" AutoGenerateColumns="False" 
            DataSourceID="odsAssets" Width="964px" DataKeyNames="AssetID" 
        onrowdatabound="gvAssets_RowDataBound" onselectedindexchanged="gvAssets_SelectedIndexChanged" 
            >
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:BoundField DataField="AssetID" HeaderText="AssetID" 
                    SortExpression="AssetID" InsertVisible="False" ReadOnly="True" />
                <asp:BoundField DataField="Description" HeaderText="Description" 
                    SortExpression="Description" />
                <asp:TemplateField HeaderText="Active" SortExpression="Active">
                    <EditItemTemplate>
                        <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Active") %>' />
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Button ID="btnActive" runat="server" 
                            CommandArgument='<%# Eval("AssetID") %>' Height="19px" 
                            onclick="btnActive_Click" Text="Active" 
                             />
                        <asp:Label ID="lblActive" runat="server" Visible="false" Text='<%# Bind("Active") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    <asp:ObjectDataSource ID="odsAssets" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByAssetSite" 
        TypeName="AssetsTableAdapters.AssetsTableAdapter">
        <SelectParameters>
            <asp:ControlParameter ControlID="ddlSite" Name="RSSiteID" 
                PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    </asp:Content>
