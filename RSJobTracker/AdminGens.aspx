﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="AdminGens.aspx.cs" Inherits="AdminGens" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script language="javascript" type="text/javascript">
    window.setTimeout("DoPostBack();", 50000);
    function DoPostBack() {
        document.forms[0].submit();
    }
    </script>
    <style type="text/css">
        .style4
        {
            width: 560px;
        }
    </style>
    </asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h1><asp:Label ID="lblHeading" runat="server" Text=""></asp:Label></h1>
    
    
    <table>
    <tr>
    <td bgcolor="#DDDDDD">
    Clients
    </td>
    <td bgcolor="#EEEEEE">
    Regions
    </td>
    <td bgcolor="#DDDDDD">
    Sub Regions
    </td>
    <td class="style4" bgcolor="#EEEEEE">
        <br />
        
    </td>
    </tr>
    <tr>
    <td valign="top" bgcolor="#DDDDDD">
        <asp:ListBox ID="ListBoxClients" runat="server" AutoPostBack="True" 
            DataSourceID="ObjectDataSourceClients" DataTextField="ClientName" 
            DataValueField="ClientID" 
            onselectedindexchanged="ListBoxClients_SelectedIndexChanged"></asp:ListBox>
        <br />
    </td>
    <td valign="top" bgcolor="#EEEEEE">
        <asp:ListBox ID="ListBoxRegions" runat="server" AutoPostBack="True" 
            DataSourceID="ObjectDataSourceRegions" DataTextField="ClientRegionName" 
            DataValueField="ClientRegionID" 
            onselectedindexchanged="ListBoxRegions_SelectedIndexChanged"></asp:ListBox>
        <br />
    </td>
    <td valign="top" bgcolor="#DDDDDD">
        <asp:ListBox ID="ListBoxSubRegions" runat="server" AutoPostBack="True" 
            DataSourceID="ObjectDataSourceSubRegions" DataTextField="SubRegionName" 
            DataValueField="ClientSubRegionID" 
            onselectedindexchanged="ListBoxSubRegions_SelectedIndexChanged">
        </asp:ListBox>
        <br />
    </td>
    <td class="style4" bgcolor="#EEEEEE">
        &nbsp;</td>
    </tr>
    </table>
    
    
    <asp:Panel ID="PanelListView" runat="server">
    </asp:Panel>
        Generators:<br />
    <br />
&nbsp;Add Generator:<br />
&nbsp;<asp:TextBox ID="TextBoxGenReg" runat="server" style="font-size: xx-small">RegNum</asp:TextBox>
        <asp:Button ID="ButtonAdd" runat="server" Height="19px" Text="Add" 
            onclick="ButtonAdd_Click" />
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </asp:ToolkitScriptManager>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
            DataSourceID="ObjectDataSourceSites" Width="781px" 
            onrowdatabound="GridView1_RowDataBound">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:TemplateField HeaderText="ClientRegionName" 
                    SortExpression="ClientRegionName">
                    <ItemTemplate>
                        <asp:DropDownList ID="DropDownListRegion" runat="server" AutoPostBack="True" 
                            DataSourceID="ObjectDataSourceRegions" DataTextField="ClientRegionName" 
                            DataValueField="ClientRegionID" 
                            onselectedindexchanged="DropDownListRegion_SelectedIndexChanged" 
                            SelectedValue='<%# Bind("ClientRegionID") %>'>
                        </asp:DropDownList>
                        <asp:Label ID="lblGenID" runat="server" Text='<%# Eval("GeneratorID") %>'></asp:Label>
                    </ItemTemplate>
                    
                </asp:TemplateField>
                <asp:TemplateField HeaderText="SubRegionName" SortExpression="SubRegionName">
                    <ItemTemplate>
                        <asp:DropDownList ID="DropDownListSubRegion" runat="server" AutoPostBack="True" 
                            onselectedindexchanged="DropDownListSubRegion_SelectedIndexChanged">
                        </asp:DropDownList>
                        <asp:Label ID="lblSubRegID" runat="server" 
                            Text='<%# Eval("ClientSubRegionID") %>' Visible="false"></asp:Label>
                    </ItemTemplate>
                    
                    
                </asp:TemplateField>
                <asp:TemplateField HeaderText="GeneratorRegNumber" 
                    SortExpression="GeneratorRegNumber">
                   
                    <ItemTemplate>
                      <asp:TextBox ID="TextBoxRegNum" runat="server" ontextchanged="TextBoxRegNum_TextChanged" AutoPostBack="True"
                            Text='<%# Bind("GeneratorRegNumber") %>'></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="LastLocation" SortExpression="LastLocation">
                    <ItemTemplate>
                       <asp:TextBox ID="TextBoxLastLocation" runat="server" 
                            Text='<%# Bind("LastLocation") %>' AutoPostBack="True" 
                            ontextchanged="TextBoxLastLocation_TextChanged" Width="100px"></asp:TextBox>
                    </ItemTemplate>
                   
                </asp:TemplateField>
                <asp:TemplateField HeaderText="LastLocationDate" 
                    SortExpression="LastLocationDate">
                    
                    <ItemTemplate>
                        <asp:TextBox ID="TextBoxLastLocationDate" runat="server" 
                            ontextchanged="TextBoxLastLocation_TextChanged" AutoPostBack="True" 
                            Text='<%# Bind("LastLocationDate", "{0:d}") %>' Width="80px"></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="LastHours" SortExpression="LastHours">
                    
                    <ItemTemplate>
                        <asp:TextBox ID="TextBoxLastHours" runat="server" Text='<%# Bind("LastHours") %>' Width="50px" ontextchanged="TextBoxLastLocation_TextChanged" AutoPostBack="True" ></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="LastHoursAt" SortExpression="LastHoursAt">
                    <ItemTemplate>
                        <asp:TextBox ID="TextBoxLastHoursAt" runat="server" 
                            Text='<%# Bind("LastHoursAt", "{0:d}") %>' 
                            ontextchanged="TextBoxLastLocation_TextChanged" AutoPostBack="True" 
                            Width="80px" ></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="LastHoursJobID" SortExpression="LastHoursJobID">
                    <ItemTemplate>
                        <asp:TextBox ID="TextBoxLastHoursJobID" runat="server" Text='<%# Bind("LastHoursJobID") %>' Width="70px" ontextchanged="TextBoxLastLocation_TextChanged" AutoPostBack="True" ></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="LastServiceType" 
                    SortExpression="LastServiceType">
                    <ItemTemplate>
                        <asp:TextBox ID="TextBoxLastServiceType" runat="server" Text='<%# Bind("LastServiceType") %>' Width="80px" ontextchanged="TextBoxLastLocation_TextChanged" AutoPostBack="True" ></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="LastServiceAt" SortExpression="LastServiceAt">
                    <ItemTemplate>
                        <asp:TextBox ID="TextBoxLastServiceAt" runat="server" 
                            Text='<%# Bind("LastServiceAt", "{0:d}") %>' 
                            ontextchanged="TextBoxLastLocation_TextChanged" AutoPostBack="True" 
                            Width="80px" ></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="LastServiceHours" 
                    SortExpression="LastServiceHours">
                    <ItemTemplate>
                        <asp:TextBox ID="TextBoxLastServiceHours" runat="server" ontextchanged="TextBoxLastLocation_TextChanged" Width="80px"  AutoPostBack="True" 
                            Text='<%# Bind("LastServiceHours") %>'></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="LastServcieJobID" 
                    SortExpression="LastServcieJobID">
                    <ItemTemplate>
                        <asp:TextBox ID="TextBoxLastServcieJobID" runat="server" ontextchanged="TextBoxLastLocation_TextChanged" AutoPostBack="True" Width="80px" 
                            Text='<%# Bind("LastServcieJobID") %>'></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="InService" SortExpression="InService">
                       <ItemTemplate>
                           <asp:CheckBox ID="CheckBoxInService" runat="server" 
                               Checked='<%# Bind("InService") %>' AutoPostBack="True" 
                               oncheckedchanged="CheckBoxInService_CheckedChanged" />
                    </ItemTemplate>
                        
                    
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Active" SortExpression="Active">
                    
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBoxActive" runat="server" 
                            Checked='<%# Bind("Active") %>' AutoPostBack="True" 
                            oncheckedchanged="CheckBoxActive_CheckedChanged" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    <asp:ObjectDataSource ID="ObjectDataSourceClients" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        TypeName="LookupsXSDTableAdapters.ClientsTableAdapter" 
    InsertMethod="Insert">
        <InsertParameters>
            <asp:Parameter Name="ClientName" Type="String" />
        </InsertParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceRegions" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByClientID" 
        TypeName="LookupsXSDTableAdapters.ClientRegionsTableAdapter">
        <SelectParameters>
            <asp:ControlParameter ControlID="ListBoxClients" DefaultValue="0" 
                Name="ClientID" PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceSites" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByRegSubReg" 
        TypeName="LookupsXSDTableAdapters.GeneratorsTableAdapter" 
    InsertMethod="Insert">
        <InsertParameters>
            <asp:Parameter Name="ClientID" Type="Int32" />
            <asp:Parameter Name="ClientRegionID" Type="Int32" />
            <asp:Parameter Name="GeneratorRegNumber" Type="String" />
            <asp:Parameter Name="LastLocation" Type="String" />
            <asp:Parameter Name="LastLocationDate" Type="DateTime" />
            <asp:Parameter Name="LastHours" Type="Int32" />
            <asp:Parameter Name="LastHoursAt" Type="DateTime" />
            <asp:Parameter Name="ClientSubRegionID" Type="Int32" />
            <asp:Parameter Name="LastHoursJobID" Type="Int32" />
            <asp:Parameter Name="LastServiceType" Type="Int32" />
            <asp:Parameter Name="LastServiceAt" Type="DateTime" />
            <asp:Parameter Name="LastServiceHours" Type="Int32" />
            <asp:Parameter Name="LastServcieJobID" Type="Int32" />
            <asp:Parameter Name="InService" Type="Boolean" />
            <asp:Parameter Name="Active" Type="Boolean" />
        </InsertParameters>
        <SelectParameters>
            <asp:ControlParameter ControlID="ListBoxRegions" DefaultValue="0" 
                Name="RegionID" PropertyName="SelectedValue" Type="Int32" />
            <asp:ControlParameter ControlID="ListBoxSubRegions" DefaultValue="0" 
                Name="SubRegionID" PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceSubRegions" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByClientRegionID" 
        TypeName="LookupsXSDTableAdapters.ClientSubRegionsTableAdapter" 
    InsertMethod="Insert">
        <InsertParameters>
            <asp:Parameter Name="ClientRegionID" Type="Int32" />
            <asp:Parameter Name="SubRegionName" Type="String" />
        </InsertParameters>
        <SelectParameters>
            <asp:ControlParameter ControlID="ListBoxRegions" Name="ClientRegionID" 
                PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    </asp:Content>
