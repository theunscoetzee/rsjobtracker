﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AdminUsers : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
           
        }
       
    }



    protected void ButtonAdd_Click(object sender, EventArgs e)
    {
        int BookedByUserID = Convert.ToInt32(Session["UserID"]);

        AssetsTableAdapters.AssetsTableAdapter ATA = new AssetsTableAdapters.AssetsTableAdapter();

        if(ddlSite.SelectedValue == "1")
        {
            int? Drivers = 61;

            ATA.InsertAsset(Convert.ToInt32(ddlSite.SelectedValue), txtAssetName.Text, cbActive.Checked, Drivers, BookedByUserID);
        }
        else if (ddlSite.SelectedValue == "2")
        {
            int? Drivers = 62;

            ATA.InsertAsset(Convert.ToInt32(ddlSite.SelectedValue), txtAssetName.Text, cbActive.Checked, Drivers, BookedByUserID);
        }
        else if (ddlSite.SelectedValue == "6")
        {
            int? Drivers = 63;

            ATA.InsertAsset(Convert.ToInt32(ddlSite.SelectedValue), txtAssetName.Text, cbActive.Checked, Drivers, BookedByUserID);
        }


        gvAssets.DataBind();
    }

    
    protected void btnActive_Click(object sender, EventArgs e)
    {
        AssetsTableAdapters.AssetsTableAdapter ATA = new AssetsTableAdapters.AssetsTableAdapter();

        
        Button myBtn = (Button)sender;

        if (myBtn.Text == "Active")
        {
            ATA.UpdateQueryActive(Convert.ToBoolean(0), Convert.ToInt32(myBtn.CommandArgument));
        }
        else
        {
            ATA.UpdateQueryActive(Convert.ToBoolean(1), Convert.ToInt32(myBtn.CommandArgument));

        }
        gvAssets.DataBind();
    }
    protected void gvAssets_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void gvAssets_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lblActive = (Label)e.Row.FindControl("lblActive");            
            Button btnActive = (Button)e.Row.FindControl("btnActive");

            if (lblActive.Text == "False")
            {
                btnActive.Text = "In-Active";
                btnActive.BackColor = System.Drawing.Color.Red;
            }
            if (lblActive.Text == "True")
            {
                btnActive.Text = "Active";
                btnActive.BackColor = System.Drawing.Color.Green;
            }
        }
    }
}
