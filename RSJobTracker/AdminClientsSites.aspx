﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="AdminClientsSites.aspx.cs" Inherits="AdminClientsSites" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script language="javascript" type="text/javascript">
    window.setTimeout("DoPostBack();", 50000);
    function DoPostBack() {
        document.forms[0].submit();
    }
    </script>
    <style type="text/css">
        .style4
        {
            width: 560px;
        }
    </style>
    </asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h1><asp:Label ID="lblHeading" runat="server" Text=""></asp:Label></h1>
    
    
    <table>
    <tr>
    <td bgcolor="#DDDDDD">
    Clients
    </td>
    <td bgcolor="#EEEEEE">
    Regions
    </td>
    <td bgcolor="#DDDDDD">
    Sub Regions
    </td>
    <td class="style4" bgcolor="#EEEEEE">
        Fiter for (type part of site code or site name or % for all)<br />
        
    </td>
    </tr>
    <tr>
    <td valign="top" bgcolor="#DDDDDD">
        <asp:ListBox ID="ListBoxClients" runat="server" AutoPostBack="True" 
            DataSourceID="ObjectDataSourceClients" DataTextField="ClientName" 
            DataValueField="ClientID" 
            onselectedindexchanged="ListBoxClients_SelectedIndexChanged"></asp:ListBox>
        <br />
        Add Client:<br />
        <asp:TextBox ID="TextBoxAddClient" runat="server" style="font-size: xx-small">ClientName</asp:TextBox>
        &nbsp;<asp:Button ID="ButtonAddClient" runat="server" Height="19px" Text="Add" 
            onclick="ButtonAddClient_Click" />
    </td>
    <td valign="top" bgcolor="#EEEEEE">
        <asp:ListBox ID="ListBoxRegions" runat="server" AutoPostBack="True" 
            DataSourceID="ObjectDataSourceRegions" DataTextField="ClientRegionName" 
            DataValueField="ClientRegionID" 
            onselectedindexchanged="ListBoxRegions_SelectedIndexChanged"></asp:ListBox>
        <br />
        Add Region:<br />
        <asp:TextBox ID="TextBoxAddRegion" runat="server" style="font-size: xx-small">Region</asp:TextBox>
        &nbsp;<asp:Button ID="ButtonAddRegion" runat="server" Height="19px" Text="Add" 
            onclick="ButtonAddRegion_Click" />
    </td>
    <td valign="top" bgcolor="#DDDDDD">
        <asp:ListBox ID="ListBoxSubRegions" runat="server" AutoPostBack="True" 
            DataSourceID="ObjectDataSourceSubRegions" DataTextField="SubRegionName" 
            DataValueField="ClientSubRegionID" 
            onselectedindexchanged="ListBoxSubRegions_SelectedIndexChanged">
        </asp:ListBox>
        <br />
        Add SubRegion:<br />
        <asp:TextBox ID="TextBoxAddSubRegion" runat="server" 
            style="font-size: xx-small">Region</asp:TextBox>
        &nbsp;<asp:Button ID="ButtonAddSubRegion" runat="server" Height="19px" 
            Text="Add" onclick="ButtonAddSubRegion_Click" />
    </td>
    <td class="style4" bgcolor="#EEEEEE">
        <asp:TextBox ID="TextBoxFilterFor" runat="server" 
            ontextchanged="ListBoxSubRegions_SelectedIndexChanged">ext</asp:TextBox>
        <asp:Button ID="Button1" runat="server" 
            onclick="ListBoxSubRegions_SelectedIndexChanged" Text="Filter!" />
        </td>
    </tr>
    </table>
    
    
    <asp:Panel ID="PanelListView" runat="server">
    </asp:Panel>
    Sites:<br />
&nbsp;Add Site:<br />
<asp:TextBox ID="TextBoxCounty" runat="server" style="font-size: xx-small">County</asp:TextBox>
&nbsp;<asp:TextBox ID="TextBoxCode" runat="server" style="font-size: xx-small">SiteCode</asp:TextBox>
        <asp:TextBox ID="TextBoxSiteName" runat="server" style="font-size: xx-small" 
            Width="154px">SiteName</asp:TextBox>
        <asp:TextBox ID="TextBoxLat" runat="server" style="font-size: xx-small">Lat</asp:TextBox>
        <asp:TextBox ID="TextBoxLong" runat="server" 
    style="font-size: xx-small" Width="85px">Long</asp:TextBox>
        <asp:TextBox ID="TextBoxSiteOwner" runat="server" 
        style="font-size: xx-small">Site Owner</asp:TextBox>
        <asp:TextBox ID="TextBoxSitePriority" runat="server" 
        style="font-size: xx-small">Priority</asp:TextBox>
        <asp:TextBox ID="TextBoxSiteResponseTime" runat="server" 
        style="font-size: xx-small">Response Time</asp:TextBox>
        <asp:Button ID="ButtonAdd" runat="server" Height="19px" Text="Add" 
    onclick="ButtonAdd_Click" /><asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
            DataSourceID="ObjectDataSourceSites" Width="781px">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:TemplateField HeaderText="ClientSiteCounty" 
                    SortExpression="ClientSiteCounty">
                   
                    <ItemTemplate>
                        <asp:TextBox ID="TextBoxCounty" runat="server" 
                            Text='<%# Bind("ClientSiteCounty") %>'></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ClientSiteCode" SortExpression="ClientSiteCode">
                    
                    <ItemTemplate>
                        <asp:TextBox ID="TextBoxSiteCode" runat="server" Text='<%# Bind("ClientSiteCode") %>'></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ClientSiteName" SortExpression="ClientSiteName">
                  
                    <ItemTemplate>
                        <asp:TextBox ID="TextBoxSiteName" runat="server" Text='<%# Bind("ClientSiteName") %>'></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ClientSiteLat" SortExpression="ClientSiteLat">
                    
                    <ItemTemplate>
                        <asp:TextBox ID="TextBoxSiteLat" runat="server" Text='<%# Bind("ClientSiteLat") %>'></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ClientSiteLong" SortExpression="ClientSiteLong">
                    
                    <ItemTemplate>
                        <asp:TextBox ID="TextBoxSiteLong" runat="server" Text='<%# Bind("ClientSiteLong") %>'></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ClientSiteOwner" 
                    SortExpression="ClientSiteOwner">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("ClientSiteOwner") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:TextBox ID="TextBoxSiteOwner" runat="server" 
                            Text='<%# Bind("ClientSiteOwner") %>'></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="SitePriority" 
                    SortExpression="ClientSitePriority">
                    
                    <ItemTemplate>
                        <asp:TextBox ID="TextBoxPriority" runat="server" 
                            Text='<%# Bind("ClientSitePriority") %>' Width="50px"></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ResponseTime" 
                    SortExpression="ClientSiteResponseTime">
                    
                    <ItemTemplate>
                        <asp:TextBox ID="TextBoxResponseTime" runat="server" Text='<%# Bind("ClientSiteResponseTime") %>' Width="50px"></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="ClientSiteID" HeaderText="ClientSiteID" 
                    SortExpression="ClientSiteID" InsertVisible="False" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Button ID="ButtonUpdate" runat="server" 
                            CommandArgument='<%# Eval("ClientSiteID") %>' Height="24px" 
                            onclick="ButtonUpdate_Click" Text="Update" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    <asp:ObjectDataSource ID="ObjectDataSourceClients" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        TypeName="LookupsXSDTableAdapters.ClientsTableAdapter" 
    InsertMethod="Insert">
        <InsertParameters>
            <asp:Parameter Name="ClientName" Type="String" />
        </InsertParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceRegions" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByClientID" 
        TypeName="LookupsXSDTableAdapters.ClientRegionsTableAdapter">
        <SelectParameters>
            <asp:ControlParameter ControlID="ListBoxClients" DefaultValue="0" 
                Name="ClientID" PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceSites" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataBySubRegionIDSiteFiler" 
        TypeName="LookupsXSDTableAdapters.ClientSitesTableAdapter">
        <SelectParameters>
            <asp:ControlParameter ControlID="ListBoxSubRegions" Name="ClientSubRegionID" 
                PropertyName="SelectedValue" Type="Int32" />
            <asp:ControlParameter ControlID="TextBoxFilterFor" DefaultValue="a" 
                Name="SiteFilter" PropertyName="Text" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceSubRegions" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByClientRegionID" 
        TypeName="LookupsXSDTableAdapters.ClientSubRegionsTableAdapter" 
    InsertMethod="Insert">
        <InsertParameters>
            <asp:Parameter Name="ClientRegionID" Type="Int32" />
            <asp:Parameter Name="SubRegionName" Type="String" />
        </InsertParameters>
        <SelectParameters>
            <asp:ControlParameter ControlID="ListBoxRegions" Name="ClientRegionID" 
                PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    </asp:Content>
