﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DriverShiftAllocation : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        
                
        if (!IsPostBack)
        {    
                if (Session["Day"] == null) { DropDownListDay.Text = DateTime.Now.Day.ToString(); Session["Day"] = DateTime.Now.Day.ToString(); } else { DropDownListDay.Text = Session["Day"].ToString(); }
                if (Session["Month"] == null) { DropDownListMonth.Text = DateTime.Now.Month.ToString(); Session["Month"] = DateTime.Now.Month.ToString(); } else { DropDownListMonth.Text = Session["Month"].ToString(); }
                if (Session["Year"] == null) { DropDownListYear.Text = DateTime.Now.Year.ToString(); Session["Year"] = DateTime.Now.Year.ToString(); } else { DropDownListYear.Text = Session["Year"].ToString(); }
               
            int DriverID = 0;
            int ShiftID = 0;
            if (Request.QueryString["DID"] != null)
            {
                DriverID = Convert.ToInt32(Request.QueryString["DID"]);
            }
            


            if (Request.QueryString["SID"] != null)
            {
                ShiftID = Convert.ToInt32( Request.QueryString["SID"]);
            }

            if (DriverID != 0 & ShiftID != 0)
            {
                //find selected shift and delete other shifts on that day
                DriversXSDTableAdapters.DriverShiftAllocationTableAdapter SATA = new DriversXSDTableAdapters.DriverShiftAllocationTableAdapter();
                DriversXSDTableAdapters.ShiftsTableAdapter STA = new DriversXSDTableAdapters.ShiftsTableAdapter();
                DriversXSD.ShiftsDataTable SDT = STA.GetDataByShiftID(ShiftID);
                DriversXSD.ShiftsRow SRow = (DriversXSD.ShiftsRow)SDT[0];
                DateTime ShiftDate = SRow.ShiftStart;

                SDT = STA.GetDataByYearMonthDay(Convert.ToInt32(SRow.ShiftStart.Year), Convert.ToInt32(SRow.ShiftStart.Month), Convert.ToInt32(SRow.ShiftStart.Day));
                for (int f = 0; f < SDT.Rows.Count; f++)
                {
                    SRow = (DriversXSD.ShiftsRow)SDT[f];
                    int t = SATA.DeleteQuery(DriverID, SRow.ShiftID);
                }

                
                


                DriversXSDTableAdapters.DriverShiftAllocationTableAdapter DSATA = new DriversXSDTableAdapters.DriverShiftAllocationTableAdapter();
                int x = DSATA.InsertQuery(DriverID, ShiftID);
                
            }

            if (Request.QueryString["DriverShiftID"] != null)
            {
                DriversXSDTableAdapters.DriverShiftAllocationTableAdapter SATA = new DriversXSDTableAdapters.DriverShiftAllocationTableAdapter();
                int t = SATA.DeleteDriverShiftID(Convert.ToInt32(Request.QueryString["DriverShiftID"].ToString()));
            }

            loadData();
        }
       
    }


    protected void loadDropdowns()
    {
       

    }

    protected void loadData()
    {
        string linetext;
     
        //get Shift IDs
        int shift06ID = 0;
        int shift18ID = 0;
        DriversXSDTableAdapters.ShiftsTableAdapter STA = new DriversXSDTableAdapters.ShiftsTableAdapter();
        DriversXSD.ShiftsDataTable SDT = STA.GetDataByYearMonthDay(Convert.ToInt32(DropDownListYear.Text), Convert.ToInt32(DropDownListMonth.Text), Convert.ToInt32(DropDownListDay.Text));
        for (int f = 0; f < SDT.Rows.Count; f++)
        {
            DriversXSD.ShiftsRow SRow = (DriversXSD.ShiftsRow)SDT[f];
            if (SRow.ShiftStart.Hour == 6) shift06ID = SRow.ShiftID;
            if (SRow.ShiftStart.Hour == 18) shift18ID = SRow.ShiftID;
        }

        Label1.Text = "<ul><table width=600 border=0  cellpadding=3 cellspacing=1><tr bgcolor=grey><td>Site</td><td>Driver</td><td><center> 06:00 Shift</td><td><center> 18:00 Shift</td></tr>";
        DriversXSDTableAdapters.DriverShiftAllocationTableAdapter DSATA = new DriversXSDTableAdapters.DriverShiftAllocationTableAdapter(); 
        DriversXSD.DriverShiftAllocationDataTable DSADT = DSATA.GetData(Convert.ToInt32(DropDownListYear.Text), Convert.ToInt32(DropDownListMonth.Text), Convert.ToInt32(DropDownListDay.Text));
        for (int x = 0; x < DSADT.Rows.Count; x++)
        {
            DriversXSD.DriverShiftAllocationRow DARow = (DriversXSD.DriverShiftAllocationRow)DSADT[x];

            linetext = "<tr bgcolor=#DDDDDD><td>" + DARow.RSGSiteName + "</td><td>" + DARow.RSGDriverName + "</td><td><center>";
            if (!DARow.IsShiftStartNull())
            {
                if (DARow.ShiftStart.Hour == 6)
                { linetext += "<center><font color=green size=4><b>  <a href='DriverShiftAllocation.aspx?DriverShiftID=" + DARow.DriverShiftID.ToString() + "'>[" + DARow.ShiftID.ToString() +"]</a>"; }
                else
                { linetext += "<center><a href='DriverShiftAllocation.aspx?DID=" + DARow.RSGDriverID + "&SID=" + shift06ID + "'> + </a>"; }

                
                linetext += "</td><td>";

                if (DARow.ShiftStart.Hour == 18)
                { linetext += "<center><font color=green size=4><b>  <a href='DriverShiftAllocation.aspx?DriverShiftID=" + DARow.DriverShiftID.ToString() + "'>[" + DARow.ShiftID.ToString() + "]</a>"; }
                else
                { linetext += "<center><a href='DriverShiftAllocation.aspx?DID=" + DARow.RSGDriverID + "&SID=" + shift18ID +  "'> + </a>"; }
             
                linetext += "</td>";
            }
            else
            {
                linetext += "<center><a href='DriverShiftAllocation.aspx?DID=" + DARow.RSGDriverID + "&SID=" + shift06ID + "'> + </a></td><td><center><a href='DriverShiftAllocation.aspx?DID=" + DARow.RSGDriverID + "&SID=" + shift18ID + "'> + </a></td>";
            }
            
            Label1.Text += linetext;
        }

        Label1.Text += "</table>";


    }



    protected void DropDownListDay_SelectedIndexChanged(object sender, EventArgs e)
    {

        Session["Day"] = DropDownListDay.Text;
        loadData();
    }
    protected void DropDownListMonth_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["Month"]=DropDownListMonth.Text;
        loadData();
    }
    protected void DropDownListYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["Year"] = DropDownListYear.Text;
        loadData();
    }
}
