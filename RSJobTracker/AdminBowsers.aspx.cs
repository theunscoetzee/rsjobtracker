﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AdminBowsers : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
           
        }
       
    }



    protected void ListBoxClients_SelectedIndexChanged(object sender, EventArgs e)
    {
        ListBoxRegions.DataBind();
        ListBoxSubRegions.DataBind();
        GridView1.DataBind();
        
    }
    protected void ListBoxRegions_SelectedIndexChanged(object sender, EventArgs e)
    {
        ListBoxSubRegions.DataBind();
        GridView1.DataBind();
    }
    protected void ListBoxSubRegions_SelectedIndexChanged(object sender, EventArgs e)
    {
        
        GridView1.DataBind();
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DropDownList ddReg = (DropDownList)e.Row.FindControl("DropDownListRegion");
            DropDownList ddSubReg = (DropDownList)e.Row.FindControl("DropDownListSubRegion");

            LookupsXSDTableAdapters.ClientSubRegionsTableAdapter CSTA = new LookupsXSDTableAdapters.ClientSubRegionsTableAdapter();
            LookupsXSD.ClientSubRegionsDataTable CSDT = CSTA.GetDataByClientRegionID(Convert.ToInt32(ddReg.SelectedValue));
            
            ListItem myLI = new ListItem("","");
            ddSubReg.Items.Add(myLI);

            for (int f = 0; f < CSDT.Rows.Count; f++)
            {
                LookupsXSD.ClientSubRegionsRow  CSRow = (LookupsXSD.ClientSubRegionsRow )CSDT[f];
                
                    myLI = new ListItem(CSRow.SubRegionName, CSRow.ClientSubRegionID.ToString());
                    ddSubReg.Items.Add(myLI);
                
            }
            Label LabelSubRegID = (Label)e.Row.FindControl("lblSubRegID");
            ddSubReg.SelectedValue=LabelSubRegID.Text;

        }
    }
    protected void DropDownListRegion_SelectedIndexChanged(object sender, EventArgs e)
    {
        //change region

        DropDownList myDD = (DropDownList)sender;
        Label mylbl = (Label)myDD.Parent.FindControl("lblBowserID");
        BowsersXSDTableAdapters.BowsersTableAdapter BTA = new BowsersXSDTableAdapters.BowsersTableAdapter();
        int x = BTA.UpdateRegionID(Convert.ToInt32(myDD.SelectedValue ),Convert.ToInt32(mylbl.Text));
        GridView1.DataBind();



    }

    protected void DropDownListSubRegion_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList myDD = (DropDownList)sender;
        Label mylbl = (Label)myDD.Parent.FindControl("lblBowserID");

        int? SubRegID = null;
        if (myDD.SelectedValue != "")
        {
            SubRegID = Convert.ToInt32(myDD.SelectedValue);
        }

        BowsersXSDTableAdapters.BowsersTableAdapter BTA = new BowsersXSDTableAdapters.BowsersTableAdapter();
        int x = BTA.UpdateSubRegionID(SubRegID, Convert.ToInt32(mylbl.Text));
        GridView1.DataBind();
        
    }

    protected void ButtonAdd_Click(object sender, EventArgs e)
    {
        if (TextBoxBowserReg.Text != "")
        {
            try
            {
                int? SubRegID = null;
                if (ListBoxSubRegions.SelectedValue != "")
                {
                    SubRegID = Convert.ToInt32(ListBoxSubRegions.SelectedValue);
                }
                BowsersXSDTableAdapters.BowsersTableAdapter BTA = new BowsersXSDTableAdapters.BowsersTableAdapter();
               
                int x = BTA.InsertQuery(Convert.ToInt32(ListBoxClients.SelectedValue), Convert.ToInt32(ListBoxRegions.SelectedValue), SubRegID, TextBoxBowserReg.Text, true);
                TextBoxBowserReg.Text = "";
            }
            catch
            {
                Response.Write("An error occured - generator could not be added. Perhpas already in database?");
            }
            GridView1.DataBind();
        }
    }
    
    protected void CheckBoxActive_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox myCB = (CheckBox)sender;
        Label mylbl = (Label)myCB.Parent.FindControl("lblBowserID");

        BowsersXSDTableAdapters.BowsersTableAdapter BTA = new BowsersXSDTableAdapters.BowsersTableAdapter();
        int x = BTA.UpdateActive(myCB.Checked, Convert.ToInt32(mylbl.Text));
        GridView1.DataBind();
    }
    protected void TextBoxLastLocation_TextChanged(object sender, EventArgs e)
    {
        TextBox myTB=(TextBox)sender;

        Label bowserID = (Label)myTB.Parent.FindControl("lblBowserID");
        TextBox LastLoc =(TextBox)myTB.Parent.FindControl("TextBoxLastLocation");
        TextBox LastLocDate = (TextBox)myTB.Parent.FindControl("TextBoxLastLocationDate");
        

        DateTime? dtLastLocDate = null;
        if(LastLocDate.Text != "") {dtLastLocDate = Convert.ToDateTime (LastLocDate.Text);}

        BowsersXSDTableAdapters.BowsersTableAdapter BTA = new BowsersXSDTableAdapters.BowsersTableAdapter();
        int x= BTA.UpdateFields(LastLoc.Text,dtLastLocDate,Convert.ToInt32(bowserID.Text));
        GridView1.DataBind();        
    }
}
