﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="DriversList.aspx.cs" Inherits="DriversList" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script language="javascript" type="text/javascript">
    window.setTimeout("DoPostBack();", 50000);
    function DoPostBack() {
        document.forms[0].submit();
    }
    </script>
    </asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h1>Drivers On/Off Shift</h1>
    
    <asp:Panel ID="panelJumpMenu" runat="server" BackColor="#CCCCCC" >
        <asp:Button ID="ButtonBack" runat="server" Text="Back" 
            onclick="ButtonDrivers_Click" Width="150px" />
        &nbsp;</asp:Panel>
    
    <asp:Panel ID="PanelFilter" runat="server">    &nbsp;&nbsp;</asp:Panel>
    
    
    
    <asp:Panel ID="PanelListView" runat="server">
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" PageSize="20" 
            style="font-size: x-small" onrowdatabound="GridView1_RowDataBound" 
            DataSourceID="ObjectDataSourceList" Width="900px" onselectedindexchanged="GridView1_SelectedIndexChanged" 
            >
            <AlternatingRowStyle BackColor="#DEDEDE" />
            <Columns>
                    <asp:BoundField DataField="RSGSiteName" HeaderText="RSGSiteName" 
                    SortExpression="RSGSiteName" />
                <asp:TemplateField HeaderText="RSGDriverName" SortExpression="RSGDriverName">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("RSGDriverName") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButtonDriverName" runat="server" 
                            CommandArgument='<%# Eval("RSGDriverID") %>' 
                            onclick="LinkButtonDriverName_Click" Text='<%# Eval("RSGDriverName") %>'></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Availabilty" HeaderText="Availabilty" 
                    ReadOnly="True" SortExpression="Availabilty" />
                <asp:TemplateField HeaderText="ShiftStarted" SortExpression="ShiftStarted">
                    <EditItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("ShiftStarted") %>'></asp:Label>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblShiftStarted" runat="server" 
                            Text='<%# Bind("ShiftStarted") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ShiftEnded" SortExpression="ShiftEnded">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("ShiftEnded") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblShiftEnded" runat="server" Text='<%# Bind("ShiftEnded") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                    <asp:TemplateField HeaderText="day(s) hh:mm:ss" SortExpression="[hh:mm:ss]">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("[[hh:mm:ss]]") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text='<%# Eval("days") %>'></asp:Label> 
                            &nbsp; <asp:Label ID="Label1" runat="server" Text='<%# Eval("hours") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Button ID="btnSetStatus" runat="server" Height="20px" 
                            Text="Set To OnShift" CommandArgument='<%# Eval("RSGDriverID") %>' 
                            onclick="btnSetStatus_Click" />
                    </ItemTemplate>
                </asp:TemplateField>

            </Columns>
            <HeaderStyle BackColor="#CCCCCC" />
            <RowStyle BackColor="White" />
        </asp:GridView>
    </asp:Panel>
    <br />
&nbsp;<asp:Panel ID="PanelDriverHistory" runat="server" Visible="False">
        <h1>Driver Shift History</asp:Label></h1>
        <asp:Button ID="Button1" runat="server" Text="Back" Width="150px" 
            onclick="Button1_Click" />
        <br />
        <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" PageSize="20" 
            style="font-size: x-small" 
            DataSourceID="ObjectDataSourceHistory" Width="900px" 
            >
            <AlternatingRowStyle BackColor="#DEDEDE" />
            <Columns>
                    <asp:BoundField DataField="RSGSiteName" HeaderText="RSGSiteName" 
                        SortExpression="RSGSiteName" />
                    <asp:BoundField DataField="RSGDriverName" HeaderText="RSGDriverName" 
                        SortExpression="RSGDriverName" />
                    <asp:TemplateField HeaderText="ShiftStarted" SortExpression="ShiftStarted">
                        
                        <ItemTemplate>
                            <asp:TextBox ID="TextBoxStart" runat="server" 
                                Text='<%# Bind("ShiftStarted") %>' ontextchanged="TextBoxStart_TextChanged"></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="StartLoggedBy" 
                        HeaderText="StartLoggedBy" 
                        SortExpression="StartLoggedBy" />
                    <asp:TemplateField HeaderText="ShiftEnded" SortExpression="ShiftEnded">
                       
                        <ItemTemplate>
                            <asp:TextBox ID="TextBoxEnd" runat="server" Text='<%# Bind("ShiftEnded") %>' 
                                ontextchanged="TextBoxEnd_TextChanged"></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="EndLoggedBy" 
                        HeaderText="EndLoggedBy" 
                        SortExpression="EndLoggedBy" />
                    <asp:BoundField DataField="[hh:mm:ss]" HeaderText="[hh:mm:ss]" ReadOnly="True" 
                        SortExpression="[hh:mm:ss]" />

            </Columns>
            <HeaderStyle BackColor="#CCCCCC" />
            <RowStyle BackColor="White" />
        </asp:GridView>
        <asp:Label ID="LabelDriverID" runat="server" Visible="False"></asp:Label>
        <br />
    </asp:Panel>
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </asp:ToolkitScriptManager>
    <asp:ObjectDataSource ID="ObjectDataSourceList" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        TypeName="DriversXSDTableAdapters.DriversShiftStatusTableAdapter">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceHistory" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByDriverID" 
        TypeName="DriversXSDTableAdapters.DriverShiftHistoryTableAdapter">
        <SelectParameters>
            <asp:ControlParameter ControlID="LabelDriverID" DefaultValue="0" 
                Name="DriverID" PropertyName="Text" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    </asp:Content>
