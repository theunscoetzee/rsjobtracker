﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="AdminBowsers.aspx.cs" Inherits="AdminBowsers" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script language="javascript" type="text/javascript">
    window.setTimeout("DoPostBack();", 50000);
    function DoPostBack() {
        document.forms[0].submit();
    }
    </script>
    <style type="text/css">
        .style4
        {
            width: 560px;
        }
    </style>
    </asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h1><asp:Label ID="lblHeading" runat="server" Text=""></asp:Label></h1>
    
    
    <table>
    <tr>
    <td bgcolor="#DDDDDD">
    Clients
    </td>
    <td bgcolor="#EEEEEE">
    Regions
    </td>
    <td bgcolor="#DDDDDD">
    Sub Regions
    </td>
    <td class="style4" bgcolor="#EEEEEE">
        <br />
        
    </td>
    </tr>
    <tr>
    <td valign="top" bgcolor="#DDDDDD">
        <asp:ListBox ID="ListBoxClients" runat="server" AutoPostBack="True" 
            DataSourceID="ObjectDataSourceClients" DataTextField="ClientName" 
            DataValueField="ClientID" 
            onselectedindexchanged="ListBoxClients_SelectedIndexChanged"></asp:ListBox>
        <br />
    </td>
    <td valign="top" bgcolor="#EEEEEE">
        <asp:ListBox ID="ListBoxRegions" runat="server" AutoPostBack="True" 
            DataSourceID="ObjectDataSourceRegions" DataTextField="ClientRegionName" 
            DataValueField="ClientRegionID" 
            onselectedindexchanged="ListBoxRegions_SelectedIndexChanged"></asp:ListBox>
        <br />
    </td>
    <td valign="top" bgcolor="#DDDDDD">
        <asp:ListBox ID="ListBoxSubRegions" runat="server" AutoPostBack="True" 
            DataSourceID="ObjectDataSourceSubRegions" DataTextField="SubRegionName" 
            DataValueField="ClientSubRegionID" 
            onselectedindexchanged="ListBoxSubRegions_SelectedIndexChanged">
        </asp:ListBox>
        <br />
    </td>
    <td class="style4" bgcolor="#EEEEEE">
        &nbsp;</td>
    </tr>
    </table>
    
    
    <asp:Panel ID="PanelListView" runat="server">
    </asp:Panel>
        Generators:<br />
    <br />
&nbsp;Add Generator:<br />
&nbsp;<asp:TextBox ID="TextBoxBowserReg" runat="server" 
    style="font-size: xx-small">RegNum</asp:TextBox>
        <asp:Button ID="ButtonAdd" runat="server" Height="19px" Text="Add" 
            onclick="ButtonAdd_Click" />
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </asp:ToolkitScriptManager>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
            DataSourceID="ObjectDataSourceSites" Width="781px" 
            onrowdatabound="GridView1_RowDataBound">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:TemplateField HeaderText="ClientRegionName" 
                    SortExpression="ClientRegionName">
                    <ItemTemplate>
                        <asp:DropDownList ID="DropDownListRegion" runat="server" AutoPostBack="True" 
                            DataSourceID="ObjectDataSourceRegions" DataTextField="ClientRegionName" 
                            DataValueField="ClientRegionID" 
                            onselectedindexchanged="DropDownListRegion_SelectedIndexChanged" 
                            SelectedValue='<%# Bind("ClientRegionID") %>'>
                        </asp:DropDownList>
                        <asp:Label ID="lblBowserID" runat="server" Text='<%# Eval("BowserID") %>'></asp:Label>
                    </ItemTemplate>
                    
                </asp:TemplateField>
                <asp:TemplateField HeaderText="SubRegionName" SortExpression="SubRegionName">
                    <ItemTemplate>
                        <asp:DropDownList ID="DropDownListSubRegion" runat="server" AutoPostBack="True" 
                            onselectedindexchanged="DropDownListSubRegion_SelectedIndexChanged">
                        </asp:DropDownList>
                        <asp:Label ID="lblSubRegID" runat="server" 
                            Text='<%# Eval("ClientSubRegionID") %>'></asp:Label>
                    </ItemTemplate>
                    
                    
                </asp:TemplateField>
                <asp:BoundField DataField="BowserReg" HeaderText="BowserReg" 
                    SortExpression="BowserReg" />
                <asp:TemplateField HeaderText="LastLocation" SortExpression="LastLocation">
                    <ItemTemplate>
                       <asp:TextBox ID="TextBoxLastLocation" runat="server" 
                            Text='<%# Bind("LastLocation") %>' AutoPostBack="True" 
                            ontextchanged="TextBoxLastLocation_TextChanged" Width="100px"></asp:TextBox>
                    </ItemTemplate>
                   
                </asp:TemplateField>
                <asp:TemplateField HeaderText="LastLocationDate" 
                    SortExpression="LastLocationDate">
                    
                    <ItemTemplate>
                        <asp:TextBox ID="TextBoxLastLocationDate" runat="server" 
                            ontextchanged="TextBoxLastLocation_TextChanged" AutoPostBack="True" 
                            Text='<%# Bind("LastLocationDate", "{0:d}") %>' Width="80px"></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
               <asp:TemplateField HeaderText="Active" SortExpression="Active">
                    
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBoxActive" runat="server" 
                            Checked='<%# Bind("Active") %>' AutoPostBack="True" 
                            oncheckedchanged="CheckBoxActive_CheckedChanged" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    <asp:ObjectDataSource ID="ObjectDataSourceClients" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        TypeName="LookupsXSDTableAdapters.ClientsTableAdapter" 
    InsertMethod="Insert">
        <InsertParameters>
            <asp:Parameter Name="ClientName" Type="String" />
        </InsertParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceRegions" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByClientID" 
        TypeName="LookupsXSDTableAdapters.ClientRegionsTableAdapter">
        <SelectParameters>
            <asp:ControlParameter ControlID="ListBoxClients" DefaultValue="0" 
                Name="ClientID" PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceSites" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByRegSubRegion" 
        TypeName="BowsersXSDTableAdapters.BowsersTableAdapter">
        <SelectParameters>
            <asp:ControlParameter ControlID="ListBoxRegions" DefaultValue="0" 
                Name="RegionID" PropertyName="SelectedValue" Type="Int32" />
            <asp:ControlParameter ControlID="ListBoxSubRegions" DefaultValue="0" 
                Name="SubRegionID" PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceSubRegions" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByClientRegionID" 
        TypeName="LookupsXSDTableAdapters.ClientSubRegionsTableAdapter" 
    InsertMethod="Insert">
        <InsertParameters>
            <asp:Parameter Name="ClientRegionID" Type="Int32" />
            <asp:Parameter Name="SubRegionName" Type="String" />
        </InsertParameters>
        <SelectParameters>
            <asp:ControlParameter ControlID="ListBoxRegions" Name="ClientRegionID" 
                PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    </asp:Content>
