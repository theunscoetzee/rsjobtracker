﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="AssetsList.aspx.cs" Inherits="AssetsList" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script language="javascript" type="text/javascript">
    window.setTimeout("DoPostBack();", 50000);
    function DoPostBack() {
        document.forms[0].submit();
    }
    </script>
    </asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h1>Assets</h1>
    

    <asp:Panel ID="panelJumpMenu" runat="server" BackColor="#CCCCCC" >
        <asp:Button ID="ButtonBack" runat="server" Text="Back" 
            Width="150px" onclick="ButtonBack_Click" />
        &nbsp;</asp:Panel>
    <asp:Panel ID="PanelListView" runat="server">
    
        <asp:Panel ID="PanelFilter0" runat="server">
            &nbsp;<table bgcolor="Black" cellpadding="1" cellspacing="1" style="width: 1300px">
                <tr>
                    <td bgcolor="#AAAAAA" colspan="2">
                        <strong>Filter for Assets</strong></td>
                </tr>
                <tr>
                    <td bgcolor="#CCCCCC">
                        Site</td>
                    <td bgcolor="#CCCCCC">
                        Asset</td>
                </tr>
                <tr>
                    <td bgcolor="#CCCCCC">
                        <asp:DropDownList ID="ddlSite" runat="server" AutoPostBack="True" 
                            DataSourceID="odsSites" DataTextField="RSGSiteName" DataValueField="RSGSiteID" 
                            onselectedindexchanged="ddlSite_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td bgcolor="#CCCCCC">
                        <asp:DropDownList ID="ddlAsset" runat="server" AutoPostBack="True" 
                            DataSourceID="odsAssets" DataTextField="Description" 
                            DataValueField="AssetID">
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
            &nbsp;</asp:Panel>

        <asp:GridView ID="gvAssets" runat="server" AutoGenerateColumns="False" PageSize="20" 
            style="font-size: x-small" 
            DataSourceID="ObjectDataSourceList" Width="900px" 
            DataKeyNames="AssetID" 
            >
            <AlternatingRowStyle BackColor="#DEDEDE" />
            <Columns>
                    <asp:TemplateField HeaderText="AssetID" InsertVisible="False" 
                        SortExpression="AssetID">
                        <EditItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("AssetID") %>'></asp:Label>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="lbAssetsHistory" runat="server" CommandArgument='<%# Eval("AssetID", "{0}") %>'
                                onclick="lbAssetsHistory_Click" Text='<%# Eval("AssetID", "{0}") %>'></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Description" HeaderText="Description" 
                        SortExpression="Description" />
                    <asp:TemplateField HeaderText="Booked for Driver" 
                        SortExpression="RSGDriverName">
                        <EditItemTemplate>
                            <asp:DropDownList ID="TextBox1" runat="server" Text='<%# Bind("RSGDriverName") %>'></asp:DropDownList>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:DropDownList Text='<%# Bind("RSGDriverID") %>' ID="DDLDriver" runat="server" DataSourceID="ODSDrivers" 
                                DataTextField="RSGDriverName" DataValueField="RSGDriverID"  >
                            </asp:DropDownList>
                            <asp:ObjectDataSource ID="ODSDrivers" runat="server" InsertMethod="Insert" 
                                OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataGetBySite" 
                                TypeName="AssetsTableAdapters.RSGDriversTableAdapter">
                                <InsertParameters>
                                    <asp:Parameter Name="RSGSiteID" Type="Int32" />
                                    <asp:Parameter Name="RSGDriverName" Type="String" />
                                    <asp:Parameter Name="Active" Type="Boolean" />
                                </InsertParameters>
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="ddlSite" DefaultValue="" Name="RSGSiteID" 
                                        PropertyName="SelectedValue" Type="Int32" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </ItemTemplate>
                    </asp:TemplateField>                    
                    <asp:TemplateField HeaderText="Booked Out Date" SortExpression="BookedOutAt">
                        <EditItemTemplate>
                            <asp:TextBox ID="lblBookedOutAt" runat="server" Text='<%# Bind("BookedOutAt") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblBookedOutAt" runat="server" Text='<%# Bind("BookedOutAt") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="UserName" HeaderText="Booked out by" 
                        SortExpression="UserName" />
                    <asp:TemplateField HeaderText="Book Assets" ShowHeader="False">
                        <ItemTemplate>
                            <asp:Button ID="btnBookAsset" runat="server" CausesValidation="False" 
                                Text="Book Asset" onclick="Button1_Click1" 
                                CommandArgument='<%# Eval("AssetID", "{0}") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>

            </Columns>
            <HeaderStyle BackColor="#CCCCCC" />
            <RowStyle BackColor="White" />
        </asp:GridView>
    </asp:Panel>
    <br />
&nbsp;<asp:Panel ID="PanelAssetHistory" runat="server" Visible="False">
        <h1>Asset History</h1>
        <asp:Button ID="Button1" runat="server" Text="Back" Width="150px" onclick="Button1_Click" 
             />
        <br />
        <br />
        <asp:Label runat="server" ID="lblAsset" Font-Bold="False" Font-Size="Large" 
            ForeColor="#666666">Asset Name: </asp:Label>
        <asp:Label ID="lblAssetName" runat="server" Font-Bold="False" Font-Size="Large" 
            ForeColor="#CC0000"></asp:Label>
        <br />
        <br />
        <asp:GridView ID="gvAssetHistory" runat="server" AutoGenerateColumns="False" 
            DataSourceID="odsAssetHistory" PageSize="20" style="font-size: x-small" 
            Width="900px">
            <AlternatingRowStyle BackColor="#DEDEDE" />
            <Columns>
                <asp:BoundField DataField="RSGDriverName" HeaderText="Driver Name" 
                    SortExpression="RSGDriverName" />
                <asp:BoundField DataField="UserName" HeaderText="Booked By Name" 
                    SortExpression="UserName" />
                <asp:BoundField DataField="DateAlocated" HeaderText="Date Alocated" 
                    SortExpression="DateAlocated" />
            </Columns>
            <HeaderStyle BackColor="#CCCCCC" />
            <RowStyle BackColor="White" />
        </asp:GridView>
        <br />
        <br />
        <asp:Label ID="lblAssetID" runat="server" Visible="False"></asp:Label>
        <br />
    </asp:Panel>
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </asp:ToolkitScriptManager>
    <asp:ObjectDataSource ID="ObjectDataSourceList" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataBySite" 
        TypeName="AssetsTableAdapters.AssetsTableAdapter">
        <SelectParameters>
            <asp:ControlParameter ControlID="ddlSite" 
                Name="RSSiteID" PropertyName="SelectedValue" Type="Int32" />
            <asp:ControlParameter ControlID="ddlAsset" Name="AssetID" 
                PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    
    <asp:ObjectDataSource ID="odsAssetHistory" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByDriverID" 
        TypeName="AssetsTableAdapters.AssetsHistoryTableAdapter">
        <SelectParameters>
            <asp:ControlParameter ControlID="lblAssetID" DefaultValue="0" 
                Name="AssetID" PropertyName="Text" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    
    <asp:ObjectDataSource ID="odsSites" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        TypeName="AssetsTableAdapters.RSGSitesTableAdapter" InsertMethod="Insert">
        <InsertParameters>
            <asp:Parameter Name="RSGSiteName" Type="String" />
        </InsertParameters>
    </asp:ObjectDataSource>
    
    <asp:ObjectDataSource ID="odsAssets" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        TypeName="AssetsTableAdapters.AssetsTableAdapter">
    </asp:ObjectDataSource>
    
    </asp:Content>
