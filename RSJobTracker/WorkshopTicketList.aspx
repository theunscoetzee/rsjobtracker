﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="WorkshopTicketList.aspx.cs" Inherits="WorkshopTicketList" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<%@ Register assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script language="javascript" type="text/javascript">
    window.setTimeout("DoPostBack();", 50000);
    function DoPostBack() {
        document.forms[0].submit();
    }
    </script>
    <style type="text/css">
        .style4
        {
            width: 190px;
        }
    </style>
    </asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:Panel ID="panelJumpMenu" runat="server" BackColor="#AAAAAA" Visible="false" >
       
    </asp:Panel>
    <asp:Panel ID="PanelFilter" runat="server"><table bgcolor="Black" 
            cellpadding="1" cellspacing="1" style="width: 1300px">
        <tr>
            <td bgcolor="#AAAAAA" colspan="6">
                <strong>Filter for Job(s)</strong></td>
        </tr>
        <tr>
            <td bgcolor="#CCCCCC">
                Workshop</td>
            <td bgcolor="#CCCCCC">
                Client</td>
            <td bgcolor="#CCCCCC">
                CallType</td>
            <td bgcolor="#CCCCCC" class="style4">
                Job Received</td>
            <td bgcolor="#CCCCCC">
                JobCompleted/Cancelled</td>
            <td bgcolor="#CCCCCC">
                &nbsp;</td>
        </tr>
        <tr>
            <td bgcolor="#CCCCCC">
                <asp:DropDownList ID="DropDownListWorkshop" runat="server">
                </asp:DropDownList>
            </td>
            <td bgcolor="#CCCCCC">
                <asp:DropDownList ID="DropDownListClient" runat="server" AutoPostBack="True" 
                    onselectedindexchanged="DropDownListClient_SelectedIndexChanged" 
                    style="height: 22px">
                </asp:DropDownList>
            </td>
            <td bgcolor="#CCCCCC">
                <asp:DropDownList ID="DropDownListCallType" runat="server">
                </asp:DropDownList>
            </td>
            <td bgcolor="#CCCCCC" class="style4">
                <asp:TextBox ID="TextBoxFrom" runat="server" Width="80px"></asp:TextBox>
                -<asp:TextBox ID="TextBoxTo" runat="server" Width="80px"></asp:TextBox>
            </td>
            <td bgcolor="#CCCCCC">
                <asp:DropDownList ID="DropDownListCompleted" runat="server">
                    <asp:ListItem Value="False">No</asp:ListItem>
                    <asp:ListItem Value="True">Yes</asp:ListItem>
                    
                </asp:DropDownList>
            </td>
            <td bgcolor="#CCCCCC" style="text-align: center">
                <asp:Button ID="ButtonFind" runat="server" Height="22px" 
                    onclick="ButtonFind_Click" Text="Find!" Width="79px" />
            </td>
        </tr>
        </table>
        &nbsp;</asp:Panel>
    
    
    
    <asp:Panel ID="PanelListView" runat="server">
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" PageSize="20" 
            style="font-size: x-small" onrowdatabound="GridView1_RowDataBound" 
            DataSourceID="ObjectDataSourceJobList" Width="1300px" 
            >
            <AlternatingRowStyle BackColor="#DEDEDE" />
            <Columns>
                <asp:TemplateField HeaderText="JobID" InsertVisible="False" 
                    SortExpression="JobID">
                    <EditItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("JobID") %>'></asp:Label>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButtonJobID" runat="server" 
                            CommandArgument='<%# Eval("JobID") %>' onclick="LinkButtonJobID_Click" 
                            Text='<%# Eval("JobID") %>'></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="On Hld" SortExpression="JobOnHold">
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBoxOnHold" runat="server" Checked='<%# Bind("JobOnHold") %>' 
                            Enabled="False" />
                    </ItemTemplate>
                    
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Comp" SortExpression="JobCompleted">
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBoxComp" runat="server" Checked='<%# Bind("JobCompleted") %>' 
                            Enabled="False" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Cancld" SortExpression="JobCancelled">
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBoxCan" runat="server" Checked='<%# Bind("JobCancelled") %>' 
                            Enabled="False" />
                    </ItemTemplate>
                   
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Client" SortExpression="ClientName">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("ClientName") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="LabelClient" runat="server" Text='<%# Bind("ClientName") %>' Visible=false></asp:Label>
                        <asp:Image ID="ImageLogo" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                    <asp:BoundField DataField="ClientSiteCode" HeaderText="Site" 
                    SortExpression="ClientSiteCode" />
                <asp:BoundField DataField="ClientSiteName" HeaderText="Workshop" 
                    SortExpression="ClientSiteName" />
                
                    <asp:TemplateField HeaderText="Call Type" SortExpression="CallType">
                        
                        <ItemTemplate>
                            <asp:Label ID="LabelCallType" runat="server" Text='<%# Bind("CallType") %>'></asp:Label>
                        </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="GeneratorRegNumber" HeaderText="Gen Reg" 
                    SortExpression="GeneratorRegNumber" />
                <asp:BoundField DataField="BowserReg" 
                    HeaderText="Bowser Reg" SortExpression="BowserReg" />
                
                <asp:BoundField DataField="RSGVehicleRegNumber" HeaderText="Vehicle Reg" 
                    SortExpression="RSGVehicleRegNumber" />
                <asp:TemplateField HeaderText="Call Received At" 
                    SortExpression="CallReceivedAt">
                    <ItemTemplate>
                        <asp:Label ID="LabelCallReceivedAt" runat="server" 
                            Text='<%# String.Format("{0:yyyy/MM/dd HH:mm}",DataBinder.Eval(Container.DataItem, "CallReceivedAt")) %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="CallReceivedUser" HeaderText="By User" 
                    SortExpression="CallReceivedUser" />
                <asp:TemplateField HeaderText="Dispatched At" 
                    SortExpression="DriverDispatchedAt">
                    
                    <ItemTemplate>
                        <asp:Label ID="LabelCallDispatchedAt" runat="server" 
                            Text='<%# String.Format("{0:yyyy/MM/dd HH:mm}",DataBinder.Eval(Container.DataItem, "DriverDispatchedAt")) %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="DispatchedUser" HeaderText="By User" 
                    SortExpression="DispatchedUser" />
                <asp:TemplateField HeaderText="Site Up At" SortExpression="SiteUpAt">
                   
                    <ItemTemplate>
                        <asp:Label ID="LabelSiteUpAt" runat="server" Text='<%# String.Format("{0:yyyy/MM/dd HH:mm}",DataBinder.Eval(Container.DataItem, "SiteUpAt")) %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="SiteUpUser" HeaderText="By User" 
                    SortExpression="SiteUpUser" />

                <asp:TemplateField HeaderText="Comments" SortExpression="Comments">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("Comments") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:TextBox ID="TextBox5" runat="server" Height="55px" ReadOnly="True" 
                            style="font-family: 'Arial Narrow'; font-size: xx-small" 
                            Text='<%# Eval("Comments") %>' TextMode="MultiLine" Width="204px"></asp:TextBox>
                    </ItemTemplate>
                    <ItemStyle Width="200px" />
                </asp:TemplateField>

            </Columns>
            <HeaderStyle BackColor="#CCCCCC" />
            <RowStyle BackColor="White" />
        </asp:GridView>
    </asp:Panel>
    <br />
&nbsp;<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </asp:ToolkitScriptManager>
    <asp:ObjectDataSource ID="ObjectDataSourceJobList" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        TypeName="JobXSDTableAdapters.JobFilterTableAdapter">
        <SelectParameters>
            <asp:Parameter DefaultValue="0" Name="JobID" Type="Int32" />
            <asp:ControlParameter ControlID="DropDownListClient" DefaultValue="" 
                Name="ClientID" PropertyName="SelectedValue" Type="Int32" />
            <asp:ControlParameter ControlID="DropDownListCompleted" DefaultValue="False" 
                Name="JobCompleted" PropertyName="SelectedValue" Type="Boolean" />
            <asp:Parameter DefaultValue="%" Name="SiteCode" Type="String" />
            <asp:Parameter DefaultValue="0" Name="CallType" Type="String" />
            <asp:ControlParameter ControlID="TextBoxFrom" DefaultValue="" Name="From" 
                PropertyName="Text" Type="DateTime" />
            <asp:ControlParameter ControlID="TextBoxTo" Name="To" PropertyName="Text" 
                Type="DateTime" />
            <asp:Parameter DefaultValue="0" Name="RegionName" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    </asp:Content>
