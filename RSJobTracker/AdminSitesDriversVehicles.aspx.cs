﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AdminSitesDriversVehicles : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
           
        }
       
    }



    protected void ListBoxClients_SelectedIndexChanged(object sender, EventArgs e)
    {
        GridView1.DataBind();
        GridView2.DataBind();
        
    }
    
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //DropDownList ddReg = (DropDownList)e.Row.FindControl("DropDownListRegion");
            //DropDownList ddSubReg = (DropDownList)e.Row.FindControl("DropDownListSubRegion");

            //LookupsXSDTableAdapters.ClientSubRegionsTableAdapter CSTA = new LookupsXSDTableAdapters.ClientSubRegionsTableAdapter();
            //LookupsXSD.ClientSubRegionsDataTable CSDT = CSTA.GetDataByClientRegionID(Convert.ToInt32(ddReg.SelectedValue));
            
            //ListItem myLI = new ListItem("","");
            //ddSubReg.Items.Add(myLI);

            //for (int f = 0; f < CSDT.Rows.Count; f++)
            //{
            //    LookupsXSD.ClientSubRegionsRow  CSRow = (LookupsXSD.ClientSubRegionsRow )CSDT[f];
                
            //        myLI = new ListItem(CSRow.SubRegionName, CSRow.ClientSubRegionID.ToString());
            //        ddSubReg.Items.Add(myLI);
                
            //}
            //Label LabelSubRegID = (Label)e.Row.FindControl("lblSubRegID");
            //ddSubReg.SelectedValue=LabelSubRegID.Text;

        }
    }
    
    protected void CheckBoxActive_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox myCB = (CheckBox)sender;
        Label mylbl = (Label)myCB.Parent.FindControl("lblBowserID");

        BowsersXSDTableAdapters.BowsersTableAdapter BTA = new BowsersXSDTableAdapters.BowsersTableAdapter();
        int x = BTA.UpdateActive(myCB.Checked, Convert.ToInt32(mylbl.Text));
        GridView1.DataBind();
    }
    protected void TextBoxLastLocation_TextChanged(object sender, EventArgs e)
    {
        TextBox myTB=(TextBox)sender;

        Label bowserID = (Label)myTB.Parent.FindControl("lblBowserID");
        TextBox LastLoc =(TextBox)myTB.Parent.FindControl("TextBoxLastLocation");
        TextBox LastLocDate = (TextBox)myTB.Parent.FindControl("TextBoxLastLocationDate");
        

        DateTime? dtLastLocDate = null;
        if(LastLocDate.Text != "") {dtLastLocDate = Convert.ToDateTime (LastLocDate.Text);}

        BowsersXSDTableAdapters.BowsersTableAdapter BTA = new BowsersXSDTableAdapters.BowsersTableAdapter();
        int x= BTA.UpdateFields(LastLoc.Text,dtLastLocDate,Convert.ToInt32(bowserID.Text));
        GridView1.DataBind();        
    }
    protected void ButtonAddSite_Click(object sender, EventArgs e)
    {
        if (TextBoxAddSite.Text != "")
        {
            LookupsXSDTableAdapters.RSGSitesTableAdapter STA = new LookupsXSDTableAdapters.RSGSitesTableAdapter();
            int x = STA.Insert(TextBoxAddSite.Text);
            ListBoxClients.DataBind();
            TextBoxAddSite.Text = "";
        }
    }
    protected void ButtonAdd_Click(object sender, EventArgs e)
    {

    }
    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList myDD=(DropDownList)sender;
        Label myLbl = (Label)myDD.Parent.FindControl("lblDiverID");

        LookupsXSDTableAdapters.RSGDriversTableAdapter DTA = new LookupsXSDTableAdapters.RSGDriversTableAdapter();
        int x = DTA.UpdateSite(Convert.ToInt32(myDD.SelectedValue), Convert.ToInt32(myLbl.Text));
        GridView1.DataBind();
    }
    protected void DropDownList2_SelectedIndexChanged1(object sender, EventArgs e)
    {
        DropDownList myDD = (DropDownList)sender;
        Label myLbl = (Label)myDD.Parent.FindControl("lblVehicleID");
        LookupsXSDTableAdapters.RSGVehiclesTableAdapter VTA = new LookupsXSDTableAdapters.RSGVehiclesTableAdapter();
        
        int x = VTA.UpdateSite(Convert.ToInt32(myDD.SelectedValue), Convert.ToInt32(myLbl.Text));
        GridView1.DataBind();
    }
    protected void CheckBox1_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox myCB = (CheckBox)sender;

        Label myLbl = (Label)myCB.Parent.Parent.FindControl("lblDiverID");

        LookupsXSDTableAdapters.RSGDriversTableAdapter DTA = new LookupsXSDTableAdapters.RSGDriversTableAdapter();
        int x = DTA.UpdateActive(myCB.Checked , Convert.ToInt32(myLbl.Text));
        GridView1.DataBind();
    }
    protected void CheckBox1_CheckedChanged1(object sender, EventArgs e)
    {
        CheckBox myCB = (CheckBox)sender;

        Label myLbl = (Label)myCB.Parent.FindControl("lblVehicleID");

        LookupsXSDTableAdapters.RSGVehiclesTableAdapter VTA = new LookupsXSDTableAdapters.RSGVehiclesTableAdapter();
        int x = VTA.UpdateActive(myCB.Checked, Convert.ToInt32(myLbl.Text));
        GridView1.DataBind();
    }
    protected void ButtonAddDriver_Click(object sender, EventArgs e)
    {
        if (TextBoxAddDriver.Text != "")
        {
            LookupsXSDTableAdapters.RSGDriversTableAdapter DTA = new LookupsXSDTableAdapters.RSGDriversTableAdapter();
            int x = DTA.InsertQuery(Convert.ToInt32(ListBoxClients.SelectedValue), TextBoxAddDriver.Text, true);
            GridView1.DataBind();
            TextBoxAddDriver.Text = "";
        }
    }
    protected void ButtonAddVehicle_Click(object sender, EventArgs e)
    {
        if (TextBoxAddVehicle.Text != "")
        {
            LookupsXSDTableAdapters.RSGVehiclesTableAdapter VTA = new LookupsXSDTableAdapters.RSGVehiclesTableAdapter();
            int x = VTA.InsertQuery(Convert.ToInt32(ListBoxClients.SelectedValue), TextBoxAddVehicle.Text,TextBoxAddVehicledesc.Text, true);
            GridView2.DataBind();
            TextBoxAddVehicledesc.Text = "";
            TextBoxAddVehicle.Text = "";
        }
    }
    protected void TextBoxVehDesc_TextChanged(object sender, EventArgs e)
    {
        TextBox myTB = (TextBox)sender;

        Label myLbl = (Label)myTB.Parent.FindControl("lblVehicleID");

        LookupsXSDTableAdapters.RSGVehiclesTableAdapter VTA = new LookupsXSDTableAdapters.RSGVehiclesTableAdapter();
        int x = VTA.UpdateDescription (myTB.Text, Convert.ToInt32(myLbl.Text));
        GridView1.DataBind();
    }
    protected void TextBoxCellNumber_TextChanged(object sender, EventArgs e)
    {
        TextBox myTB = (TextBox)sender;
        Label myLbl = (Label)myTB.Parent.FindControl("lblDiverID");

        LookupsXSDTableAdapters.RSGDriversTableAdapter DTA = new LookupsXSDTableAdapters.RSGDriversTableAdapter();
        int x = DTA.UpdateCellNumber(myTB.Text, Convert.ToInt32(myLbl.Text));
        GridView1.DataBind();
    }
}
