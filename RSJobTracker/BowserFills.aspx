﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="BowserFills.aspx.cs" Inherits="BowserFills" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script language="javascript" type="text/javascript">
    window.setTimeout("DoPostBack();", 50000);
    function DoPostBack() {
        document.forms[0].submit();
    }
    </script>
    <style type="text/css">
        .style4
        {
            width: 560px;
        }
    </style>
    </asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h1><asp:Label ID="lblHeading" runat="server" Text=""></asp:Label></h1>
    
    
    <table>
    <tr>
    <td bgcolor="#DDDDDD">
    Clients
    </td>
    <td bgcolor="#EEEEEE">
    Regions
    </td>
    <td bgcolor="#DDDDDD">
    &nbsp;</td>
    <td class="style4" bgcolor="#EEEEEE">
        Add Entry<br />
        
    </td>
    </tr>
    <tr>
    <td valign="top" bgcolor="#DDDDDD">
        <asp:ListBox ID="ListBoxClients" runat="server" AutoPostBack="True" 
            DataSourceID="ObjectDataSourceClients" DataTextField="ClientName" 
            DataValueField="ClientID" 
            onselectedindexchanged="ListBoxClients_SelectedIndexChanged"></asp:ListBox>
        <br />
    </td>
    <td valign="top" bgcolor="#EEEEEE">
        <asp:ListBox ID="ListBoxRegions" runat="server" AutoPostBack="True" 
            DataSourceID="ObjectDataSourceRegions" DataTextField="ClientRegionName" 
            DataValueField="ClientRegionID" 
            onselectedindexchanged="ListBoxRegions_SelectedIndexChanged"></asp:ListBox>
        <br />
    </td>
    <td valign="top" bgcolor="#DDDDDD">
        <br />
    </td>
    <td class="style4" bgcolor="#EEEEEE">
        Bowser:<asp:DropDownList ID="DropDownListBowser" runat="server" 
            DataSourceID="ObjectDataSourceBowsers" DataTextField="BowserReg" 
            DataValueField="BowserID">
        </asp:DropDownList>
&nbsp;&nbsp; Liters:<asp:TextBox ID="TextBoxLiters" runat="server" Width="67px"></asp:TextBox>
&nbsp; at:<asp:TextBox ID="TextBoxDate" runat="server"></asp:TextBox>
&nbsp;
        <asp:Button ID="ButtonAdd" runat="server" onclick="ButtonAdd_Click1" 
            Text="Add" />
        </td>
    </tr>
    </table>
    
    
    <asp:Panel ID="PanelListView" runat="server">
    </asp:Panel>
        Recent Fills:<br />
&nbsp;<asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
            DataSourceID="ObjectDataSourceFillsList" Width="781px" 
            onrowdatabound="GridView1_RowDataBound">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:BoundField DataField="FilledAt" HeaderText="FilledAt" 
                    SortExpression="FilledAt" />
                <asp:BoundField DataField="BowserReg" HeaderText="BowserReg" 
                    SortExpression="BowserReg" />
                <asp:TemplateField HeaderText="LitersFilled" SortExpression="LitersFilled">
                    <ItemTemplate>
                        <asp:Label ID="LabelLogID" runat="server" Text='<%# Eval("FuelLogID") %>' 
                            Visible="False"></asp:Label>
                        <asp:TextBox ID="TextBoxLiters" runat="server" AutoPostBack="True" width="75"
                            ontextchanged="TextBoxLiters_TextChanged" Text='<%# Bind("LitersFilled") %>'></asp:TextBox>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("LitersFilled") %>'></asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="JobID" HeaderText="JobID" 
                    SortExpression="JobID" />
               
            </Columns>
        </asp:GridView>
    <asp:ObjectDataSource ID="ObjectDataSourceClients" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        TypeName="LookupsXSDTableAdapters.ClientsTableAdapter" 
    InsertMethod="Insert">
        <InsertParameters>
            <asp:Parameter Name="ClientName" Type="String" />
        </InsertParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceRegions" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByClientID" 
        TypeName="LookupsXSDTableAdapters.ClientRegionsTableAdapter">
        <SelectParameters>
            <asp:ControlParameter ControlID="ListBoxClients" DefaultValue="0" 
                Name="ClientID" PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceFillsList" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByBowserFills" 
        TypeName="FuelLogXSDTableAdapters.FuelLogTableAdapter">
        <SelectParameters>
            <asp:ControlParameter ControlID="ListBoxClients" Name="ClientID" 
                PropertyName="SelectedValue" Type="Int32" />
            <asp:ControlParameter ControlID="ListBoxRegions" Name="ClientRegionID" 
                PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceSubRegions" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByClientRegionID" 
        TypeName="LookupsXSDTableAdapters.ClientSubRegionsTableAdapter" 
    InsertMethod="Insert">
        <InsertParameters>
            <asp:Parameter Name="ClientRegionID" Type="Int32" />
            <asp:Parameter Name="SubRegionName" Type="String" />
        </InsertParameters>
        <SelectParameters>
            <asp:ControlParameter ControlID="ListBoxRegions" Name="ClientRegionID" 
                PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceBowsers" runat="server" 
        InsertMethod="Insert" OldValuesParameterFormatString="original_{0}" 
        SelectMethod="GetDataByRegionID" 
        TypeName="LookupsXSDTableAdapters.BowsersTableAdapter">
        <InsertParameters>
            <asp:Parameter Name="ClientID" Type="Int32" />
            <asp:Parameter Name="ClientRegionID" Type="Int32" />
            <asp:Parameter Name="BowserReg" Type="String" />
            <asp:Parameter Name="LastLocation" Type="String" />
            <asp:Parameter Name="LastLocationDate" Type="DateTime" />
            <asp:Parameter Name="ClientSubRegionID" Type="Int32" />
            <asp:Parameter Name="Active" Type="Boolean" />
        </InsertParameters>
        <SelectParameters>
            <asp:ControlParameter ControlID="ListBoxRegions" Name="ClientRegionID" 
                PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <br />
    </asp:Content>
