﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="TicketList.aspx.cs" Inherits="TicketList" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<%@ Register assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script language="javascript" type="text/javascript">
    window.setTimeout("DoPostBack();", 50000);
    function DoPostBack() {
        document.forms[0].submit();
    }
    </script>
    <style type="text/css">
        .style4
        {
            width: 190px;
        }
    </style>
    </asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:Panel ID="panelJumpMenu" runat="server" BackColor="#AAAAAA" >
        <asp:Button ID="ButtonDrivers" runat="server" Text="Drivers on/off" 
            onclick="ButtonDrivers_Click" Width="80px" Font-Names="Arial Narrow" /> 
        &nbsp;<asp:Button ID="ButtonBowserFills" runat="server" 
            onclick="ButtonBowserFills_Click" Text="Bowser Fills" Width="80px" 
            Font-Names="Arial Narrow" />
        &nbsp;<asp:Button ID="ButtonGens" runat="server" onclick="ButtonGens_Click" 
            Text="Gen Rep" Width="80px" />
        &nbsp;&nbsp;
        <asp:Label ID="LabelCounters" runat="server" style="font-size: large"></asp:Label>
    </asp:Panel>
    Jobs with no comments since
    <asp:Label ID="Label20MinsAgo" runat="server" Text="Label20MinsAgo"></asp:Label>
    :<asp:ListView ID="ListView1" runat="server" 
        DataSourceID="ObjectDataSource1" GroupItemCount="12">
        <AlternatingItemTemplate>
            <td runat="server" style="background-color:#FFF8DC;" valign="top" width="150">
                <asp:LinkButton ID="LinkButtonUpdateComment" runat="server" Text='<%# Eval("JobID") %>' CommandArgument ='<%# Eval("JobID") %>' onclick="LinkButtonUpdateComment_Click" >LinkButton</asp:LinkButton><asp:Image ID="Image1" runat="server" ImageUrl='<%# Eval("Colour") %>'  ToolTip='<%# Eval("ClientSiteName") %>' /><br />Site: <asp:Label ID="LabelClientSite" runat="server" 
                            Text='<%# Eval("ClientSiteID") %>'></asp:Label>
            <br /><asp:Label ID="LabelCallType" runat="server" 
                            Text='<%# Eval("CallType") %>'></asp:Label>
            </td>
        </AlternatingItemTemplate>
       
        <EmptyDataTemplate>
            <table runat="server" 
                style="background-color: #FFFFFF;border-collapse: collapse;border-color: #999999;border-style:none;border-width:1px;">
                <tr>
                    <td>
                        All Jobs updated.</td>
                </tr>
            </table>
        </EmptyDataTemplate>
        <EmptyItemTemplate>
<td runat="server" />
        </EmptyItemTemplate>
        <GroupTemplate>
            <tr ID="itemPlaceholderContainer" runat="server">
                <td ID="itemPlaceholder" runat="server">
                </td>
            </tr>
        </GroupTemplate>
        
        <ItemTemplate>
            <td runat="server" style="background-color:#DCDCDC;color: #000000;" valign="top" width="150">
                <asp:LinkButton ID="LinkButtonUpdateComment1" runat="server" Text='<%# Eval("JobID") %>' CommandArgument ='<%# Eval("JobID") %>' onclick="LinkButtonUpdateComment_Click" >LinkButton</asp:LinkButton><asp:Image ID="Image2" runat="server" ImageUrl='<%# Eval("Colour") %>'   ToolTip='<%# Eval("ClientSiteName") %>' /><br />Site: <asp:Label ID="LabelSiteID3" runat="server" 
                            Text='<%# Eval("ClientSiteID") %>'></asp:Label>  <br /><asp:Label ID="LabelCallType2" runat="server" 
                            Text='<%# Eval("CallType") %>'></asp:Label>
            </td>
        </ItemTemplate>
        <LayoutTemplate>
            <table runat="server">
                <tr runat="server">
                    <td runat="server">
                        <table ID="groupPlaceholderContainer" runat="server" border="1" 
                            style="background-color: #FFFFFF;border-collapse: collapse;border-color: #999999;border-style:none;border-width:1px;font-family: Verdana, Arial, Helvetica, sans-serif;">
                            <tr ID="groupPlaceholder" runat="server">
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr runat="server">
                    <td runat="server" 
                        style="text-align: center;background-color: #CCCCCC;font-family: Verdana, Arial, Helvetica, sans-serif;color: #000000;">
                    </td>
                </tr>
            </table>
        </LayoutTemplate>
       
    </asp:ListView><asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        TypeName="JobXSDTableAdapters.CommensRequiredTableAdapter">
        <SelectParameters>
            <asp:ControlParameter ControlID="Label20MinsAgo" Name="_20MinsAgo" 
                PropertyName="Text" Type="DateTime" />
        </SelectParameters>
    </asp:ObjectDataSource><asp:Panel ID="PanelFilter" runat="server"><table bgcolor="Black" 
            cellpadding="1" cellspacing="1" style="width: 1300px">
        <tr>
            <td bgcolor="#AAAAAA" colspan="8">
                <strong>Filter for Job(s)</strong></td>
            <td bgcolor="#EEEEEE" rowspan="3"><center>
                <asp:Button ID="ButtonAdd" runat="server" Height="22px" 
                    onclick="ButtonAdd_Click" style="text-align: center" Text="New Job" 
                    Width="79px" /></center>
            </td>
        </tr>
        <tr>
            <td bgcolor="#CCCCCC">
                jobID</td>
            <td bgcolor="#CCCCCC">
                Client</td>
            <td bgcolor="#CCCCCC">
                Region</td>
            <td bgcolor="#CCCCCC">
                Site</td>
            <td bgcolor="#CCCCCC">
                CallType</td>
            <td bgcolor="#CCCCCC" class="style4">
                Call received</td>
            <td bgcolor="#CCCCCC">
                JobCompleted/Cancelled</td>
            <td bgcolor="#CCCCCC">
                &nbsp;</td>
        </tr>
        <tr>
            <td bgcolor="#CCCCCC">
                <asp:TextBox ID="TextBoxJobID" runat="server" Width="43px"></asp:TextBox>
            </td>
            <td bgcolor="#CCCCCC">
                <asp:DropDownList ID="DropDownListClient" runat="server" AutoPostBack="True" 
                    onselectedindexchanged="DropDownListClient_SelectedIndexChanged" 
                    style="height: 22px">
                </asp:DropDownList>
            </td>
            <td bgcolor="#CCCCCC">
                <asp:DropDownList ID="DropDownListRegions" runat="server" AutoPostBack="True">
                </asp:DropDownList>
            </td>
            <td bgcolor="#CCCCCC">
                <asp:TextBox ID="TextBoxSite" runat="server"></asp:TextBox>
            </td>
            <td bgcolor="#CCCCCC">
                <asp:DropDownList ID="DropDownListCallType" runat="server">
                </asp:DropDownList>
            </td>
            <td bgcolor="#CCCCCC" class="style4">
                <asp:TextBox ID="TextBoxFrom" runat="server" Width="80px"></asp:TextBox>
                -<asp:TextBox ID="TextBoxTo" runat="server" Width="80px"></asp:TextBox>
            </td>
            <td bgcolor="#CCCCCC">
                <asp:DropDownList ID="DropDownListCompleted" runat="server">
                    <asp:ListItem Value="False">No</asp:ListItem>
                    <asp:ListItem Value="True">Yes</asp:ListItem>
                    
                </asp:DropDownList>
            </td>
            <td bgcolor="#CCCCCC" style="text-align: center">
                <asp:Button ID="ButtonFind" runat="server" Height="22px" 
                    onclick="ButtonFind_Click" Text="Find!" Width="79px" />
            </td>
        </tr>
        </table>
        &nbsp;</asp:Panel>
    
    
    
    <asp:Panel ID="PanelListView" runat="server">
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" PageSize="20" 
            style="font-size: x-small" onrowdatabound="GridView1_RowDataBound" 
            DataSourceID="ObjectDataSourceJobList" Width="1300px" 
            >
            <AlternatingRowStyle BackColor="#DEDEDE" />
            <Columns>
                <asp:TemplateField HeaderText="JobID" InsertVisible="False" 
                    SortExpression="JobID">
                    <EditItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("JobID") %>'></asp:Label>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButtonJobID" runat="server" 
                            CommandArgument='<%# Eval("JobID") %>' onclick="LinkButtonJobID_Click" 
                            Text='<%# Eval("JobID") %>'></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="On Hld" SortExpression="JobOnHold">
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBoxOnHold" runat="server" Checked='<%# Bind("JobOnHold") %>' 
                            Enabled="False" />
                    </ItemTemplate>
                    
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Comp" SortExpression="JobCompleted">
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBoxComp" runat="server" Checked='<%# Bind("JobCompleted") %>' 
                            Enabled="False" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Cancld" SortExpression="JobCancelled">
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBoxCan" runat="server" Checked='<%# Bind("JobCancelled") %>' 
                            Enabled="False" />
                    </ItemTemplate>
                   
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Client" SortExpression="ClientName">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("ClientName") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="LabelClient" runat="server" Text='<%# Bind("ClientName") %>' Visible=false></asp:Label>
                        <asp:Image ID="ImageLogo" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="ClientRegionName" HeaderText="Region" 
                    SortExpression="ClientRegionName" />
                    <asp:BoundField DataField="SubRegionName" HeaderText="SubRegion" 
                    SortExpression="SubRegionName" />
                    <asp:BoundField DataField="ClientSiteCode" HeaderText="Site" 
                    SortExpression="ClientSiteCode" />
                <asp:BoundField DataField="ClientSiteName" HeaderText="Site Name" 
                    SortExpression="ClientSiteName" />
                    <asp:BoundField DataField="TicketNumber" HeaderText="Ticket Number" 
                    SortExpression="TicketNumber" />
                
                    <asp:TemplateField HeaderText="Call Type" SortExpression="CallType">
                        
                        <ItemTemplate>
                            <asp:Label ID="LabelCallType" runat="server" Text='<%# Bind("CallType") %>'></asp:Label>
                        </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="GeneratorRegNumber" HeaderText="Gen Reg" 
                    SortExpression="GeneratorRegNumber" />
                <asp:BoundField DataField="BowserReg" 
                    HeaderText="Bowser Reg" SortExpression="BowserReg" />
                
                <asp:TemplateField HeaderText="Call Received At" 
                    SortExpression="CallReceivedAt">
                    
                    <ItemTemplate>
                        <asp:Label ID="LabelCallReceivedAt" runat="server" 
                            Text='<%# String.Format("{0:yyyy/MM/dd HH:mm}",DataBinder.Eval(Container.DataItem, "CallReceivedAt")) %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="CallReceivedUser" HeaderText="By User" 
                    SortExpression="CallReceivedUser" />
                <asp:BoundField DataField="RSGDriverName" HeaderText="Driver" 
                    SortExpression="RSGDriverName" />
                <asp:BoundField DataField="RSGVehicleRegNumber" HeaderText="Vehicle Reg" 
                    SortExpression="RSGVehicleRegNumber" />
                <asp:TemplateField HeaderText="Dispatched At" 
                    SortExpression="DriverDispatchedAt">
                    
                    <ItemTemplate>
                        <asp:Label ID="LabelCallDispatchedAt" runat="server" 
                            Text='<%# String.Format("{0:yyyy/MM/dd HH:mm}",DataBinder.Eval(Container.DataItem, "DriverDispatchedAt")) %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="DispatchedUser" HeaderText="By User" 
                    SortExpression="DispatchedUser" />
                <asp:TemplateField HeaderText="Site Up At" SortExpression="SiteUpAt">
                   
                    <ItemTemplate>
                        <asp:Label ID="LabelSiteUpAt" runat="server" Text='<%# String.Format("{0:yyyy/MM/dd HH:mm}",DataBinder.Eval(Container.DataItem, "SiteUpAt")) %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="SiteUpUser" HeaderText="By User" 
                    SortExpression="SiteUpUser" />

                <asp:TemplateField HeaderText="Comments" SortExpression="Comments">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("Comments") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:TextBox ID="TextBox5" runat="server" Height="55px" ReadOnly="True" 
                            style="font-family: 'Arial Narrow'; font-size: xx-small" 
                            Text='<%# Eval("Comments") %>' TextMode="MultiLine" Width="204px"></asp:TextBox>
                    </ItemTemplate>
                    <ItemStyle Width="200px" />
                </asp:TemplateField>

            </Columns>
            <HeaderStyle BackColor="#CCCCCC" />
            <RowStyle BackColor="White" />
        </asp:GridView>
    </asp:Panel>
    <br />
&nbsp;<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </asp:ToolkitScriptManager>
    <asp:ObjectDataSource ID="ObjectDataSourceJobList" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        TypeName="JobXSDTableAdapters.JobFilterTableAdapter">
        <SelectParameters>
            <asp:ControlParameter ControlID="TextBoxJobID" Name="JobID" 
                PropertyName="Text" Type="Int32" DefaultValue="0" />
            <asp:ControlParameter ControlID="DropDownListClient" DefaultValue="" 
                Name="ClientID" PropertyName="SelectedValue" Type="Int32" />
            <asp:ControlParameter ControlID="DropDownListCompleted" DefaultValue="False" 
                Name="JobCompleted" PropertyName="SelectedValue" Type="Boolean" />
            <asp:ControlParameter ControlID="TextBoxSite" DefaultValue="%" Name="SiteCode" 
                PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="DropDownListCallType" DefaultValue="0" 
                Name="CallType" PropertyName="SelectedValue" Type="String" />
            <asp:ControlParameter ControlID="TextBoxFrom" DefaultValue="" Name="From" 
                PropertyName="Text" Type="DateTime" />
            <asp:ControlParameter ControlID="TextBoxTo" Name="To" PropertyName="Text" 
                Type="DateTime" />
            <asp:ControlParameter ControlID="DropDownListRegions" DefaultValue="0" 
                Name="RegionName" PropertyName="SelectedValue" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    </asp:Content>
