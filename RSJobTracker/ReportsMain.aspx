﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ReportsMain.aspx.cs" Inherits="ReportsMain" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<%@ Register assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script language="javascript" type="text/javascript">
    window.setTimeout("DoPostBack();", 50000);
    function DoPostBack() {
        document.forms[0].submit();
    }
    </script>
    </asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:Panel ID="PanelFilter" runat="server">    &nbsp;&nbsp;</asp:Panel>
    
    
    
    <asp:Panel ID="PanelListView" runat="server">
        Jobs <strong>Completed</strong> per day:</asp:Panel>
    <br />
&nbsp;<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </asp:ToolkitScriptManager>
    <asp:ObjectDataSource ID="ObjectDataSourceJobPerDay" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        TypeName="GraphsTableAdapters.JobPerDayTableAdapter"></asp:ObjectDataSource>
    <asp:Chart ID="Chart1" runat="server" DataSourceID="ObjectDataSourceJobPerDay" 
        Height="157px" Width="1300px" ImageLocation="~/graphimages/grp.png" 
    ImageStorageMode="UseImageLocation">
        <Series>
            <asp:Series Name="Series1" XValueMember="Day" YValueMembers="Count" 
                IsValueShownAsLabel="True">
            </asp:Series>
        </Series>
        <ChartAreas>
            <asp:ChartArea Name="ChartArea1">
            </asp:ChartArea>
        </ChartAreas>
    </asp:Chart>
    <br />
    <br />
    <table><tr><td>
    All Vodacom Jobs <strong>Completed</strong> By Call Type:</td><td valign="top">&nbsp;</td>
    <td>
    All MTN Jobs <strong>Completed </strong>By Call Type:</td><td valign="top">&nbsp;</td><td>
        &nbsp;<tr><td>
    <asp:Chart ID="Chart2" runat="server" 
        DataSourceID="ObjectDataSourceJobsByCallTypeVod" 
        ImageLocation="~/graphimages/pie1.png" ImageStorageMode="UseImageLocation" 
        Width="438px">
        <Series>
            <asp:Series ChartType="Doughnut" Name="Series1" XValueMember="CallType" 
                YValueMembers="Expr1" >
            </asp:Series>
        </Series>
        <ChartAreas>
            <asp:ChartArea Name="ChartArea1">
            </asp:ChartArea>
        </ChartAreas>
    </asp:Chart>
        </td><td valign="top"><asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                DataSourceID="ObjectDataSourceJobsByCallTypeVod">
            <Columns>
                <asp:BoundField DataField="CallType" HeaderText="CallType" 
                    SortExpression="CallType" />
                    <asp:BoundField DataField="Expr1" HeaderText="Count" ReadOnly="True" 
                    SortExpression="Expr1" />
                
            </Columns>
            </asp:GridView>
        </td>
    
    <td>
    <asp:ObjectDataSource ID="ObjectDataSourceJobsByCallTypeVod" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        TypeName="GraphsTableAdapters.JobsByCallTypeTableAdapter">
        <SelectParameters>
            <asp:Parameter DefaultValue="1" Name="ClientID" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceJobsByCallTypeMTN" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        TypeName="GraphsTableAdapters.JobsByCallTypeTableAdapter">
        <SelectParameters>
            <asp:Parameter DefaultValue="2" Name="ClientID" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:Chart ID="Chart3" runat="server" 
        DataSourceID="ObjectDataSourceJobsByCallTypeMTN" 
        ImageLocation="~/graphimages/pie2.png" ImageStorageMode="UseImageLocation" 
        Width="438px">
        <Series>
            <asp:Series ChartType="Doughnut" Name="Series1" XValueMember="CallType" 
                YValueMembers="Expr1" >
            </asp:Series>
        </Series>
        <ChartAreas>
            <asp:ChartArea Name="ChartArea1">
            </asp:ChartArea>
        </ChartAreas>
    </asp:Chart>
        </td><td valign="top"><asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" 
                DataSourceID="ObjectDataSourceJobsByCallTypeMTN">
            <Columns>
                <asp:BoundField DataField="CallType" HeaderText="CallType" 
                    SortExpression="CallType" />
                    <asp:BoundField DataField="Expr1" HeaderText="Count" ReadOnly="True" 
                    SortExpression="Expr1" />
                
            </Columns>
            </asp:GridView>
        </td><td></table>
                &nbsp;</asp:Content>
