﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AssetsList : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }
    protected void ddlSite_SelectedIndexChanged(object sender, EventArgs e)
    {
        gvAssets.DataBind();
    }
    protected void lbAssetsHistory_Click(object sender, EventArgs e)
    {        
        LinkButton myLB = (LinkButton)sender;
        lblAssetID.Text = myLB.CommandArgument;
        Label AssetName = (Label)myLB.Parent.Parent.FindControl("Description");

        AssetsTableAdapters.AssetsTableAdapter ATA = new AssetsTableAdapters.AssetsTableAdapter();
        Assets.AssetsDataTable ADT = ATA.GetDataByAssetID(Convert.ToInt32(lblAssetID.Text));
        Assets.AssetsRow ARow = (Assets.AssetsRow)ADT[0];
        lblAssetName.Text = ARow.Description;
        PanelListView.Visible = false;
        PanelAssetHistory.Visible = true;
        gvAssetHistory.DataBind();
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        PanelListView.Visible = true;
        PanelAssetHistory.Visible = false;
    }
    protected void Button1_Click1(object sender, EventArgs e)
    {
        Button myBtn = (Button)sender;

        DropDownList RSGDriverID = (DropDownList)myBtn.Parent.Parent.FindControl("DDLDriver");
        Label BookedOutAt = (Label)myBtn.Parent.Parent.FindControl("lblBookedOutAt");
        LinkButton AssetID = (LinkButton)myBtn.Parent.Parent.FindControl("lbAssetsHistory");

        int BookedByUserID = Convert.ToInt32(Session["UserID"]);

        AssetsTableAdapters.AssetsTableAdapter ATA = new AssetsTableAdapters.AssetsTableAdapter();
        ATA.UpdateAsset(BookedByUserID, DateTime.Now, Convert.ToInt32(RSGDriverID.Text), Convert.ToInt32(myBtn.CommandArgument));

        AssetsTableAdapters.AssetsHistoryTableAdapter AHTA = new AssetsTableAdapters.AssetsHistoryTableAdapter();
        AHTA.InsertQueryHistory(Convert.ToInt32(AssetID.Text), Convert.ToInt32(RSGDriverID.Text), DateTime.Now, BookedByUserID);

        gvAssets.DataBind();
    }
    protected void ButtonBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("TicketList.aspx");
    }
}
