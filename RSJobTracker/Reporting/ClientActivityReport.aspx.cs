﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ClientActivityReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            loadDropdowns();
        }
       
    }


    protected void loadDropdowns()
    {


        ListItem myLI = new ListItem("All", "0");
        DropDownListClient.Items.Add(myLI);


        
        
        LookupsXSDTableAdapters.ClientsTableAdapter CTA = new LookupsXSDTableAdapters.ClientsTableAdapter();
        LookupsXSD.ClientsDataTable CDT = CTA.GetData();


        for (int f = 0; f < CDT.Rows.Count; f++)
        {
            LookupsXSD.ClientsRow CRow = (LookupsXSD.ClientsRow)CDT[f];
            myLI = new ListItem(CRow.ClientName, CRow.ClientID.ToString());
            DropDownListClient.Items.Add(myLI);
        }


        myLI = new ListItem("All", "0");
        DropDownListRegions.Items.Add(myLI);

        LookupsXSDTableAdapters.ClientRegionsTableAdapter CRTA = new LookupsXSDTableAdapters.ClientRegionsTableAdapter();
        LookupsXSD.ClientRegionsDataTable CRDT = CRTA.GetDataByClientID(Convert.ToInt32(DropDownListClient.SelectedValue));
        
        for (int f = 0; f < CRDT.Rows.Count; f++)
        {
            LookupsXSD.ClientRegionsRow CRRow = (LookupsXSD.ClientRegionsRow)CRDT[f];
            myLI = new ListItem(CRRow.ClientRegionName, CRRow.ClientRegionName);
            DropDownListRegions.Items.Add(myLI);
        }


       
        myLI = new ListItem("", "0");
        DropDownListCallType.Items.Add(myLI);


        LookupsXSDTableAdapters.CallTypesTableAdapter CTTA = new LookupsXSDTableAdapters.CallTypesTableAdapter();
        LookupsXSD.CallTypesDataTable CTDT = CTTA.GetData();

        for (int f = 0; f < CTDT.Rows.Count; f++)
        {
            LookupsXSD.CallTypesRow CTRow = (LookupsXSD.CallTypesRow)CTDT[f];
            myLI = new ListItem(CTRow.CallType, CTRow.CallType);
            DropDownListCallType.Items.Add(myLI);
        }
        

        TextBoxFrom.Text = DateTime.Now.AddDays(-14).ToShortDateString();
        TextBoxTo.Text = DateTime.Now.AddDays(1).ToShortDateString();

    }


    protected void ButtonFind_Click(object sender, EventArgs e)
    {
        ReportViewer1.LocalReport.Refresh();
    }
    
    protected void DropDownListClient_SelectedIndexChanged(object sender, EventArgs e)
    {
        ReportViewer1.LocalReport.Refresh();
    }
    protected void DropDownListClient_SelectedIndexChanged1(object sender, EventArgs e)
    {
        DropDownListRegions.Items.Clear();

        ListItem myLI = new ListItem("All", "0");
        DropDownListRegions.Items.Add(myLI);



        LookupsXSDTableAdapters.ClientRegionsTableAdapter CRTA = new LookupsXSDTableAdapters.ClientRegionsTableAdapter();
        LookupsXSD.ClientRegionsDataTable CRDT = CRTA.GetDataByClientID(Convert.ToInt32(DropDownListClient.SelectedValue));

        for (int f = 0; f < CRDT.Rows.Count; f++)
        {
            LookupsXSD.ClientRegionsRow CRRow = (LookupsXSD.ClientRegionsRow)CRDT[f];
            myLI = new ListItem(CRRow.ClientRegionName, CRRow.ClientRegionName);
            DropDownListRegions.Items.Add(myLI);
        }
        ReportViewer1.LocalReport.Refresh();
    }
    protected void DropDownListRegions_SelectedIndexChanged(object sender, EventArgs e)
    {
        ReportViewer1.LocalReport.Refresh();
    }
}
