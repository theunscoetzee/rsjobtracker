﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="DailyGeneratorReport.aspx.cs" Inherits="DailyGeneratorReport" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script language="javascript" type="text/javascript">
    window.setTimeout("DoPostBack();", 50000);
    function DoPostBack() {
        document.forms[0].submit();
    }
    </script>
    </asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h1><asp:Label ID="lblHeading" runat="server" Text=""></asp:Label></h1>
    
    <asp:Panel ID="PanelFilter" runat="server">    &nbsp;<table bgcolor="Black" 
            cellpadding="1" cellspacing="1">
        <tr>
            <td bgcolor="#AAAAAA" colspan="3">
                <strong>Filter </strong></td>
        </tr>
        <tr>
            <td bgcolor="#CCCCCC">
                Client</td>
            <td bgcolor="#CCCCCC">
                Region</td>
            <td bgcolor="#CCCCCC">
                &nbsp;</td>
        </tr>
        <tr>
            <td bgcolor="#CCCCCC">
                <asp:DropDownList ID="DropDownListClient" runat="server" 
                    onselectedindexchanged="DropDownListClient_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td bgcolor="#CCCCCC">
                <asp:DropDownList ID="DropDownListRegion" runat="server">
                </asp:DropDownList>
            </td>
            <td bgcolor="#CCCCCC" style="text-align: center">
                <asp:Button ID="ButtonFind" runat="server" Height="22px" 
                    Text="Find!" Width="79px" onclick="ButtonFind_Click" />
            </td>
        </tr>
        </table>
        &nbsp;</asp:Panel>
    
    
    
    <asp:Panel ID="PanelListView" runat="server">
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" 
            Font-Size="8pt" InteractiveDeviceInfos="(Collection)" 
            WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="1700px" 
            Height="4000px" ShowPageNavigationControls="False">
            <LocalReport ReportPath="Reports\DailyGeneratorReport.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource DataSourceId="ObjectDataSourceGenerators" 
                        Name="DataSet1" />
                </DataSources>
            </LocalReport>
        </rsweb:ReportViewer>
    </asp:Panel>
    <br />
&nbsp;<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </asp:ToolkitScriptManager>
    <asp:ObjectDataSource ID="ObjectDataSourceGenerators" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        TypeName="GeneratorsXSTableAdapters.DailyGenReportTableAdapter">
        <SelectParameters>
            <asp:ControlParameter ControlID="DropDownListClient" Name="ClientID" 
                PropertyName="SelectedValue" Type="Int32" />
            <asp:ControlParameter ControlID="DropDownListRegion" Name="RegionID" 
                PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    </asp:Content>
