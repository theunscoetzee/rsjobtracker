﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DailyGeneratorReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            loadDropdowns();
            DropDownListClient.SelectedValue = "1";
        }
       
    }


    protected void loadDropdowns()
    {
       



        ListItem myLI = new ListItem("All","0");
        DropDownListClient.Items.Add(myLI);

        LookupsXSDTableAdapters.ClientsTableAdapter CTA = new LookupsXSDTableAdapters.ClientsTableAdapter();
        LookupsXSD.ClientsDataTable CDT = CTA.GetData();
        
       
        for (int f = 0; f < CDT.Rows.Count; f++)
        {
            LookupsXSD.ClientsRow CRow = (LookupsXSD.ClientsRow)CDT[f];
            myLI = new ListItem(CRow.ClientName ,CRow.ClientID.ToString());
            DropDownListClient.Items.Add(myLI);
        }

        myLI = new ListItem("All", "0");
        DropDownListRegion.Items.Add(myLI);
        LookupsXSDTableAdapters.ClientRegionsTableAdapter  CRTA = new LookupsXSDTableAdapters.ClientRegionsTableAdapter ();
        LookupsXSD.ClientRegionsDataTable CRDT = CRTA.GetData();


        for (int f = 0; f < CRDT.Rows.Count; f++)
        {
            LookupsXSD.ClientRegionsRow CRow = (LookupsXSD.ClientRegionsRow)CRDT[f];
            myLI = new ListItem(CRow.ClientRegionName, CRow.ClientRegionID.ToString());
            DropDownListRegion.Items.Add(myLI);
        }

       

       

        

    }


    protected void ButtonFind_Click(object sender, EventArgs e)
    {
        ReportViewer1.LocalReport.Refresh();
    }
    protected void DropDownListClient_SelectedIndexChanged(object sender, EventArgs e)
    {
        //ReportViewer1.LocalReport.Refresh();
    }
}
