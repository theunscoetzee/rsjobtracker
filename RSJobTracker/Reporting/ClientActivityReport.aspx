﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ClientActivityReport.aspx.cs" Inherits="ClientActivityReport" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script language="javascript" type="text/javascript">
    window.setTimeout("DoPostBack();", 50000);
    function DoPostBack() {
        document.forms[0].submit();
    }
    </script>
    </asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h1><asp:Label ID="lblHeading" runat="server" Text=""></asp:Label></h1>
    
    <asp:Panel ID="PanelFilter" runat="server">  
    <table bgcolor="Black" 
            cellpadding="1" cellspacing="1" >
        <tr>
            <td bgcolor="#AAAAAA" colspan="7">
                <strong>Filter for Job(s)</strong></td>
          
        </tr>
        <tr>
            <td bgcolor="#CCCCCC">
                Client</td>
            <td bgcolor="#CCCCCC">
                Region</td>
            <td bgcolor="#CCCCCC">
                Site</td>
            <td bgcolor="#CCCCCC">
                CallType</td>
            <td bgcolor="#CCCCCC" class="style4">
                Call received</td>
            <td bgcolor="#CCCCCC">
                JobCompleted/Cancelled</td>
            <td bgcolor="#CCCCCC">
                &nbsp;</td>
        </tr>
        <tr>
            <td bgcolor="#CCCCCC">
                <asp:DropDownList ID="DropDownListClient" runat="server" AutoPostBack="True" 
                    onselectedindexchanged="DropDownListClient_SelectedIndexChanged1">
                </asp:DropDownList>
            </td>
            <td bgcolor="#CCCCCC">
                <asp:DropDownList ID="DropDownListRegions" runat="server" AutoPostBack="True" 
                    onselectedindexchanged="DropDownListRegions_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td bgcolor="#CCCCCC">
                <asp:TextBox ID="TextBoxSite" runat="server"></asp:TextBox>
            </td>
            <td bgcolor="#CCCCCC">
                <asp:DropDownList ID="DropDownListCallType" runat="server">
                </asp:DropDownList>
            </td>
            <td bgcolor="#CCCCCC" class="style4">
                <asp:TextBox ID="TextBoxFrom" runat="server" Width="80px"></asp:TextBox>
                -<asp:TextBox ID="TextBoxTo" runat="server" Width="80px"></asp:TextBox>
            </td>
            <td bgcolor="#CCCCCC">
                <asp:DropDownList ID="DropDownListCompleted" runat="server">
                    <asp:ListItem Value="False">No</asp:ListItem>
                    <asp:ListItem Value="True" Selected="True">Yes</asp:ListItem>
                    
                </asp:DropDownList>
                &nbsp;&nbsp;
            </td>
            <td bgcolor="#CCCCCC" style="text-align: center">
                <asp:Button ID="ButtonFind" runat="server" Height="22px" 
                    onclick="ButtonFind_Click" Text="Find!" Width="79px" />
            </td>
        </tr>
        </table>
        &nbsp;</asp:Panel>
    
    
    
    <asp:Panel ID="PanelListView" runat="server">
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" 
            Font-Size="8pt" InteractiveDeviceInfos="(Collection)" 
            WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="1132px">
            <LocalReport ReportPath="Reports\ClientActivityReport.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource DataSourceId="ObjectDataSourceJobList" 
                        Name="DataSet1" />
                </DataSources>
            </LocalReport>
        </rsweb:ReportViewer>
    </asp:Panel>
    <br />
&nbsp;<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </asp:ToolkitScriptManager>
    <asp:ObjectDataSource ID="ObjectDataSourceJobList" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        TypeName="JobXSDTableAdapters.JobFilterTableAdapter">
        <SelectParameters>
            <asp:Parameter DefaultValue="0" Name="JobID" Type="Int32" />
            <asp:ControlParameter ControlID="DropDownListClient" DefaultValue="0" 
                Name="ClientID" PropertyName="SelectedValue" Type="Int32" />
            <asp:ControlParameter ControlID="DropDownListCompleted" DefaultValue="0" 
                Name="JobCompleted" PropertyName="SelectedValue" Type="Boolean" />
            <asp:ControlParameter ControlID="TextBoxSite" DefaultValue="%" Name="SiteCode" 
                PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="DropDownListCallType" DefaultValue="0" 
                Name="CallType" PropertyName="SelectedValue" Type="String" />
            <asp:ControlParameter ControlID="TextBoxFrom" DefaultValue="" Name="From" 
                PropertyName="Text" Type="DateTime" />
            <asp:ControlParameter ControlID="TextBoxTo" Name="To" PropertyName="Text" 
                Type="DateTime" />
            <asp:ControlParameter ControlID="DropDownListRegions" DefaultValue="" 
                Name="RegionName" PropertyName="SelectedValue" Type="String" />
        </SelectParameters>
</asp:ObjectDataSource>
    </asp:Content>
