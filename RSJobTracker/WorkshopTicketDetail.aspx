﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="WorkshopTicketDetail.aspx.cs" Inherits="WorkshopTicketDetail" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
        .style3
        {
            color: #FF6600;
        }
        </style>
    </asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    
        <asp:Button ID="ButtonBack" runat="server" Text="Back" 
            onclick="ButtonBack_Click" />
    &nbsp;<asp:Panel ID="PanelEditHeader" runat="server">
   <ul>
       <asp:Label ID="LabelError" runat="server" 
           style="font-size: medium; font-weight: 700; color: #FF0000" Text="Label" 
           Visible="False"></asp:Label>
&nbsp;<table width="1200" bgcolor=#333333 cellspacing =1 cellpadding=2>
           <tr>
               <td bgcolor="#AAAAAA">
                   JOB NUMBER:
                   <asp:Label ID="LabelJobNumber" runat="server" style="font-weight: 700" 
                       Text="2213"></asp:Label>
                   &nbsp;<asp:Label ID="LabelCallType" runat="server" Text="Label" 
                       style="font-weight: 700"></asp:Label>
               </td>
           </tr>
           <tr>
               <td bgcolor="#EEEEEE">
                   Work done:</td>
           </tr>
           <tr>
               <td bgcolor="#EEEEEE">
                   Add Work Done: Category:
                   <asp:DropDownList ID="DropDownListCategory" runat="server" AutoPostBack="True" 
                       onselectedindexchanged="DropDownListCategory_SelectedIndexChanged">
                   </asp:DropDownList>
                   &nbsp;<br />
                   <asp:Label ID="LabelWorkDone" runat="server" Text="work performed: " 
                       Visible="False"></asp:Label>
                   &nbsp;<asp:TextBox ID="TextBoxWorkDescription" runat="server" Visible="False" 
                       Width="235px"></asp:TextBox>
                   <asp:Label ID="LabelpartUsed" runat="server" Text="part used: " Visible="False"></asp:Label>
                   <asp:DropDownList ID="DropDownListPartUsed" runat="server" Visible="False">
                   </asp:DropDownList>
                   &nbsp;<asp:TextBox ID="TextBoxPartDescription" runat="server" Visible="False"></asp:TextBox>
                   <asp:Label ID="LabelPartNumber" runat="server" Text="part number: " 
                       Visible="False"></asp:Label>
                   <asp:TextBox ID="TextBoxPartNumber" runat="server" Visible="False"></asp:TextBox>
                   &nbsp;<asp:Label ID="LabelpartCost" runat="server" Text="cost: " Visible="False"></asp:Label>
                   <asp:TextBox ID="TextBoxCost" runat="server" Visible="False">0</asp:TextBox>
                   &nbsp;<asp:Button ID="ButtonAddLine" runat="server" onclick="Button1_Click" 
                       Text="  Add  " Visible="False" Width="75px" />
                   <br />
                   </td>
           </tr>
        
           
           
       
           <tr>
               <td bgcolor="#EEEEEE">
                   <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                       DataSourceID="ObjectDataSourceLines" Width="1200px">
                       <Columns>
                           <asp:BoundField DataField="UserName" HeaderText="UserName" 
                               SortExpression="UserName" />
                           <asp:BoundField DataField="LoggedAt" DataFormatString="{0:d}" 
                               HeaderText="LoggedAt" SortExpression="LoggedAt" />
                           <asp:BoundField DataField="Category" HeaderText="Category" 
                               SortExpression="Category" />
                           <asp:BoundField DataField="Description" HeaderText="Description" 
                               SortExpression="Description" />
                           <asp:BoundField DataField="PartNumber" HeaderText="PartNumber" 
                               SortExpression="PartNumber" />
                           <asp:BoundField DataField="PartDescription" HeaderText="PartDescription" 
                               SortExpression="PartDescription" />
                           <asp:BoundField DataField="CostExVat" HeaderText="CostExVat" 
                               SortExpression="CostExVat">
                           <ItemStyle HorizontalAlign="Right" />
                           </asp:BoundField>
                       </Columns>
                   </asp:GridView>
                   <asp:ObjectDataSource ID="ObjectDataSourceLines" runat="server" 
                       InsertMethod="Insert" OldValuesParameterFormatString="original_{0}" 
                       SelectMethod="GetDataByJobID" 
                       TypeName="WorkshopLinesXSDTableAdapters.WorkshopLinesTableAdapter">
                       <InsertParameters>
                           <asp:Parameter Name="JobID" Type="Int32" />
                           <asp:Parameter Name="UserID" Type="Int32" />
                           <asp:Parameter Name="LoggedAt" Type="DateTime" />
                           <asp:Parameter Name="Category" Type="String" />
                           <asp:Parameter Name="Description" Type="String" />
                           <asp:Parameter Name="PartNumber" Type="String" />
                           <asp:Parameter Name="PartDescription" Type="String" />
                           <asp:Parameter Name="CostExVat" Type="Decimal" />
                       </InsertParameters>
                       <SelectParameters>
                           <asp:ControlParameter ControlID="LabelJobNumber" Name="JobID" 
                               PropertyName="Text" Type="Int32" />
                       </SelectParameters>
                   </asp:ObjectDataSource>
               </td>
           </tr>
        
           
           
       
       </table>
       <table bgcolor="#333333" cellpadding="2" cellspacing="1" width="1000">
       </table>
       

     
    </ul>
      
     </asp:Panel>  
    
           <asp:Panel ID="PanelComments" runat="server">
         <ul>
           
               <span class="style3"><b>Comments and notes</b></span>
             <table bgcolor="Black" width="1000" cellspacing="1">
             <tr>
             <td bgcolor="#AAAAAA">Add comment/note:
                 <table>
                 <tr>
                 <td>
                 Comment/Note:
                 </td>
                 <td>
                 <asp:TextBox ID="TextBoxComment" runat="server" Width="663px"></asp:TextBox>
                 </td>
                     <td rowspan=2>
                          <asp:Button ID="ButtonAddLogItem" runat="server" 
                             onclick="ButtonAddLogItem_Click" Text="Add" Width="75px" />&nbsp;</td>
                 </tr>
                 <tr>
                 <td>
                 Photo(optional):
                 </td>
                 <td>
                 <asp:FileUpload ID="FileUpload1" runat="server" Width="200px" />
                 </td>
                     
                 </tr>
                 </table>
                 
             </td>
             </tr></table>
                 <asp:GridView 
           ID="GridViewComments" runat="server" AutoGenerateColumns="False" PageSize="20" 
           style="font-size: x-small" Width="1000px" 
                 DataSourceID="ObjectDataSourceJobComments">
           <AlternatingRowStyle BackColor="#DEDEDE" />
                     <Columns>
                         <asp:BoundField DataField="CommentText" HeaderText="Comment" 
                             SortExpression="CommentText" />
                         <asp:BoundField DataField="CommentAt" HeaderText="Logged At" 
                             SortExpression="CommentAt" />
                         <asp:BoundField DataField="UserName" HeaderText="Logged by User" 
                             SortExpression="UserName" />
                         <asp:TemplateField HeaderText="Photo" SortExpression="ImageURL">
                             <ItemTemplate>
                                 <asp:HyperLink ID="HyperLink2" runat="server" 
                                     NavigateUrl='<%# "uploadedimages/" + Eval("ImageURL") %>' 
                                     Text='<%# Eval("ImageURL") %>'></asp:HyperLink>
                             </ItemTemplate>
                             <EditItemTemplate>
                                 <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("ImageURL") %>'></asp:TextBox>
                             </EditItemTemplate>
                         </asp:TemplateField>
                     </Columns>
           <HeaderStyle BackColor="#CCCCCC" />
           <RowStyle BackColor="White" />
       </asp:GridView>
             
                 
                 
             
             <asp:ObjectDataSource ID="ObjectDataSourceJobComments" runat="server" 
                 OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByJobID" 
                 TypeName="JobXSDTableAdapters.JobCommentsTableAdapter">
                 <SelectParameters>
                     <asp:SessionParameter Name="JobID" SessionField="JobID" Type="Int32" />
                 </SelectParameters>
             </asp:ObjectDataSource>
             
                 
                 
             
       </ul>
       </asp:Panel>
   
    
    <br />
&nbsp;<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </asp:ToolkitScriptManager>
           &nbsp;&nbsp;
           </asp:Content>
