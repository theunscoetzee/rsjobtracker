﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MobileDriverTicketList : System.Web.UI.Page
{
    int ctopen = 0;
    int ctcan = 0;
    int cthold = 0;
    int ctover90 = 0;
    int ctover180 = 0;
    int ctrefill = 0;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {

            Session["RSGDriverID"] = Request.QueryString["DID"].ToString();

            LookupsXSDTableAdapters.RSGDriversTableAdapter DTA = new LookupsXSDTableAdapters.RSGDriversTableAdapter();
            LookupsXSD.RSGDriversDataTable DDT = DTA.GetDataByDriverID(Convert.ToInt32(Session["RSGDriverID"]));
            LookupsXSD.RSGDriversRow DRow = (LookupsXSD.RSGDriversRow)DDT[0];
            loadDropdowns();
            LabelDriver.Text = DRow.RSGDriverName;
            GridView1.DataBind();
            MultiView1.ActiveViewIndex = 0;
            
        }
     
        GridView1.DataBind();
    }
    

    protected void loadDropdowns()
    {
       




    }


  

   
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        LinkButton myLB = (LinkButton)sender;
        
        Session["Mode"] = "Edit";
        Session["JobID"] = myLB.Text;
        Response.Redirect("TicketDetail.aspx");

    }
    

   
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
       



        



           
        


    }
    protected void LinkButtonJobID_Click(object sender, EventArgs e)
    {
     
      
    }


    protected void DropDownListDriver_SelectedIndexChanged(object sender, EventArgs e)
    {
        GridView1.DataBind();
    }
    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton myIB = (ImageButton)sender;
        Label myLbl = (Label)myIB.Parent.FindControl("LabelJobAcknowledged");
        myLbl.Text = "Job Acknowledged: " + DateTime.Now.ToString();
        myLbl.BackColor = System.Drawing.Color.Green;
        myIB.Visible = false;
        myIB = (ImageButton)myIB.Parent.FindControl("ImageButtonAtDepot");
        myIB.Visible = true;
    }
    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
   
    protected void ImageButtonAtDepot_Click1(object sender, ImageClickEventArgs e)
    {
        ImageButton myIB = (ImageButton)sender;
        Label myLbl = (Label)myIB.Parent.FindControl("LabelArriveDepot");
        myLbl.Text = "At Depot: " + DateTime.Now.ToString();
        myLbl.BackColor = System.Drawing.Color.Green;
        myIB.Visible = false;
        myIB = (ImageButton)myIB.Parent.FindControl("ImageButtonLeaveDepot");
        myIB.Visible = true;
    }
    protected void ImageButtonLeaveDepot_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton myIB = (ImageButton)sender;
        Label myLbl = (Label)myIB.Parent.FindControl("LabelLeaveDepot");
        myLbl.Text = "Leave Depot: " + DateTime.Now.ToString();
        myLbl.BackColor = System.Drawing.Color.Green;
        myIB.Visible = false;
        myIB = (ImageButton)myIB.Parent.FindControl("ImageButtonArriveSite");
        myIB.Visible = true;
    }
    protected void ImageButtonArriveSite_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton myIB = (ImageButton)sender;
        Label myLbl = (Label)myIB.Parent.FindControl("LabelArriveSite");
        myLbl.Text = "Arrive Site: " + DateTime.Now.ToString();
        myLbl.BackColor = System.Drawing.Color.Green;
        myIB.Visible = false;
        myIB = (ImageButton)myIB.Parent.FindControl("ImageButtonJobCompleted");
        myIB.Visible = true;
    }
    protected void ImageButtonJobCompleted_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton myIB = (ImageButton)sender;
        Label myLbl = (Label)myIB.Parent.FindControl("LabelJobCompleted");
        myLbl.Text = "Job Completed: " + DateTime.Now.ToString();
        myLbl.BackColor = System.Drawing.Color.Green;
        myIB.Visible = false;
       
    }
    protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton myLB = (ImageButton)sender;
        Session["JobID"] = myLB.CommandArgument;
        Session["Mode"] = "Edit";
        MultiView1.ActiveViewIndex = 1;
        GridView2.DataBind();
    }
    protected void ImageButtonBack_Click(object sender, ImageClickEventArgs e)
    {
        MultiView1.ActiveViewIndex = 0;
    }
}
