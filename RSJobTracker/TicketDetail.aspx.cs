﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;


public partial class MyTickets : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

       if (!IsPostBack)
       {

            if (Session["Mode"] != null)
            {
                if (Session["Mode"] == "Add")
                {
                    LabelJobNumber.Text = "New";
                    PanelComments.Visible = false;
                    
                    ButtonAddUpdate.Text = "Add Job";
                    LabelCallReceivedBy.Visible = false;
                    LabelDriverDispatchedBy.Visible = false;
                    LabelDriverDispatchedBy1.Visible = false;
                    ButtonAddBowserLiters.Enabled = false;
                    ButtonAddGenLiters.Enabled = false;
                    CheckBoxCompleted.Enabled = false;
                    CheckBoxCancelled.Enabled = false;
                    CheckBoxOnHold.Enabled = false;
                    loadDropdowns();
                    pnlFuel.Visible=false;
                    ButtonSendSMS.Enabled = false;
                }
            }
            if (Session["Mode"].ToString() == "Edit")
            {

                ButtonAddUpdate.Text = "Update Job";
                PanelEditHeader.Visible = true;
                PanelComments.Visible = true;
                ButtonAddBowserLiters.Enabled = true;
                ButtonAddGenLiters.Enabled = true;
                CheckBoxCompleted.Enabled = true;
                CheckBoxCancelled.Enabled = true;
                CheckBoxOnHold.Enabled = true;

                loadDropdowns();
                LoadJobData(Convert.ToInt32(Session["JobID"]));
                ButtonSendSMS.Enabled = true;

                pnlFuel.Visible = true;

                ButtonAddUpdate.Visible = false;

            }
           
            
            

        }
       
    }

    


    protected void loadDropdowns()
    {
       

        ListItem myLI = new ListItem("Select...", "0");
        if (Session["Mode"].ToString()=="Add") DropDownListClient.Items.Add(myLI);

        LookupsXSDTableAdapters.ClientsTableAdapter CTA = new LookupsXSDTableAdapters.ClientsTableAdapter();
        LookupsXSD.ClientsDataTable CDT = CTA.GetData();


        for (int f = 0; f < CDT.Rows.Count; f++)
        {
            LookupsXSD.ClientsRow CRow = (LookupsXSD.ClientsRow)CDT[f];
            myLI = new ListItem(CRow.ClientName, CRow.ClientID.ToString());
            DropDownListClient.Items.Add(myLI);
        }



        myLI = new ListItem("Select...", "0");
        DropDownListRSGSite.Items.Add(myLI);

        LookupsXSDTableAdapters.RSGSitesTableAdapter DTA = new LookupsXSDTableAdapters.RSGSitesTableAdapter();
        LookupsXSD.RSGSitesDataTable DDT = DTA.GetData();


        for (int f = 0; f < DDT.Rows.Count; f++)
        {
            LookupsXSD.RSGSitesRow DDRow = (LookupsXSD.RSGSitesRow)DDT[f];
            myLI = new ListItem(DDRow.RSGSiteName , DDRow.RSGSiteID .ToString());
            DropDownListRSGSite.Items.Add(myLI);
        }

       


      

        myLI = new ListItem("Select...", "0");
        DropDownListCallType.Items.Add(myLI);
        
        
        LookupsXSDTableAdapters.CallTypesTableAdapter CTTA = new LookupsXSDTableAdapters.CallTypesTableAdapter();
        LookupsXSD.CallTypesDataTable CTDT = CTTA.GetData();

        for (int f = 0; f < CTDT.Rows.Count; f++)
        {
            LookupsXSD.CallTypesRow CTRow = (LookupsXSD.CallTypesRow)CTDT[f];
            myLI = new ListItem(CTRow.CallType, CTRow.CallType);
            DropDownListCallType.Items.Add(myLI);
        }

        


        

      
        //myLI = new ListItem("Select...", "0");
        //DropDownListGenerator.Items.Add(myLI);
        //LookupsXSDTableAdapters.GeneratorsTableAdapter GTA = new LookupsXSDTableAdapters.GeneratorsTableAdapter();
        //LookupsXSD.GeneratorsDataTable GDT = GTA.GetData();

        //for (int f = 0; f < GDT.Rows.Count; f++)
        //{
        //    LookupsXSD.GeneratorsRow GRow = (LookupsXSD.GeneratorsRow)GDT[f];
        //    myLI = new ListItem(GRow.GeneratorRegNumber ,GRow.GeneratorID.ToString() );
        //    DropDownListGenerator.Items.Add(myLI);
        //}

        //myLI = new ListItem("Select...", "0");
        //DropDownListBowser.Items.Add(myLI);
        //LookupsXSDTableAdapters.BowsersTableAdapter BTA = new LookupsXSDTableAdapters.BowsersTableAdapter();
        //LookupsXSD.BowsersDataTable BDT = BTA.GetData();

        //for (int f = 0; f < BDT.Rows.Count; f++)
        //{
        //    LookupsXSD.BowsersRow BRow = (LookupsXSD.BowsersRow)BDT[f];
        //    myLI = new ListItem(BRow.BowserReg ,BRow.BowserID.ToString());
        //    DropDownListBowser.Items.Add(myLI);
        //}

        //defaults

        TextBoxGenFillAt.Text = String.Format("{0:yyyy/MM/dd HH:mm}",DateTime.Now);
        TextBoxBowserFillAt.Text = String.Format("{0:yyyy/MM/dd HH:mm}",DateTime.Now);
        TextBoxGenFillLiters.Text = "0";
        TextBoxBowserFillLiters.Text = "0";

        if (TextBoxCallRec.Text == "") TextBoxCallRec.Text = DateTime.Now.ToString();
        
    }




    protected void ButtonAddLogItem_Click(object sender, EventArgs e)
    {
        if (TextBoxComment.Text != "")
        {

            //upload photo if one was specified
            String savePath = "";
            String fileName = "";
            if (FileUpload1.HasFile)
            {
                savePath = ConfigurationManager.AppSettings["UploadPhotoSavepath"] + "/";
                // Get the name of the file to upload.
                fileName = FileUpload1.FileName;

                // Append the name of the file to upload to the path.
                savePath += fileName;
                FileUpload1.SaveAs(savePath);

            }



            JobXSDTableAdapters.JobCommentsTableAdapter JCTA = new JobXSDTableAdapters.JobCommentsTableAdapter();
            int x = JCTA.InsertQuery(Convert.ToInt32(Session["JobID"]), TextBoxComment.Text, DateTime.Now, Convert.ToInt32(Session["UserID"]), fileName);

        }
        GridViewComments.DataBind();
    }
    

    protected void DropDownListClient_SelectedIndexChanged(object sender, EventArgs e)
    {
        
        LoadRegion();
    }

    protected void LoadRegion()
    {
        DropDownListRegion.Items.Clear();
        DropDownListSubRegion.Items.Clear();
        DropDownListSite.Items.Clear();
        ListItem myLI = new ListItem("Select...", "0");
       DropDownListRegion.Items.Add(myLI);

        if (DropDownListClient.SelectedValue != "0")
        {

            LookupsXSDTableAdapters.ClientRegionsTableAdapter CRTA = new LookupsXSDTableAdapters.ClientRegionsTableAdapter();
            LookupsXSD.ClientRegionsDataTable CRDT = CRTA.GetDataByClientID(Convert.ToInt32(DropDownListClient.SelectedValue));


            for (int f = 0; f < CRDT.Rows.Count; f++)
            {
                LookupsXSD.ClientRegionsRow CRow = (LookupsXSD.ClientRegionsRow)CRDT[f];
                myLI = new ListItem(CRow.ClientRegionName, CRow.ClientRegionID.ToString());
                DropDownListRegion.Items.Add(myLI);
            }
        }
    }

    protected void DropDownListRegion_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadSubRegion();
        
    }

    protected void LoadSubRegion()
    {
        DropDownListSubRegion.Items.Clear();
        DropDownListSite.Items.Clear();

        ListItem myLI = new ListItem("Select...", "0");
        DropDownListSubRegion.Items.Add(myLI);

        if (DropDownListRegion.SelectedValue != "0")
        {
            LookupsXSDTableAdapters.ClientSubRegionsTableAdapter CSRTA = new LookupsXSDTableAdapters.ClientSubRegionsTableAdapter();
            LookupsXSD.ClientSubRegionsDataTable CSRDT = CSRTA.GetDataByClientRegionID(Convert.ToInt32(DropDownListRegion.SelectedValue));


            for (int f = 0; f < CSRDT.Rows.Count; f++)
            {
                LookupsXSD.ClientSubRegionsRow CRow = (LookupsXSD.ClientSubRegionsRow)CSRDT[f];

                myLI = new ListItem(CRow.SubRegionName, CRow.ClientSubRegionID.ToString());
                DropDownListSubRegion.Items.Add(myLI);
            }
        }
    }


    protected void DropDownListSubRegion_SelectedIndexChanged(object sender, EventArgs e)
    {
       // LoadSite();
        loadGenerators(0);
        loadBowsers();
    }

    protected void loadGenerators(int selectedID)
    {
        DropDownListGenerator.Items.Clear();
        ListItem myLI = new ListItem("","0");
        DropDownListGenerator.Items.Add(myLI);

        if (DropDownListCallType.SelectedValue == "Call out" | DropDownListCallType.SelectedValue == "Recovered Generator" | DropDownListCallType.SelectedValue == "Refill Generator" | DropDownListCallType.SelectedValue == "Service" | DropDownListCallType.SelectedValue == "Spill clean up" | DropDownListCallType.SelectedValue == "Swop - Recover" | DropDownListCallType.SelectedValue == "Wash Generator")
        {
            // All generators
            LookupsXSDTableAdapters.GeneratorsTableAdapter GTA = new LookupsXSDTableAdapters.GeneratorsTableAdapter();
            LookupsXSD.GeneratorsDataTable GDT = GTA.GetDataByAllSubRegionID(Convert.ToInt32(DropDownListSubRegion.SelectedValue));
            if (GDT.Rows.Count == 0)
            {
                //generators at region?
                GDT = GTA.GetDataByAllRegionID(Convert.ToInt32(DropDownListRegion.SelectedValue));
            }

            if (GDT.Rows.Count > 0)
            {
                for (int f = 0; f < GDT.Rows.Count; f++)
                {
                    LookupsXSD.GeneratorsRow GRow = (LookupsXSD.GeneratorsRow)GDT[f];
                    myLI = new ListItem(GRow.GeneratorRegNumber, GRow.GeneratorID.ToString());
                    DropDownListGenerator.Items.Add(myLI);
                }
            }

            try
            {
                // try to set selected to generator at site
                ListItem myLIsel = DropDownListSite.SelectedItem;
                //Response.Write("[" + myLIsel.Text + "]");
                GDT = GTA.GetDataByLastLocation(myLIsel.Text);
                if (GDT.Rows.Count > 0)
                {
                    LookupsXSD.GeneratorsRow GRow2 = (LookupsXSD.GeneratorsRow)GDT[0];
                    myLI = new ListItem(GRow2.GeneratorRegNumber, GRow2.GeneratorID.ToString());
                    DropDownListGenerator.Items.Add(myLI);
                    DropDownListGenerator.SelectedValue = GRow2.GeneratorID.ToString();
                }
            }
            catch
            {
            }
            
        
        
        }



        if (DropDownListCallType.SelectedValue == "Deployed & Recovered Generator" | DropDownListCallType.SelectedValue == "Deployed Generator" | DropDownListCallType.SelectedValue == "Swop - Deploy" | DropDownListCallType.SelectedValue == "Workshop Service Generator")
        {

            // Only want aviailbale generators
            //Generators at sub region?
            LookupsXSDTableAdapters.GeneratorsTableAdapter GTA = new LookupsXSDTableAdapters.GeneratorsTableAdapter();
            LookupsXSD.GeneratorsDataTable GDT = GTA.GetDataByAvailableSubRegionID(Convert.ToInt32(DropDownListSubRegion.SelectedValue), selectedID);
            if (GDT.Rows.Count == 0)
            {
                //generators at region?
                GDT = GTA.GetDataByAvailableRegionID(Convert.ToInt32(DropDownListRegion.SelectedValue), selectedID);
            }

            if (GDT.Rows.Count > 0)
            {
                for (int f = 0; f < GDT.Rows.Count; f++)
                {
                    LookupsXSD.GeneratorsRow GRow = (LookupsXSD.GeneratorsRow)GDT[f];
                    myLI = new ListItem(GRow.GeneratorRegNumber, GRow.GeneratorID.ToString());
                    DropDownListGenerator.Items.Add(myLI);
                }
            }
        }
    }

    protected void loadBowsers()
    {
        DropDownListBowser.Items.Clear();
        ListItem myLI = new ListItem("", "0");
        DropDownListBowser.Items.Add(myLI);
        LookupsXSDTableAdapters.BowsersTableAdapter BTA = new LookupsXSDTableAdapters.BowsersTableAdapter();
        LookupsXSD.BowsersDataTable BDT = BTA.GetDataBySubRegionID(Convert.ToInt32(DropDownListSubRegion.SelectedValue));
        if (BDT.Rows.Count == 0)
        {
            BDT = BTA.GetDataByRegionID(Convert.ToInt32(DropDownListRegion.SelectedValue));
        }

        if (BDT.Rows.Count > 0)
        {
            for (int f = 0; f < BDT.Rows.Count; f++)
            {
                LookupsXSD.BowsersRow BRow = (LookupsXSD.BowsersRow)BDT[f];
                myLI = new ListItem(BRow.BowserReg, BRow.BowserID.ToString());
                DropDownListBowser.Items.Add(myLI);
            }
        }

    }


    protected void LoadSite()
    {
        DropDownListSite.Items.Clear(); 
        ListItem myLI = new ListItem("Select...", "0");
       DropDownListSite.Items.Add(myLI);

        if (DropDownListSubRegion.SelectedValue != "0")
        {
            string FilterText = "%";
            if (TextBoxSiteFilter.Text != "") { FilterText = TextBoxSiteFilter.Text; }

            LookupsXSDTableAdapters.ClientSitesTableAdapter CSTA = new LookupsXSDTableAdapters.ClientSitesTableAdapter();
            LookupsXSD.ClientSitesDataTable CSDT = CSTA.GetDataByRegionIDSiteFilter(Convert.ToInt32(DropDownListRegion.SelectedValue),FilterText);

            for (int f = 0; f < CSDT.Rows.Count; f++)
            {
                LookupsXSD.ClientSitesRow CSRow = (LookupsXSD.ClientSitesRow)CSDT[f];
                myLI = new ListItem(CSRow.ClientSiteCode + " : "+ CSRow.ClientSiteCounty + " : "+CSRow.ClientSiteName , CSRow.ClientSiteID.ToString());
                DropDownListSite.Items.Add(myLI);
            }
        }
    }


    protected void DropDownListSite_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList myDD = (DropDownList)sender; 
        ListItem myLI=myDD.SelectedItem;

        LookupsXSDTableAdapters.ClientSitesTableAdapter CSTA = new LookupsXSDTableAdapters.ClientSitesTableAdapter();
        LookupsXSD.ClientSitesDataTable CSDT = CSTA.GetDataByClientSiteID (Convert.ToInt32(myDD.SelectedValue ));
        LookupsXSD.ClientSitesRow CRow = (LookupsXSD.ClientSitesRow)CSDT[0];
        LabelSiteLoc.Text = "<br>" + CRow.ClientSiteCode +":" + CRow.ClientSiteName  +" &nbsp;Site lat/long: " +CRow.ClientSiteLat + "  /  " + CRow.ClientSiteLong;
        HyperLink1.NavigateUrl = "http://maps.google.co.za/maps?q=" + CRow.ClientSiteLat + "," + CRow.ClientSiteLong ;

        if (!CRow.IsClientSiteOwnerNull()) TextBoxClientSiteOwner.Text = CRow.ClientSiteOwner;

        //if (!CRow.IsClientSitePriorityNull()) LabelClientSitePriority.Text = CRow.ClientSitePriority.ToString();
        if (!CRow.IsClientSiteResponseTimeNull())
        {
            DateTime CalcETA = DateTime.Now.AddHours(CRow.ClientSiteResponseTime);
            LabelCalculatedETA.Text = CalcETA.Year + "/" + CalcETA.Month + "/" + CalcETA.Day + " " + CalcETA.Hour + ":" + CalcETA.Minute; 
        }



        if (Session["Mode"].ToString() == "Edit") updateJob();

        //see if generator deployed at site
        GeneratorsXSTableAdapters.GeneratorStatusTableAdapter GTA = new GeneratorsXSTableAdapters.GeneratorStatusTableAdapter();
        GeneratorsXS.GeneratorStatusDataTable GDT = GTA.GetDataByLastLocation(myLI.Text);
        if (GDT.Rows.Count > 0)
        {
            GeneratorsXS.GeneratorStatusRow GRow = (GeneratorsXS.GeneratorStatusRow)GDT[0];
            LabelSiteLoc.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + GRow.GeneratorRegNumber + " deployed at this site.";
        }

        loadGenerators(0);
    }


    protected void DropDownListRSGSite_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadDriversForRSGSite(0);
        LoadVehcilesAtRSGSite();
        if (Session["Mode"].ToString() == "Edit") updateJob();
    }

    protected void LoadDriversForRSGSite(int RSGDriverID)
    {
        //drivers at site
        int RSGSiteID = Convert.ToInt32(DropDownListRSGSite.SelectedValue );
        DropDownListDriver.Items.Clear();
        ListItem myLI = new ListItem("Select...", "0");
         DropDownListDriver.Items.Add(myLI);
        DriversXSDTableAdapters.DriversShiftStatusTableAdapter DSTA = new DriversXSDTableAdapters.DriversShiftStatusTableAdapter();
        DriversXSD.DriversShiftStatusDataTable DDT = DSTA.GetDataByOnDutyDriverAtRSSite(RSGSiteID,RSGDriverID );
        
        for (int f = 0; f < DDT.Rows.Count; f++)
        {
            DriversXSD.DriversShiftStatusRow DDRow = (DriversXSD.DriversShiftStatusRow)DDT[f]; 
            
            myLI = new ListItem(DDRow.RSGDriverName , DDRow.RSGDriverID.ToString());
            DropDownListDriver.Items.Add(myLI);
        }
    }
    protected void LoadVehcilesAtRSGSite()
    {
        //vehicles at site
        int RSGSiteID = Convert.ToInt32(DropDownListRSGSite.SelectedValue);
        DropDownListVehicle.Items.Clear();
        ListItem myLI = new ListItem("Select...", "0");
         DropDownListVehicle.Items.Add(myLI);
        LookupsXSDTableAdapters.RSGVehiclesTableAdapter VTA = new LookupsXSDTableAdapters.RSGVehiclesTableAdapter();
        LookupsXSD.RSGVehiclesDataTable VDT = VTA.GetDataByRSGSiteID(Convert.ToInt32(RSGSiteID ));
        

        for (int f = 0; f < VDT.Rows.Count; f++)
        {
            LookupsXSD.RSGVehiclesRow VRow = (LookupsXSD.RSGVehiclesRow)VDT[f];
            myLI = new ListItem(VRow.RSGVehicleRegNumber, VRow.RSGVehicleID.ToString());
            DropDownListVehicle.Items.Add(myLI);
        }

    }
    protected void ButtonSetCallRec_Click(object sender, EventArgs e)
    {
        if (TextBoxCallRec.Text == "")
        {
            TextBoxCallRec.Text = String.Format("{0:yyyy/MM/dd HH:mm}",DateTime.Now);
            LabelCallReceivedByID.Text = Session["UserName"].ToString();
        }
    }
    protected void ButtonSetDisp_Click(object sender, EventArgs e)
    {
       
            if (TextBoxDisp.Text == "")
            {
                TextBoxDisp.Text = String.Format("{0:yyyy/MM/dd HH:mm}", DateTime.Now);
                LabelDriverDispatchedByID.Text = Session["UserID"].ToString();
                if (Session["Mode"].ToString() == "Edit") UpdateTimes();
            }
        
    }

    protected void UpdateTimes()
    {
        
        DateTime? disptime = null;
        int? dispid=null;
        DateTime? siteuptime = null;
        int? siteupid   =null;

        try
        {
            disptime=Convert.ToDateTime(TextBoxDisp.Text);
            dispid=Convert.ToInt32(LabelDriverDispatchedByID.Text);
        }
        catch{}
        
        try
        {
            siteuptime=Convert.ToDateTime(TextBoxSiteUp.Text);
            siteupid=Convert.ToInt32(LabelSiteUpByID.Text);
        }
        catch{}


        try
        {
            JobXSDTableAdapters.JobTableAdapter JTA = new JobXSDTableAdapters.JobTableAdapter();
            int x = JTA.UpdateTimes(disptime, dispid, siteuptime, siteupid, Convert.ToInt32(Session["JobID"]));
        }
        catch
        {
        }
    }
    protected void ButtonSetSiteUp_Click(object sender, EventArgs e)
    {
        if (TextBoxSiteUp.Text == "")
        {
            TextBoxSiteUp.Text = String.Format("{0:yyyy/MM/dd HH:mm}",DateTime.Now);
            LabelSiteUpByID.Text = Session["UserID"].ToString();
            if (Session["Mode"].ToString() == "Edit") UpdateTimes();
        }

       

    }

    protected string Addvalidation()
    {
        string error = "";
        if (DropDownListSite.SelectedValue=="0") {error += "Client Site Must be specified";}
        if (TextBoxCallRec.Text=="") 
        {
            error += "Call received time must bespecified";
        }
        else
        {
        try
            {
                DateTime myDT = Convert.ToDateTime(TextBoxCallRec.Text);
            }
            catch
            {
                error += "Call Recieved Time is not a valid date and time";
            }
        }
        return error;

        if (DropDownListCallType.SelectedValue == "0")
        {
            error += "Call type must be specified";
        }



    }

   




    protected void ButtonAddUpdate_Click(object sender, EventArgs e)
    {
        Button myBut = (Button)sender;




        if (myBut.Text.Contains("Add"))
        {
            // do add validation
            if (Addvalidation() == "")
            {
                DateTime? disp = null;
                DateTime? siteup = null;
                int? dispby = null;
                int? siteupby = null;
                DateTime rec = Convert.ToDateTime(TextBoxCallRec.Text);
                if (TextBoxDisp.Text != "") { disp = Convert.ToDateTime(TextBoxDisp.Text); dispby = Convert.ToInt32(Session["UserID"]); }
                if (TextBoxSiteUp.Text != "") { siteup = Convert.ToDateTime(TextBoxSiteUp.Text); siteupby = Convert.ToInt32(Session["UserID"]); }
                int? RSGSiteID = null;
                int? DriverID = null;
                int? VehicleID = null;

                if (DropDownListRSGSite.SelectedValue != "0")
                {
                    RSGSiteID = Convert.ToInt32(DropDownListRSGSite.SelectedValue);
                    DriverID = Convert.ToInt32(DropDownListDriver.SelectedValue);
                    VehicleID = Convert.ToInt32(DropDownListVehicle.SelectedValue);

                }

                DateTime? CalcETA = null;
                try
                {
                    CalcETA = Convert.ToDateTime(LabelCalculatedETA.Text);
                }
                catch { }
                DateTime? CommittedETA = null;
                try
                {
                    CommittedETA = Convert.ToDateTime(TextBoxCommitedETA.Text);
                }
                catch { }

                JobXSDTableAdapters.JobTableAdapter JTA = new JobXSDTableAdapters.JobTableAdapter();
                int x = JTA.InsertNewJob(Convert.ToInt32(DropDownListSite.SelectedValue), TextBoxTicketNumber.Text, DropDownListCallType.SelectedValue, Convert.ToInt32(DropDownListGenerator.SelectedValue), Convert.ToInt32(DropDownListBowser.SelectedValue), Convert.ToDateTime(TextBoxCallRec.Text), Convert.ToInt32(Session["UserID"]), DriverID, VehicleID , disp, dispby, siteup, siteupby, TextBoxTrackerRef.Text, false,CalcETA); 

                JobXSD.JobDataTable JDT = JTA.GetDataByFindNew(DropDownListCallType.Text,rec,Convert.ToInt32(DropDownListSite.SelectedValue));
                if (JDT.Rows.Count>0)
                {
                    JobXSD.JobRow JRow = (JobXSD.JobRow)JDT[0];
                    int JobID=JRow.JobID ;
                    Session["JobID"]=JobID;
                    Session["Mode"]="Edit";
                    Response.Redirect("TicketDetail.aspx"); 
                }
            }
        }

        if (myBut.Text.Contains("Update"))
        {
            updateJob();
            Response.Redirect("TicketList.aspx");
        }

    }

    protected void updateJob()
    {
        LabelError.Visible = false;
        if (!CheckBoxCompleted.Checked)
        {
            if (Addvalidation() == "")
            {
                DateTime? disp = null;
                DateTime? siteup = null;
                
                int? dispby = null;
                int? siteupby = null;
                DateTime rec = Convert.ToDateTime(TextBoxCallRec.Text);
                if (TextBoxDisp.Text != "") { disp = Convert.ToDateTime(TextBoxDisp.Text); dispby = Convert.ToInt32(LabelDriverDispatchedByID.Text); }
                if (TextBoxSiteUp.Text != "") { siteup = Convert.ToDateTime(TextBoxSiteUp.Text); siteupby = Convert.ToInt32(LabelSiteUpByID.Text); }
                int? RSGSiteID = null;
                int? DriverID = null;
                int? VehicleID = null;

                if (DropDownListRSGSite.SelectedValue != "0")
                {
                    RSGSiteID = Convert.ToInt32(DropDownListRSGSite.SelectedValue);
                    DriverID = Convert.ToInt32(DropDownListDriver.SelectedValue);
                    VehicleID = Convert.ToInt32(DropDownListVehicle.SelectedValue);

                }

                int? RecbyID = Convert.ToInt32(LabelCallReceivedByID.Text);
                int? curhours=null;
                if(TextBoxLastHours.Text !="")
                {
                    curhours = Convert.ToInt32(TextBoxLastHours.Text);
                    //save Generator hours if required
                    GeneratorsXSTableAdapters.QueriesTableAdapter QTA = new GeneratorsXSTableAdapters.QueriesTableAdapter();
                    int y = QTA.UpdateLastHours(curhours, DateTime.Now, Convert.ToInt32(Session["JobID"]), Convert.ToInt32(DropDownListGenerator.SelectedValue));
                }
                bool completed = false;
                if (CheckBoxCompleted.Checked) { completed = true; }
               
                    JobXSDTableAdapters.JobTableAdapter JTA = new JobXSDTableAdapters.JobTableAdapter();
                    int x = JTA.UpdateJob(Convert.ToInt32(DropDownListSite.SelectedValue), TextBoxTicketNumber.Text, DropDownListCallType.SelectedValue, Convert.ToInt32(DropDownListGenerator.SelectedValue), Convert.ToInt32(DropDownListBowser.SelectedValue), rec, RecbyID, DriverID, VehicleID, disp, dispby, siteup, siteupby, curhours, 0, completed,Convert.ToInt32 (DropDownListService.SelectedValue),DropDownListGenMob.Text,DropDownListAlarmActive.Text,TextBoxClientSiteOwner.Text,Convert.ToInt32(Session["JobID"]));
               
                    

               
            }
        }
        else
        {
            LoadJobData(Convert.ToInt32(Session["JobID"]));
            LabelError.Text = "Update not done - this Job has been completed";
            LabelError.Visible = true;
        }

    }

    protected string updateGenerator()
    {
        //Update generator last hours/service if needed
        if (DropDownListGenerator.SelectedValue != "0")
        {
            GeneratorsXSTableAdapters.QueriesTableAdapter QTA = new GeneratorsXSTableAdapters.QueriesTableAdapter();
            int x;

            if (DropDownListService.Text != "0")
            {

                if (TextBoxLastHours.Text == "") { return "service hours must be supplied"; }

                    x = QTA.UpdateLastService(Convert.ToInt32(DropDownListService.Text), Convert.ToDateTime(TextBoxSiteUp.Text), Convert.ToInt32(TextBoxLastHours.Text), Convert.ToInt32(Session["JobID"].ToString()), Convert.ToInt32(DropDownListGenerator.SelectedValue));
                    x = QTA.UpdateLastHours(Convert.ToInt32(TextBoxLastHours.Text), Convert.ToDateTime(TextBoxSiteUp.Text), Convert.ToInt32(Session["JobID"].ToString()), Convert.ToInt32(DropDownListGenerator.SelectedValue));
                

            }
            else
            {
                if (TextBoxLastHours.Text != "")
                {
                    x = QTA.UpdateLastHours(Convert.ToInt32(TextBoxLastHours.Text), Convert.ToDateTime(TextBoxSiteUp.Text), Convert.ToInt32(Session["JobID"].ToString()), Convert.ToInt32(DropDownListGenerator.SelectedValue));
                
                }
            }

            //update last location
            if (DropDownListCallType.Text == "Recovered Generator" | DropDownListCallType.Text=="Deployed & Recovered Generator" | DropDownListCallType.Text == "Call Cancelled")
            {
               
                x = QTA.UpdateLastLocation("Yard", Convert.ToDateTime(TextBoxSiteUp.Text), Convert.ToInt32(DropDownListGenerator.SelectedValue));
            }
            if (DropDownListCallType.Text == "Deployed Generator" | DropDownListCallType.Text == "Call out" )
            
            { 
                ListItem myLI = (ListItem)DropDownListSite.SelectedItem;

                x = QTA.UpdateLastLocation(myLI.Text, Convert.ToDateTime(TextBoxSiteUp.Text), Convert.ToInt32(DropDownListGenerator.SelectedValue));
            }

        }
        return ("");
    }

    protected void LoadJobData(int JobID)
    {
        JobXSDTableAdapters.JobTableAdapter JTA = new JobXSDTableAdapters.JobTableAdapter();
        JobXSD.JobDataTable JDT = JTA.GetDataByJobID (JobID);
        JobXSD.JobRow JRow = (JobXSD.JobRow)JDT[0];

        LabelJobNumber.Text = JRow.JobID.ToString() ;
        DropDownListClient.SelectedValue=JRow.ClientID.ToString();
        LoadRegion();
        DropDownListRegion.SelectedValue=JRow.ClientRegionID.ToString();
        LoadSubRegion();
        DropDownListSubRegion.SelectedValue=JRow.ClientSubRegionID.ToString();
        TextBoxSiteFilter.Text = JRow.ClientSiteName;
        LoadSite();
        DropDownListSite.SelectedValue=JRow.ClientSiteID.ToString();

        if (!JRow.IsTicketNumberNull()) { TextBoxTicketNumber.Text = JRow.TicketNumber; }
        DropDownListCallType.Text = JRow.CallType;

        if (!JRow.IsCallReceivedAtNull()) { TextBoxCallRec.Text = String.Format("{0:yyyy/MM/dd HH:mm}",JRow.CallReceivedAt); }
        if (!JRow.IsDriverDispatchedAtNull()) { TextBoxDisp.Text = String.Format("{0:yyyy/MM/dd HH:mm}",JRow.DriverDispatchedAt); }
        if (!JRow.IsSiteUpAtNull()) { TextBoxSiteUp.Text = String.Format("{0:yyyy/MM/dd HH:mm}",JRow.SiteUpAt); }

        if (!JRow.IsCallReceivedUserNull()) { LabelCallReceivedBy.Text = JRow.CallReceivedUser; LabelCallReceivedByID.Text = JRow.CallReceivedByUserID.ToString(); }
        if (!JRow.IsDispatchedUserNull()) { LabelDriverDispatchedBy.Text = JRow.DispatchedUser; LabelDriverDispatchedByID.Text = JRow.DriverDispatchedByUserID.ToString(); }
        if (!JRow.IsSiteUpByUserIDNull()) { LabelSiteUpBy.Text = JRow.SiteUpUser; LabelSiteUpByID.Text = JRow.SiteUpByUserID.ToString(); }

        if (!JRow.IsAlarmStatusNull()) { DropDownListAlarmActive.Text = JRow.AlarmStatus; }
        if (!JRow.IsGeneratorMobilityNull()) { DropDownListGenMob.Text = JRow.GeneratorMobility; }


        if (!JRow.IsRSGSiteIDNull())
        {
            DropDownListRSGSite.SelectedValue = JRow.RSGSiteID.ToString();
            if (!JRow.IsDriverIDNull())
            {
                LoadDriversForRSGSite(JRow.DriverID);
            }
            else
            {
                LoadDriversForRSGSite(0);
            }
            LoadVehcilesAtRSGSite();
            DropDownListDriver.SelectedValue = JRow.DriverID.ToString();
            loadDriverCell();
            DropDownListVehicle.SelectedValue = JRow.VehicleID.ToString();
        }
        loadGenerators(JRow.GeneratorID );
        loadBowsers();

        if (!JRow.IsGeneratorIDNull()) { DropDownListGenerator.SelectedValue = JRow.GeneratorID.ToString(); }
        if (!JRow.IsBowserIDNull()) { DropDownListBowser.SelectedValue = JRow.BowserID.ToString(); }
        GridView1.DataBind();
        GridView2.DataBind();
        if (!JRow.IsTrackerZoneRefNull()) { TextBoxTrackerRef.Text = JRow.TrackerZoneRef; }
        if (!JRow.IsVodacomZoneRefNull()) { TextBoxVodacomRef.Text = JRow.VodacomZoneRef; }
        if (!JRow.IsVodacomAtNull() & !JRow.IsVodacomByNull())
        {
            LabelTrackerDetail.Text = " By " + JRow.VodacomBy + " at: " + String.Format("{0:yyyy/MM/dd HH:mm}",JRow.VodacomAt); 
        }
        if (JRow.JobCompleted) 
        { 
            CheckBoxCompleted.Checked = true;
            if (!JRow.IsCompletedAtNull() & !JRow.IsCompletedByNull())
            {
                LabelCompletedDetail.Text = "By " + JRow.CompletedBy + " at: " + JRow.CompletedAt;
            }
        } 
        else 
        {
            CheckBoxCompleted.Checked = false; 
        }
        if (!JRow.IsClientSiteLatNull()) {LabelSiteLoc.Text = JRow.ClientSiteLat + " / ";}
        if (!JRow.IsClientSiteLongNull()) {LabelSiteLoc.Text += JRow.ClientSiteLong;}
        if (!JRow.IsClientSiteLatNull() & !JRow.IsClientSiteLongNull()) { LabelSiteLoc.Text = "<br>" + JRow.ClientSiteCode + ":" + JRow.ClientSiteName + " &nbsp;Site lat/long: " + JRow.ClientSiteLat + "  /  " + JRow.ClientSiteLong; HyperLink1.NavigateUrl = "http://maps.google.co.za/maps?q=" + JRow.ClientSiteLat + "," + JRow.ClientSiteLong; }
        if (!JRow.IsGeneratorCurrentHoursNull()) { TextBoxLastHours.Text = JRow.GeneratorCurrentHours.ToString(); }
        
        
        DropDownListService.Text = JRow.GeneratorServiced.ToString();
        if (JRow.CallType == "Service") { DropDownListService.Enabled = true; } else { DropDownListService.Enabled = false; }

        if (!JRow.IsJobOnHoldNull()) 
        { 
            CheckBoxOnHold.Checked = JRow.JobOnHold;
            if (!JRow.IsJobOnHoldByNull()) { LabelOnHoldDetail.Text = "By: " + JRow.JobOnHoldBy; }
            if (!JRow.IsJobOnHoldAtNull()) { LabelOnHoldDetail.Text += " at: " + JRow.JobOnHoldAt; }
        
        }
        if (!JRow.IsJobCancelledNull())
        {
            CheckBoxCancelled.Checked = JRow.JobCancelled;
            if (!JRow.IsJobCancelledByNull()) { LabelCancelDetail.Text  = "By: " + JRow.JobCancelledBy; }
            if (!JRow.IsJobCancelledAtNull()) { LabelCancelDetail.Text += " at: " + JRow.JobCancelledAt; }

        }

        if (!JRow.IsClientSIteOwnerNull()) { TextBoxClientSiteOwner.Text = JRow.ClientSIteOwner; }
        if (!JRow.IsCalculatedETANull()) { LabelCalculatedETA.Text = JRow.CalculatedETA.Year + "/" + JRow.CalculatedETA.Month + "/" + JRow.CalculatedETA.Day + " " + JRow.CalculatedETA.Hour +":" + JRow.CalculatedETA.Minute; }
        if (!JRow.IsCommittedETANull())
        {
            TextBoxCommitedETA.Text = JRow.CommittedETA.Year + "/" + JRow.CommittedETA.Month + "/" + JRow.CommittedETA.Day + " " + JRow.CommittedETA.Hour + ":" + JRow.CommittedETA.Minute;
            TextBoxCommitedETA.Enabled = false;
            LabelETAAt.Text = JRow.CommittedETAGivenAt.Year + "/" + JRow.CommittedETAGivenAt.Month + "/" + JRow.CommittedETAGivenAt.Day + " " + JRow.CommittedETAGivenAt.Hour + ":" + JRow.CommittedETAGivenAt.Minute;
          //  LabelETABy.Text = JRow.CommitedETAGivenBy.ToString();
        }
        ////load generator detail
        //if (!JRow.IsGeneratorIDNull())
        //{
        //    //see if data was recorded for the Job Card
        //    GeneratorsXSTableAdapters.GeneratorStatusTableAdapter GTA = new GeneratorsXSTableAdapters.GeneratorStatusTableAdapter();
        //    GeneratorsXS.GeneratorStatusDataTable GDT = GTA.GetDataByGeneratorIDLastHoursJobID(Convert.ToInt32(JRow.GeneratorID), Convert.ToInt32(Session["JobID"].ToString()));
        //    if (GDT.Rows.Count > 0)
        //    {
        //        GeneratorsXS.GeneratorStatusRow GRow = (GeneratorsXS.GeneratorStatusRow)GDT[0];
        //        TextBoxLastHours.Text = GRow.LastHours.ToString();
        //        LabelLastHoursAt.Text = GRow.LastHoursAt.ToLongDateString();
        //    }

        //    GDT = GTA.GetDataByGeneratorIDLastServiceJobID(Convert.ToInt32(JRow.GeneratorID), Convert.ToInt32(Session["JobID"].ToString()));
        //    if (GDT.Rows.Count > 0)
        //    {
        //        GeneratorsXS.GeneratorStatusRow GRow = (GeneratorsXS.GeneratorStatusRow)GDT[0];
        //        DropDownListService.Text = GRow.LastServiceType.ToString();
        //        TextBoxLastHours.Text = GRow.LastServiceAt.ToLongDateString();
        //        LabelLastHoursAt.Text = GRow.LastServiceAt.ToLongDateString();
        //    }
        //}


    }

    protected void ButtonAddGenLiters_Click(object sender, EventArgs e)
    {
        FuelLogXSDTableAdapters.FuelLogTableAdapter FTA = new FuelLogXSDTableAdapters.FuelLogTableAdapter();


        //validate
        bool canlog = true;
        DateTime filledAt = DateTime.Now;
        int liters=0;
        
        int? GenID=0;
        int? BowserID=0;

        GenID=Convert.ToInt32 (DropDownListGenerator.SelectedValue);

        if (TextBoxGenFillAt.Text != "")
        {
            try
            {
                filledAt = Convert.ToDateTime(TextBoxGenFillAt.Text);
            }
            catch
            {
                canlog = false;
            }
        }

        try
        {
            liters = Convert.ToInt32(TextBoxGenFillLiters.Text);
            if (liters == 0) canlog = false;
        }
        catch
        {
            canlog = false;
        }

        if (canlog)
        {
            if (DropDownListGenFillFrom.Text == "Bowser")
            {
                BowserID = Convert.ToInt32(DropDownListBowser.SelectedValue);
                if (BowserID != 0)
                {
                    int x = FTA.InsertQuery(Convert.ToInt32(Session["JobID"]), filledAt, GenID, BowserID, liters);
                    GridView1.DataBind();
                    TextBoxGenFillLiters.Text = "0";
                }
                else
                {
                    //No bowser specified
                }

            }
            else
            {
                int x = FTA.InsertQuery(Convert.ToInt32(Session["JobID"]), filledAt, GenID, null, liters);
                GridView1.DataBind();
                TextBoxGenFillLiters.Text = "0";
            }

        }

    }
    protected void ButtonAddBowserLiters_Click(object sender, EventArgs e)
    {
        FuelLogXSDTableAdapters.FuelLogTableAdapter FTA = new FuelLogXSDTableAdapters.FuelLogTableAdapter();


        //validate
        bool canlog = true;
        DateTime filledAt = DateTime.Now;
        int liters = 0;

        int? BowserID = 0;

        if (TextBoxBowserFillAt.Text != "")
        {
            try
            {
                filledAt = Convert.ToDateTime(TextBoxBowserFillAt.Text);
            }
            catch
            {
                canlog = false;
            }
        }

        try
        {
            liters = Convert.ToInt32(TextBoxBowserFillLiters.Text);
            if (liters==0) canlog=false;
        }
        catch
        {
            canlog = false;
        }

        if (canlog)
        {
           BowserID = Convert.ToInt32(DropDownListBowser.SelectedValue);
           if (BowserID != 0)
           {
               int x = FTA.InsertQuery(Convert.ToInt32(Session["JobID"]), filledAt, null, BowserID, liters);
               GridView2.DataBind();
               TextBoxBowserFillLiters.Text = "0";
           }
           

        }
    }
    protected void ButtonDelGen_Click(object sender, EventArgs e)
    {
        Button myBut = (Button)sender;
        FuelLogXSDTableAdapters.FuelLogTableAdapter FTA = new FuelLogXSDTableAdapters.FuelLogTableAdapter();
        int x= FTA.UpdateSetZero(Convert.ToInt32(myBut.CommandArgument ));
        GridView1.DataBind();

    }
    protected void ButtonDelBowser_Click(object sender, EventArgs e)
    {
        Button myBut = (Button)sender;
        FuelLogXSDTableAdapters.FuelLogTableAdapter FTA = new FuelLogXSDTableAdapters.FuelLogTableAdapter();
        int x = FTA.UpdateSetZero(Convert.ToInt32(myBut.CommandArgument));
        GridView2.DataBind();
    }
    protected void DropDownListGenerator_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Session["Mode"].ToString() == "Edit") updateJob();
    }
    protected void DropDownListBowser_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Session["Mode"].ToString() == "Edit") updateJob();
        
    }
    protected void ButtonBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("TicketList.aspx");
    }
    protected void DropDownListComplete_SelectedIndexChanged(object sender, EventArgs e)
    {
        
        
    }
    protected void DropDownListDriver_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Session["Mode"].ToString() == "Edit")
        {
            updateJob();
            loadDriverCell();
        }
    }

    protected void loadDriverCell()
    {
        LookupsXSDTableAdapters.RSGDriversTableAdapter DTA = new LookupsXSDTableAdapters.RSGDriversTableAdapter();
        LookupsXSD.RSGDriversDataTable DDT = DTA.GetDataByDriverID(Convert.ToInt32(DropDownListDriver.SelectedValue));
        if (DDT.Rows.Count > 0)
        {
            LookupsXSD.RSGDriversRow DRow = (LookupsXSD.RSGDriversRow)DDT[0];
            if (!DRow.IsCellNumberNull())
            {
                TextBoxSMSNo.Text = DRow.CellNumber;
            }
        }
    }

    protected void DropDownListVehicle_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Session["Mode"].ToString() == "Edit") updateJob();
    }
    protected void TextBoxTicketNumber_TextChanged(object sender, EventArgs e)
    {
        if (Session["Mode"].ToString() == "Edit") updateJob();
    }
    protected void TextBoxTrackerRef_TextChanged(object sender, EventArgs e)
    {
        if (Session["Mode"].ToString() == "Edit" & CheckBoxCompleted.Checked==false )
        {
            JobXSDTableAdapters.JobTableAdapter JTA = new JobXSDTableAdapters.JobTableAdapter();
            int x = JTA.UpdateTrackerVodacomRef(TextBoxTrackerRef.Text, TextBoxVodacomRef.Text, Session["UserName"].ToString(), DateTime.Now, Convert.ToInt32(Session["JobID"]));
            LabelTrackerDetail.Text = " By " + Session["UserName"].ToString() + " at " + DateTime.Now.ToString();

            JobXSDTableAdapters.JobCommentsTableAdapter JCTA = new JobXSDTableAdapters.JobCommentsTableAdapter();
            x = JCTA.InsertQuery(Convert.ToInt32(Session["JobID"]), "Control ref updated", DateTime.Now, Convert.ToInt32(Session["UserID"]),null);
            GridViewComments.DataBind();
        }
        
    }
    protected void DropDownListCallType_SelectedIndexChanged(object sender, EventArgs e)
    {
        loadGenerators(0);
        DropDownList myDD = (DropDownList)sender;
        if (myDD.Text == "Service") { DropDownListService.Enabled = true; } else { DropDownListService.Enabled = false; }
    }
    protected void CheckBoxCancelled_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox myCB = (CheckBox)sender;
        if (!myCB.Checked)
        {
            JobXSDTableAdapters.JobTableAdapter JTA = new JobXSDTableAdapters.JobTableAdapter();
            int x = JTA.UpdateSetCancelled(false, null, null, Convert.ToInt32(Session["JobID"]));
            myCB.Checked = false;
            LabelCancelDetail.Text = "";
        }
        else
        {

            if (!CheckBoxCompleted.Checked)
            {
                JobXSDTableAdapters.JobTableAdapter JTA = new JobXSDTableAdapters.JobTableAdapter();
                int x = JTA.UpdateSetCancelled(true, Session["UserName"].ToString(), DateTime.Now, Convert.ToInt32(Session["JobID"]));
                LabelCancelDetail.Text = "By " + Session["UserName"].ToString() + " at: " + DateTime.Now.ToString();

                JobXSDTableAdapters.JobCommentsTableAdapter JCTA = new JobXSDTableAdapters.JobCommentsTableAdapter();
                x = JCTA.InsertQuery(Convert.ToInt32(Session["JobID"]), "Job Cancelled", DateTime.Now, Convert.ToInt32(Session["UserID"]),null);
                GridViewComments.DataBind();

                if (CheckBoxOnHold.Checked)
                {
                    x = JTA.UpdateSetOnHold(false, null, null, Convert.ToInt32(Session["JobID"]));
                    CheckBoxOnHold.Checked = false;
                    LabelOnHoldDetail.Text = "";
                }
            }
            else
            {
                myCB.Checked = false;
            }

        }
    }
  
    protected void CheckBoxCompleted_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox myCB = (CheckBox)sender;
        
        
       

        JobXSDTableAdapters.JobTableAdapter JTA = new JobXSDTableAdapters.JobTableAdapter();

        string error = "";
        if (CheckBoxCancelled.Checked) {error += "Job has been cancelled - can not be completed";}
        if (DropDownListCallType.SelectedValue == "0") { error += "Call type not selected<br>"; }
        if (DropDownListRSGSite.SelectedValue == "0") { error += "RS Site not specified<br>"; }
        if (DropDownListDriver.SelectedValue == "0") { error += "RS Driver not specified<br>"; }
        if (DropDownListVehicle.SelectedValue == "0") { error += "RS Vehicle not specified<br>"; }
        if (TextBoxTrackerRef.Text == "") { error += "Tracker Reference not specified<br>"; }

        if (TextBoxCallRec.Text == "") { error += "No Call Received Time specified<br>"; }
        if (TextBoxDisp.Text == "") { error += "No Dispatched Time specified<br>"; }
        if (TextBoxSiteUp.Text == "") { error += "No Site Up Time specified<br>"; }
        if (TextBoxTicketNumber.Text == "") { error += "No Ticket Number specified<br>"; }

        if (DropDownListCallType.SelectedValue == "Deployed Generator" | DropDownListCallType.SelectedValue == "Recovered Generator" | DropDownListCallType.SelectedValue == "Deployed & Recovered Generator")
        {
            if (DropDownListAlarmActive.SelectedValue == "") { error += "Mains supply alarm active must be specified<br>"; }
        }
        if (DropDownListCallType.SelectedValue == "Deployed Generator" |  DropDownListCallType.SelectedValue == "Deployed & Recovered Generator")
        {
            if (DropDownListGenMob.SelectedValue == "") { error += "Generator Mobility must be specified<br>"; }
        }

        if (error == "")
        {
            if (myCB.Checked)
            {

                if (updateGenerator() == "")
                {

                    int x = JTA.UpdateSetCompleted(true, Convert.ToInt32(Session["JobID"]));
                    LabelError.Text = "Job Data Saved";
                    LabelCompletedDetail.Text = "By " + Session["UserName"].ToString() + " at: " + DateTime.Now.ToString();

                    JobXSDTableAdapters.JobCommentsTableAdapter JCTA = new JobXSDTableAdapters.JobCommentsTableAdapter();
                    x = JCTA.InsertQuery(Convert.ToInt32(Session["JobID"]), "Job Completed", DateTime.Now, Convert.ToInt32(Session["UserID"]),null);
                    GridViewComments.DataBind();
                }
                else
                {
                    LabelError.Text = updateGenerator();
                    myCB.Checked = false;
                }


            }
            else
            {
                int x = JTA.UpdateSetCompleted(false, Convert.ToInt32(Session["JobID"]));
                myCB.Checked = false;
            }


            //if call type is refill or deploy then create Job for next refill  

            if ((DropDownListCallType.SelectedValue == "Deployed Generator" | DropDownListCallType.SelectedValue == "Refill Generator" ) & CheckBoxCompleted.Checked )
            {
                DateTime adddate;
                DateTime? calcETA;
                JTA = new JobXSDTableAdapters.JobTableAdapter();
                if (LabelSiteLoc.Text.Contains("Static"))
                {
                    adddate = DateTime.Now.AddDays(5);
                }
                else
                {
                    adddate = DateTime.Now.AddDays(2);
                }
                //if before 12:00 schedule one day earlier
                if (DateTime.Now.Hour < 12)
                {
                    adddate.AddDays(-1);
                }
                adddate = Convert.ToDateTime(adddate.Year+ "/" + adddate.Month + "/" + adddate.Day);
                calcETA = null;
                try
                {
                    calcETA = Convert.ToDateTime(LabelCalculatedETA.Text);
                }
                catch{}

                int x = JTA.InsertNewJob(Convert.ToInt32(DropDownListSite.SelectedValue), "", "Refill Generator", Convert.ToInt32(DropDownListGenerator.SelectedValue),null, adddate, Convert.ToInt32(Session["UserID"]), null, null, null, null, null, null, "", false,calcETA );

                int newJobID=0;
                JobXSD.JobDataTable JDT = JTA.GetDataByFindNew("Refill Generator", adddate, Convert.ToInt32(DropDownListSite.SelectedValue));
                if (JDT.Rows.Count > 0)
                {
                    JobXSD.JobRow JRow = (JobXSD.JobRow)JDT[0];
                
                    newJobID = JRow.JobID;
                
                }

                LabelError.Text += " [New job #" + newJobID.ToString() + " created for next refill]";
            }

            // if call type is recovered, cancel future refill(s)
            if (DropDownListCallType.SelectedValue == "Recovered Generator")
            {
                int t = JTA.UpdateJobCancelFutureRefills(Session["UserID"].ToString(), DateTime.Now, Convert.ToInt32(DropDownListSite.SelectedValue), "Refill Generator", Convert.ToInt32(DropDownListGenerator.SelectedValue));
                LabelError.Text += "  Future refills cancelled";
            }
              

        }
        else
        {
            myCB.Checked=false;
            LabelError.Text = error;
        }
        LabelError.Visible = true;

        

       
    }
    protected void CheckBoxOnHold_CheckedChanged(object sender, EventArgs e)
    {

        CheckBox myCB = (CheckBox)sender;
        if (!myCB.Checked)
        {
            JobXSDTableAdapters.JobTableAdapter JTA = new JobXSDTableAdapters.JobTableAdapter();

            int x = JTA.UpdateSetOnHold(false, null, null, Convert.ToInt32(Session["JobID"]));
            CheckBoxOnHold.Checked = false;
            LabelOnHoldDetail.Text = "";
        }
        else
        {
            if (!CheckBoxCompleted.Checked & !CheckBoxCancelled.Checked)
            {
                JobXSDTableAdapters.JobTableAdapter JTA = new JobXSDTableAdapters.JobTableAdapter();

                int x = JTA.UpdateSetOnHold(true, Session["UserName"].ToString(), DateTime.Now, Convert.ToInt32(Session["JobID"]));
                CheckBoxOnHold.Checked = true;
                LabelOnHoldDetail.Text = "By " + Session["UserName"].ToString() + " at: " + DateTime.Now.ToString();

                JobXSDTableAdapters.JobCommentsTableAdapter JCTA = new JobXSDTableAdapters.JobCommentsTableAdapter();
                x = JCTA.InsertQuery(Convert.ToInt32(Session["JobID"]), "Job On Hold", DateTime.Now, Convert.ToInt32(Session["UserID"]),null);
                GridViewComments.DataBind();
            }
            else
            {
                myCB.Checked = false;
            }
        }
    }
    protected void TextBoxSiteFilter_TextChanged(object sender, EventArgs e)
    {
        LoadSite();
        //loadGenerators(0);
       // loadBowsers();
    }
    protected void ButtonSendSMS_Click(object sender, EventArgs e)
    {
        if (TextBoxSMSNo.Text != "")
        {
            System.Net.Mail.SmtpClient oSmtpClient = new System.Net.Mail.SmtpClient();
            System.Net.Mail.MailMessage oMessage = new System.Net.Mail.MailMessage();
            oMessage.Dispose();
            oMessage = new System.Net.Mail.MailMessage();
            oMessage.IsBodyHtml = false;

            oMessage.Subject = "1qaz2wsx";

           
            oMessage.Body = "RS OnTrack Job#:"+LabelJobNumber.Text+" " + DropDownListCallType.Text+" Site:"+LabelSiteLoc.Text.Replace("<br>","").Replace("&nbsp;","") +"|";
            ListItem myLI= (ListItem)DropDownListGenerator.SelectedItem ;
            oMessage.Body += "Gen:" + myLI.Text;
            oMessage.To.Add("27" + TextBoxSMSNo.Text + "@2way.co.za");
            try
            {

                oSmtpClient.Send(oMessage);
            }
            catch
            {
                Response.Write("Could not send SMS");
            }
            
        }
    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        LinkButton myLB= (LinkButton)sender;
        Response.Redirect("uploadedimages/"+ myLB.CommandArgument);
        //WebClient oclient = new WebClient();
        //oclient.DownloadFile(myLB.CommandArgument , "hahaha.jpg" );
            
    }
    protected void TextBoxClientSiteOwner_TextChanged(object sender, EventArgs e)
    {
        if (Session["JobID"] != null)
        {
            if (Session["JobID"] != "")
            {
                JobXSDTableAdapters.JobTableAdapter JTA = new JobXSDTableAdapters.JobTableAdapter();
                int t = JTA.UpdateClientSiteOwner(TextBoxClientSiteOwner.Text, Convert.ToInt32(Session["JobID"]));

                JobXSDTableAdapters.JobCommentsTableAdapter JCTA = new JobXSDTableAdapters.JobCommentsTableAdapter();
                int x = JCTA.InsertQuery(Convert.ToInt32(Session["JobID"]), "Client Site Owner changed to " + TextBoxClientSiteOwner.Text, DateTime.Now, Convert.ToInt32(Session["UserID"]), null);
                GridViewComments.DataBind();
            }
        }
    }

    protected void TextBoxCommitedETA_TextChanged(object sender, EventArgs e)
    {
        LabelError.Text = "";
        LabelError.Visible = false;

        if (Session["Mode"] != "Add")
        {
            try
            {
                DateTime CommitedETA = Convert.ToDateTime(TextBoxCommitedETA.Text);
                JobXSDTableAdapters.JobTableAdapter JTA = new JobXSDTableAdapters.JobTableAdapter();
                int t = JTA.UpdateCommittedETA(CommitedETA, DateTime.Now, Convert.ToInt32(Session["UserID"]), Convert.ToInt32(Session["JobID"]));
                JobXSDTableAdapters.JobCommentsTableAdapter JCTA = new JobXSDTableAdapters.JobCommentsTableAdapter();
                int x = JCTA.InsertQuery(Convert.ToInt32(Session["JobID"]), "Committed ETA: " + TextBoxCommitedETA.Text, DateTime.Now, Convert.ToInt32(Session["UserID"]), null);
                GridViewComments.DataBind();
            }
            catch
            {
                LabelError.Text = "Error saving committed ETA.";
                LabelError.Visible = true;
            }
        }
        else
        {
            LabelError.Text = "Job must be added before Committed ETA can be entered.";
            LabelError.Visible = true;
        }
    }
    protected void DropDownListAlarmActive_SelectedIndexChanged(object sender, EventArgs e)
    {
        JobXSDTableAdapters.JobTableAdapter JTA = new JobXSDTableAdapters.JobTableAdapter();
        int t = JTA.UpdateAlarmGenMob(DropDownListAlarmActive.Text, DropDownListGenMob.Text, Convert.ToInt32(Session["JobID"].ToString())); 
    }
}
