﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WorkshopTicketList : System.Web.UI.Page
{
    int ctopen = 0;
    int ctcan = 0;
    int cthold = 0;
    int ctover90 = 0;
    int ctover180 = 0;
    int ctrefill = 0;


    protected void Page_Load(object sender, EventArgs e)
    {
       
        if (!IsPostBack)
        {
            loadDropdowns();
          //  graph();
            if (Session["UserIsController"].ToString() == "True" | Session["UserIsManager"].ToString() == "True" | Session["UserIsSystemAdmin"].ToString() == "True")
            {

            }
            else
            {
                Response.Write("user has no rights");
                Response.End();
            }
       
            
            
        }
        
        GridView1.DataBind();
        
    }
    

    protected void loadDropdowns()
    {
       



        ListItem myLI = new ListItem("All","0");
        DropDownListClient.Items.Add(myLI);

        LookupsXSDTableAdapters.ClientsTableAdapter CTA = new LookupsXSDTableAdapters.ClientsTableAdapter();
        LookupsXSD.ClientsDataTable CDT = CTA.GetData();
        
       
        for (int f = 0; f < CDT.Rows.Count; f++)
        {
            LookupsXSD.ClientsRow CRow = (LookupsXSD.ClientsRow)CDT[f];
            myLI = new ListItem(CRow.ClientName ,CRow.ClientID.ToString());
            DropDownListClient.Items.Add(myLI);
        }

        myLI = new ListItem("RS Workshop", "RSWS");
        DropDownListWorkshop.Items.Add(myLI);

        LookupsXSDTableAdapters.ClientSitesTableAdapter CSTA = new LookupsXSDTableAdapters.ClientSitesTableAdapter();
        LookupsXSD.ClientSitesDataTable CSDT = CSTA.GetDataByClientSiteCode("RSWS");

        for (int f = 0; f < CSDT.Rows.Count; f++)
        {
            LookupsXSD.ClientSitesRow CRow = (LookupsXSD.ClientSitesRow)CSDT[f];
            myLI = new ListItem(CRow.ClientSiteName, CRow.ClientSiteCode );
            DropDownListWorkshop.Items.Add(myLI);
        }



        myLI = new ListItem("Workshop", "Workshop");
        DropDownListCallType.Items.Add(myLI);


        LookupsXSDTableAdapters.CallTypesTableAdapter CTTA = new LookupsXSDTableAdapters.CallTypesTableAdapter();
        LookupsXSD.CallTypesDataTable CTDT = CTTA.GetData();

        for (int f = 0; f < CTDT.Rows.Count; f++)
        {
            LookupsXSD.CallTypesRow CTRow = (LookupsXSD.CallTypesRow)CTDT[f];
            if (CTRow.CallType.Contains("Workshop"))
            {
                myLI = new ListItem(CTRow.CallType, CTRow.CallType);
                DropDownListCallType.Items.Add(myLI);
            }
        }
        

        TextBoxFrom.Text = DateTime.Now.AddDays (-14).ToShortDateString();
        TextBoxTo.Text = DateTime.Now.AddDays(1).ToShortDateString();

    }


    
    
    protected void ButtonFind_Click(object sender, EventArgs e)
    {
        
        GridView1.DataBind();
        
    }

    

  

   
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
       



        if (DropDownListCompleted.SelectedValue == "False")
        {



            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ctopen++;


                Image myImage = (Image)e.Row.FindControl("ImageLogo");
                Label LabelClient = (Label)e.Row.FindControl("LabelClient");
                if (LabelClient.Text == "MTN")
                {
                    myImage.ImageUrl = "~/images/yellow.jpg";
                }
                else
                {
                    myImage.ImageUrl = "~/images/red.jpg";
                }


                Label LabelCallReceivedAt = (Label)e.Row.FindControl("LabelCallReceivedAt");
                Label LabelDispatchedAt = (Label)e.Row.FindControl("LabelDispatchedAt");
                Label LabelSiteUpAt = (Label)e.Row.FindControl("labelSiteUpAt");
                Label LabelCallType = (Label)e.Row.FindControl("LabelCallType");

                CheckBox CBHold = (CheckBox)e.Row.FindControl("CheckBoxOnHold");
                CheckBox CBCan = (CheckBox)e.Row.FindControl("CheckBoxCan");

                if (CBHold.Checked) cthold++;
                if (CBCan.Checked) ctcan++;


                if (!CBHold.Checked & !CBCan.Checked)
                {
                    DateTime DTreceived = Convert.ToDateTime(LabelCallReceivedAt.Text);
                    
                    if (LabelCallType.Text != "Refill Generator")
                    {

                        if (DTreceived.AddMinutes(90) < DateTime.Now) { ctover90++;  e.Row.BackColor = System.Drawing.Color.Yellow; }
                        if (DTreceived.AddMinutes(180) < DateTime.Now) { ctover180++; ctover90--; e.Row.BackColor = System.Drawing.Color.Red; }
                    }
                    else
                    {
                        if (DTreceived.AddMinutes(1440) > DateTime.Now)
                        { ctrefill++;  e.Row.BackColor = System.Drawing.Color.Cyan; }
                        else
                        { ctover180++; e.Row.BackColor = System.Drawing.Color.Red; }
                    }
                }
                else
                {
                    e.Row.BackColor = System.Drawing.Color.LightPink;
                }


               
            }

        }

        if (DropDownListCompleted.SelectedValue == "True")
        {
            


            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ctopen++;
                try
                {
                    Label LabelCallReceivedAt = (Label)e.Row.FindControl("LabelCallReceivedAt");
                    Label LabelDispatchedAt = (Label)e.Row.FindControl("LabelDispatchedAt");
                    Label LabelSiteUpAt = (Label)e.Row.FindControl("labelSiteUpAt");

                     CheckBox CBHold = (CheckBox)e.Row.FindControl("CheckBoxOnHold");
                    CheckBox CBCan = (CheckBox)e.Row.FindControl("CheckBoxCan");
                    if (CBHold.Checked) cthold++;
                    if (CBCan.Checked) ctcan++;

                    if (!CBHold.Checked & !CBCan.Checked)
                    {
                        DateTime DTreceived = Convert.ToDateTime(LabelCallReceivedAt.Text);
                        DateTime DTcompleted = Convert.ToDateTime(LabelSiteUpAt.Text);
                        if (DTreceived.AddMinutes(90) < DTcompleted) { ctover90++; e.Row.BackColor = System.Drawing.Color.LightYellow; }
                        if (DTreceived.AddMinutes(180) < DTcompleted) { ctover90--; ctover180++; e.Row.BackColor = System.Drawing.Color.Salmon; }
                    }
                    else
                    {
                        e.Row.BackColor = System.Drawing.Color.LightPink;
                    }
                }
                 
                catch
                {
                    
                }
            }
        }


    }
    protected void LinkButtonJobID_Click(object sender, EventArgs e)
    {
        LinkButton myLB = (LinkButton)sender;
        Session["JobID"]= myLB.Text;
        Session["Mode"] = "Edit";
        Response.Redirect("WorkshopTicketDetail.aspx");
    }

    
 protected void DropDownListClient_SelectedIndexChanged(object sender, EventArgs e)
    {
       

       
    }

   
}
