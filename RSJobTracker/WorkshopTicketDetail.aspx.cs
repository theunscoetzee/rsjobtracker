﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;


public partial class WorkshopTicketDetail : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

       if (!IsPostBack)
       {

           LabelJobNumber.Text = Session["JobID"].ToString();
           LoadJobData(Convert.ToInt32(Session["JobID"].ToString()));
           loadDropdowns(); 
            

        }
       
    }

    
    protected void loadDropdowns()
    {
        ListItem myLI = new ListItem("", "");
        DropDownListCategory.Items.Add(myLI);

        WorkshopLinesXSDTableAdapters.WokrshopLineCategoryTableAdapter LCTA = new WorkshopLinesXSDTableAdapters.WokrshopLineCategoryTableAdapter();
        WorkshopLinesXSD.WokrshopLineCategoryDataTable LCDT = LCTA.GetData();
        for (int f =0;f<LCDT.Rows.Count;f++)
        {
            WorkshopLinesXSD.WokrshopLineCategoryRow CRow = (WorkshopLinesXSD.WokrshopLineCategoryRow )LCDT[f];
            myLI = new ListItem(CRow.Category, CRow.Category);
            DropDownListCategory.Items.Add(myLI);
        }


        myLI = new ListItem("", "");
        DropDownListPartUsed.Items.Add(myLI);

        WorkshopLinesXSDTableAdapters.WorkshopPartsTableAdapter WPTA = new WorkshopLinesXSDTableAdapters.WorkshopPartsTableAdapter();
        WorkshopLinesXSD.WorkshopPartsDataTable WPDT = WPTA.GetData();
        for (int f = 0; f < WPDT.Rows.Count; f++)
        {
            WorkshopLinesXSD.WorkshopPartsRow PRow = (WorkshopLinesXSD.WorkshopPartsRow)WPDT[f];
            myLI = new ListItem(PRow.PartCategory + " " + PRow.PartDescription + " " + PRow.PartNumber, PRow.PartCategory + " " + PRow.PartDescription + " " + PRow.PartNumber);
            DropDownListPartUsed.Items.Add(myLI);
        }

        
    }



    protected void ButtonAddLogItem_Click(object sender, EventArgs e)
    {
        if (TextBoxComment.Text != "")
        {

            //upload photo if one was specified
            String savePath = "";
            String fileName = "";
            if (FileUpload1.HasFile)
            {
                savePath = ConfigurationManager.AppSettings["UploadPhotoSavepath"] + "/";
                // Get the name of the file to upload.
                fileName = FileUpload1.FileName;

                // Append the name of the file to upload to the path.
                savePath += fileName;
                FileUpload1.SaveAs(savePath);

            }



            JobXSDTableAdapters.JobCommentsTableAdapter JCTA = new JobXSDTableAdapters.JobCommentsTableAdapter();
            int x = JCTA.InsertQuery(Convert.ToInt32(Session["JobID"]), TextBoxComment.Text, DateTime.Now, Convert.ToInt32(Session["UserID"]), fileName);

        }
        GridViewComments.DataBind();
    }
    

    

    protected void LoadJobData(int JobID)
    {
        JobXSDTableAdapters.JobTableAdapter JTA = new JobXSDTableAdapters.JobTableAdapter();
        JobXSD.JobDataTable JDT = JTA.GetDataByJobID (JobID);
        JobXSD.JobRow JRow = (JobXSD.JobRow)JDT[0];

        LabelJobNumber.Text = JRow.JobID.ToString() ;
        LabelCallType.Text = JRow.CallType;
        if (JRow.CallType.Contains("Generator")) { LabelCallType.Text += " " + JRow.GeneratorRegNumber; }
        if (JRow.CallType.Contains("Vehicle")) { LabelCallType.Text += " " + JRow.RSGVehicleRegNumber ; } 
        if (JRow.CallType.Contains("Bowser")) { LabelCallType.Text += " " + JRow.BowserReg ; }

        


    }

    protected void ButtonBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("WorkshopTicketList.aspx");
    }
    
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        LinkButton myLB= (LinkButton)sender;
        Response.Redirect("uploadedimages/"+ myLB.CommandArgument);
        //WebClient oclient = new WebClient();
        //oclient.DownloadFile(myLB.CommandArgument , "hahaha.jpg" );
            
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        WorkshopLinesXSDTableAdapters.WorkshopLinesTableAdapter WLTA = new WorkshopLinesXSDTableAdapters.WorkshopLinesTableAdapter();
        int t = WLTA.InsertQuery(Convert.ToInt32(LabelJobNumber.Text), Convert.ToInt32(Session["UserID"].ToString()), DateTime.Now, DropDownListCategory.Text, TextBoxWorkDescription.Text, DropDownListPartUsed.Text, TextBoxPartDescription.Text, Convert.ToDecimal(TextBoxCost.Text));

        GridView1.DataBind();


        LabelWorkDone.Visible = false;
        TextBoxWorkDescription.Visible = false;
        LabelPartNumber.Visible = false;
        LabelpartUsed.Visible = false;
        DropDownListPartUsed.Visible = false;
        TextBoxPartDescription.Visible = false;
        TextBoxPartNumber.Visible = false;
        ButtonAddLine.Visible = false;
        LabelpartCost.Visible = false;
        TextBoxCost.Visible = false;

    }
    protected void DropDownListCategory_SelectedIndexChanged(object sender, EventArgs e)
    {

        LabelWorkDone.Visible = false;
        TextBoxWorkDescription.Visible = false;
        LabelPartNumber.Visible = false;
        LabelpartUsed.Visible = false;
        DropDownListPartUsed.Visible = false;
        TextBoxPartDescription.Visible = false;
        TextBoxPartNumber.Visible = false;
        LabelpartCost.Visible = false;
        TextBoxCost.Visible = false;

        DropDownList myDD = (DropDownList)sender;
        if (myDD.Text == "Work Done")
        {
            LabelWorkDone.Visible = true;
            TextBoxWorkDescription.Visible = true;

            LabelPartNumber.Visible = false;
            LabelpartUsed.Visible = false;
            DropDownListPartUsed.Visible = false;
            TextBoxPartDescription.Visible = false;
            TextBoxPartNumber.Visible = false;
            LabelpartCost.Visible = true;
            TextBoxCost.Visible = true;
            TextBoxCost.Text = "0";

            ButtonAddLine.Visible = true;
        }

        
        if (myDD.Text == "Replaced" | myDD.Text == "Repaired")
        {
            LabelWorkDone.Visible = false;
            TextBoxWorkDescription.Visible = false;
            TextBoxWorkDescription.Text = myDD.Text;

            LabelPartNumber.Visible = true;
            LabelpartUsed.Visible = true;
            DropDownListPartUsed.Visible = true;
            TextBoxPartDescription.Visible = true;
            LabelpartCost.Visible = true;
            TextBoxCost.Visible = true;
            TextBoxCost.Text = "0";

            TextBoxPartNumber.Visible = true;
            ButtonAddLine.Visible = true;
        }
    }
}
