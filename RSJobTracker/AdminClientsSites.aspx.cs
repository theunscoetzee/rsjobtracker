﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AdminClientsSites : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
           
        }
       
    }



    protected void ListBoxClients_SelectedIndexChanged(object sender, EventArgs e)
    {
        ListBoxRegions.DataBind();
        ListBoxSubRegions.DataBind();
        GridView1.DataBind();
        
    }
    protected void ListBoxRegions_SelectedIndexChanged(object sender, EventArgs e)
    {
        ListBoxSubRegions.DataBind();
        GridView1.DataBind();
    }
    protected void ListBoxSubRegions_SelectedIndexChanged(object sender, EventArgs e)
    {
        
        GridView1.DataBind();
    }
    protected void ButtonAdd_Click(object sender, EventArgs e)
    {
        try
        {
            LookupsXSDTableAdapters.ClientSitesTableAdapter CSTA = new LookupsXSDTableAdapters.ClientSitesTableAdapter();
            int x = CSTA.InsertQuery(Convert.ToInt32(ListBoxClients.SelectedValue), Convert.ToInt32(ListBoxSubRegions.SelectedValue), TextBoxCounty.Text, TextBoxCode.Text, TextBoxSiteName.Text, TextBoxLat.Text, TextBoxLong.Text,TextBoxSiteOwner.Text,Convert.ToInt32(TextBoxSitePriority.Text),Convert.ToInt32(TextBoxSiteResponseTime.Text));
            GridView1.DataBind();
        }
        catch
        {
            Response.Write("An update error occured.");
        }
    }
    protected void ButtonAddSubRegion_Click(object sender, EventArgs e)
    {
        LookupsXSDTableAdapters.ClientSubRegionsTableAdapter CSTA = new LookupsXSDTableAdapters.ClientSubRegionsTableAdapter();
        int x = CSTA.InsertQuery(Convert.ToInt32(ListBoxRegions.SelectedValue), TextBoxAddSubRegion.Text);
        ListBoxSubRegions.DataBind();
    }
    protected void ButtonAddRegion_Click(object sender, EventArgs e)
    {
        LookupsXSDTableAdapters.ClientRegionsTableAdapter CRTA = new LookupsXSDTableAdapters.ClientRegionsTableAdapter();
        int x = CRTA.InsertQuery(Convert.ToInt32(ListBoxClients.SelectedValue), TextBoxAddRegion.Text);
        ListBoxRegions.DataBind();
    }
    protected void ButtonAddClient_Click(object sender, EventArgs e)
    {
        if (TextBoxAddClient.Text != "")
        {
            LookupsXSDTableAdapters.ClientsTableAdapter CTA = new LookupsXSDTableAdapters.ClientsTableAdapter();
            int x = CTA.InsertQuery(TextBoxAddClient.Text);
            ListBoxClients.DataBind();
        }
    }
    protected void ButtonUpdate_Click(object sender, EventArgs e)
    {
        Button myBut = (Button)sender;
        TextBox ClientSiteCounty = (TextBox)myBut.Parent.FindControl("TextBoxCounty");
        TextBox ClientSiteCode = (TextBox)myBut.Parent.FindControl("TextBoxSiteCode");
        TextBox ClientSiteName = (TextBox)myBut.Parent.FindControl("TextBoxSiteName");
        TextBox ClientSiteLat = (TextBox)myBut.Parent.FindControl("TextBoxSiteLat");
        TextBox ClientSiteLong = (TextBox)myBut.Parent.FindControl("TextBoxSiteLong");
        TextBox ClientSiteOwner = (TextBox)myBut.Parent.FindControl("TextBoxSiteOwner");
        TextBox ClientSitePriority = (TextBox)myBut.Parent.FindControl("TextBoxPriority");
        TextBox ClientSiteResponseTime = (TextBox)myBut.Parent.FindControl("TextBoxResponseTime");


        LookupsXSDTableAdapters.ClientSitesTableAdapter CSTA = new LookupsXSDTableAdapters.ClientSitesTableAdapter();
        int x = CSTA.UpdateQuery(ClientSiteCounty.Text, ClientSiteCode.Text, ClientSiteName.Text, ClientSiteLat.Text, ClientSiteLong.Text,ClientSiteOwner.Text,Convert.ToInt32(ClientSitePriority.Text),Convert.ToInt32(ClientSiteResponseTime.Text),Convert.ToInt32(myBut.CommandArgument));

        GridView1.DataBind();

    }
  
}
